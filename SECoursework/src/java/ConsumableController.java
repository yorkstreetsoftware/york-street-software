/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.DatabaseConnection;
import classes.Drink;
import classes.Food;
import classes.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Max
 */
@WebServlet(urlPatterns = {"/ConsumableController"})
public class ConsumableController extends HttpServlet {

    HttpServletRequest request = null;
    HttpServletResponse response = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        this.request = request;
        this.response = response;

        String requestType = request.getParameter("request");

        switch (requestType) {
            case "updateFood":
                updateFood();
                break;

            case "updateDrink":
                updateDrink();
                break;
        }

        response.getOutputStream().flush();
    }

    private void updateDrink() throws ServletException, IOException {
        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        HttpSession session = request.getSession(true);

        User user = null;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }
        ArrayList<Drink> drinkList = user.getCustomDrinkList();

        drinkName = request.getParameter("drinkName");
        calories = request.getParameter("dcalories");
        protein = request.getParameter("dprotein");
        carbohydrates = request.getParameter("dcarbohydrates");
        sugar = request.getParameter("dsugar");
        fat = request.getParameter("dfat");
        satFat = request.getParameter("dsatFat");
        salt = request.getParameter("dsalt");
        String drinkID = request.getParameter("drinkid");
        int id = Integer.parseInt(drinkID);

        int dCalories = Integer.parseInt(calories);
        double dProtein = Double.parseDouble(protein);
        double dCarbohydrates = Double.parseDouble(carbohydrates);
        double dSugar = Double.parseDouble(sugar);
        double dFat = Double.parseDouble(fat);
        double dSatFat = Double.parseDouble(satFat);
        double dSalt = Double.parseDouble(salt);
        Drink drink = null;

        for (int i = 0; i < drinkList.size(); i++) {

            if (drinkList.get(i).getId() == id) {
                drink = drinkList.get(i);
            }
        }

        try {
            drink.updateDrinkName(drinkName);
            drink.updateCalories(dCalories);
            drink.updateCarbohydrates(dCarbohydrates);
            drink.updateSugar(dSugar);
            drink.updateFat(dFat);
            drink.updateSatFat(dSatFat);
            drink.updateSalt(dSalt);
            drink.updateProtein(dProtein);
            
        } catch (SQLException ex) {
            Logger.getLogger(ConsumableController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void updateFood() throws IOException, ServletException {
        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.getIsStaff() == false) {
            response.sendRedirect("index.jsp");
            return;
        }

        foodName = request.getParameter("foodName");
        mealType = request.getParameter("mealType");
        calories = request.getParameter("calories");
        protein = request.getParameter("protein");
        carbohydrates = request.getParameter("carbohydrates");
        sugar = request.getParameter("sugar");
        fat = request.getParameter("fat");
        satFat = request.getParameter("satFat");
        salt = request.getParameter("salt");
        String foodID = request.getParameter("foodid");
        int id = Integer.parseInt(foodID);
        int fCalories = Integer.parseInt(calories);
        double fProtein = Double.parseDouble(protein);
        double fCarbohydrates = Double.parseDouble(carbohydrates);
        double fSugar = Double.parseDouble(sugar);
        double fFat = Double.parseDouble(fat);
        double fSatFat = Double.parseDouble(satFat);
        double fSalt = Double.parseDouble(salt);
        Food food = null;

        for (int i = 0; i < user.getCustomFoodList().size(); i++) {
            if (user.getCustomFoodList().get(i).getId() == id) {
                food = user.getCustomFoodList().get(i);
            }
        }
        try {
            food.updateFoodName(foodName);
            food.updateMealType(mealType);
            food.updateCalories(fCalories);
            food.updateCarbohydrates(fCarbohydrates);
            food.updateSugar(fSugar);
            food.updateFat(fFat);
            food.updateSatFat(fSatFat);
            food.updateSalt(fSalt);
            food.updateProtein(fProtein);

        } catch (SQLException ex) {
            Logger.getLogger(ConsumableController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
