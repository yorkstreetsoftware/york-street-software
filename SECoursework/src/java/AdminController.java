/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.DatabaseConnection;
import classes.Drink;
import classes.Food;
import classes.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Max
 */
@WebServlet(urlPatterns = {"/AdminController"})
public class AdminController extends HttpServlet {

    HttpServletRequest request = null;
    HttpServletResponse response = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;

        String requestType = request.getParameter("request");

        switch (requestType) {
            case "Food":
                Food();
                break;
            case "Drink":
                drink();
                break;
            case "foodSession":
                setSession();
                break;
            case "updateFood":
                updateFood();
                break;
            case "drinkSession":
                setDrinkSession();
                break;
            case "updateDrink":
                updateDrink();
                break;
            case "deleteFood":
                deleteFood();
                break;
            case "deleteDrink":
                deleteDrink();
                break;
            case "method":
                method();
                break;
            case "banUser":
                banUser();
                break;
        }
    }

    private void banUser() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        HttpSession session = request.getSession(true);
        ArrayList<User> userList = new ArrayList<>();
        if (session.getAttribute("userlist") != null) {
            userList = (ArrayList<User>) session.getAttribute("userlist");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }
        String userID = request.getParameter("userid");
        int id = Integer.parseInt(userID);
        User user = null;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getUserID() == id) {
                user = userList.get(i);
            }
        }
        try {
            user.banUser();
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("admin.jsp");

    }

    private void setUserSession() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        HttpSession session = request.getSession(true);
        ArrayList<User> userList = new ArrayList<>();
        ResultSet rs = null;
        sql = "Select * from usertable";
        String firstname, lastname, email;
        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {
                userID = rs.getInt(1);
                email = rs.getString(2);
                firstname = rs.getString(4);
                lastname = rs.getString(5);
                User user = new User(firstname, lastname, email, userID);
                userList.add(user);

            }

            session.setAttribute("userlist", userList);

        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void deleteDrink() throws IOException, ServletException {
        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        HttpSession session = request.getSession(true);
        ArrayList<Drink> drinkList = new ArrayList<>();
        String drinkID = request.getParameter("drinkid");
        Drink drink = null;

        if (session.getAttribute("drinkList") != null) {
            drinkList = (ArrayList<Drink>) session.getAttribute("drinkList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }
        int id = Integer.parseInt(drinkID);
        for (int i = 0; i < drinkList.size(); i++) {

            if (drinkList.get(i).getId() == id) {
                drink = drinkList.get(i);

            }

            try {
                drink.deleteDrink();
            } catch (SQLException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
            }
            response.sendRedirect("admin.jsp");

        }

    }

    private void deleteFood() throws ServletException, IOException {
        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        Food food = null;
        String foodID = request.getParameter("foodid");
        ArrayList<Food> foodList = new ArrayList<>();
        if (session.getAttribute("foodList") != null) {
            foodList = (ArrayList<Food>) session.getAttribute("foodList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }
        int id = Integer.parseInt(foodID);

        for (int i = 0; i < foodList.size(); i++) {
            if (foodList.get(i).getId() == id) {
                food = foodList.get(i);
            }
        }

        try {
            food.deleteFood();
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("admin.jsp");

    }

    private void setDrinkSession() throws ServletException, IOException {
        int drinkID, calories;
        String drinkName;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        HttpSession session = request.getSession(true);
        ArrayList<Drink> drinkList = new ArrayList<>();
        ResultSet rs = null;
        sql = "Select * from Drink";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {
                drinkID = rs.getInt(1);
                drinkName = rs.getString(2);
                calories = rs.getInt(3);
                protein = rs.getDouble(4);
                carbohydrates = rs.getDouble(5);
                sugar = rs.getDouble(6);
                fat = rs.getDouble(7);
                satFat = rs.getDouble(8);
                salt = rs.getDouble(9);
                userID = rs.getInt(10);
                Drink drink = new Drink(drinkID, drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID);
                drinkList.add(drink);
            }

            session.setAttribute("drinkList", drinkList);

        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void updateDrink() throws ServletException, IOException {

        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        HttpSession session = request.getSession(true);
        ArrayList<Drink> drinkList = new ArrayList<>();

        if (session.getAttribute("drinkList") != null) {
            drinkList = (ArrayList<Drink>) session.getAttribute("drinkList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }

        drinkName = request.getParameter("drinkName");
        calories = request.getParameter("dcalories");
        protein = request.getParameter("dprotein");
        carbohydrates = request.getParameter("dcarbohydrates");
        sugar = request.getParameter("dsugar");
        fat = request.getParameter("dfat");
        satFat = request.getParameter("dsatFat");
        salt = request.getParameter("dsalt");
        String drinkID = request.getParameter("drinkid");
        int id = Integer.parseInt(drinkID);

        int dCalories = Integer.parseInt(calories);
        double dProtein = Double.parseDouble(protein);
        double dCarbohydrates = Double.parseDouble(carbohydrates);
        double dSugar = Double.parseDouble(sugar);
        double dFat = Double.parseDouble(fat);
        double dSatFat = Double.parseDouble(satFat);
        double dSalt = Double.parseDouble(salt);
        Drink drink = null;

        for (int i = 0; i < drinkList.size(); i++) {

            if (drinkList.get(i).getId() == id) {
                drink = drinkList.get(i);

            }

        }

        try {
            drink.updateDrinkName(drinkName);
            drink.updateCalories(dCalories);
            drink.updateCarbohydrates(dCarbohydrates);
            drink.updateSugar(dSugar);
            drink.updateFat(dFat);
            drink.updateSatFat(dSatFat);
            drink.updateSalt(dSalt);
            drink.updateProtein(dProtein);
            setDrinkSession();
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setSession() throws ServletException, IOException {

        int foodID, calories;
        String foodName, mealType;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        HttpSession session = request.getSession(true);
        ArrayList<Food> foodList = new ArrayList<>();
        ResultSet rs = null;
        sql = "Select * from food;";

        rs = databaseConnection.getData(sql, connection);
        try {
            while (rs.next()) {

                foodID = rs.getInt(1);
                foodName = rs.getString(2);
                mealType = rs.getString(3);
                calories = rs.getInt(4);
                protein = rs.getDouble(5);
                carbohydrates = rs.getDouble(6);
                sugar = rs.getDouble(7);
                fat = rs.getDouble(8);
                satFat = rs.getDouble(9);
                salt = rs.getDouble(10);
                userID = rs.getInt(11);

                Food food = new Food(foodID, foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID);

                foodList.add(food);

            }

            session.setAttribute("foodList", foodList);
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void Food() throws ServletException, IOException {
        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        //drop down for mealType breakfast,lunch,dinner,other
        foodName = request.getParameter("foodName");
        mealType = request.getParameter("mealType");
        calories = request.getParameter("calories");
        protein = request.getParameter("protein");
        carbohydrates = request.getParameter("carbohydrates");
        sugar = request.getParameter("sugar");
        fat = request.getParameter("fat");
        satFat = request.getParameter("satFat");
        salt = request.getParameter("salt");
        int userID = 0;
        int fCalories = Integer.parseInt(calories);
        double fProtein = Double.parseDouble(protein);
        double fCarbohydrates = Double.parseDouble(carbohydrates);
        double fSugar = Double.parseDouble(sugar);
        double fFat = Double.parseDouble(fat);
        double fSatFat = Double.parseDouble(satFat);
        double fSalt = Double.parseDouble(salt);

        //used to insert food in to the database 
        //INSERT INTO food VALUES (foodName, mealType, calories, carbohydrates, sugar, fat, satFat, salt);
        try {
            Food food = new Food(foodName, mealType, fCalories, fProtein, fCarbohydrates, fSugar, fFat, fSatFat, fSalt, userID);
            food.createFood();
            response.sendRedirect("admin.jsp");
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void updateFood() throws ServletException, IOException {

        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        ArrayList<Food> foodList = new ArrayList<>();;
        if (session.getAttribute("foodList") != null) {
            foodList = (ArrayList<Food>) session.getAttribute("foodList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }

        //drop down for mealType breakfast,lunch,dinner,other
        foodName = request.getParameter("foodName");
        mealType = request.getParameter("mealType");
        calories = request.getParameter("calories");
        protein = request.getParameter("protein");
        carbohydrates = request.getParameter("carbohydrates");
        sugar = request.getParameter("sugar");
        fat = request.getParameter("fat");
        satFat = request.getParameter("satFat");
        salt = request.getParameter("salt");
        String foodID = request.getParameter("foodid");
        int id = Integer.parseInt(foodID);
        int fCalories = Integer.parseInt(calories);
        double fProtein = Double.parseDouble(protein);
        double fCarbohydrates = Double.parseDouble(carbohydrates);
        double fSugar = Double.parseDouble(sugar);
        double fFat = Double.parseDouble(fat);
        double fSatFat = Double.parseDouble(satFat);
        double fSalt = Double.parseDouble(salt);
        Food food = null;

        for (int i = 0; i < foodList.size(); i++) {
            if (foodList.get(i).getId() == id) {
                food = foodList.get(i);
            }
        }

        try {
            food.updateFoodName(foodName);
            food.updateMealType(mealType);
            food.updateCalories(fCalories);
            food.updateCarbohydrates(fCarbohydrates);
            food.updateSugar(fSugar);
            food.updateFat(fFat);
            food.updateSatFat(fSatFat);
            food.updateSalt(fSalt);
            food.updateProtein(fProtein);

            setSession();
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void drink() throws ServletException, IOException {

        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        drinkName = request.getParameter("drinkName");
        calories = request.getParameter("dcalories");
        protein = request.getParameter("dprotein");
        carbohydrates = request.getParameter("dcarbohydrates");
        sugar = request.getParameter("dsugar");
        fat = request.getParameter("dfat");
        satFat = request.getParameter("dsatFat");
        salt = request.getParameter("dsalt");

        int dCalories = Integer.parseInt(calories);
        double dProtein = Double.parseDouble(protein);
        double dCarbohydrates = Double.parseDouble(carbohydrates);
        double dSugar = Double.parseDouble(sugar);
        double dFat = Double.parseDouble(fat);
        double dSatFat = Double.parseDouble(satFat);
        double dSalt = Double.parseDouble(salt);

        try {
            Drink drink = new Drink(drinkName, dCalories, dProtein, dCarbohydrates, dSugar, dFat, dSatFat, dSalt, 0);
            drink.createDrink();
            response.sendRedirect("admin.jsp");
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void method() throws ServletException, IOException {
        setSession();
        setDrinkSession();
        setUserSession();
        response.sendRedirect("admin.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
