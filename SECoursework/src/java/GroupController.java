/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.DatabaseConnection;
import classes.Group;
import classes.GroupGoal;
import classes.GroupList;
import classes.NewsFeed;
import classes.User;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Josh
 */
@WebServlet(urlPatterns = {"/GroupController"})
public class GroupController extends HttpServlet {

    HttpServletRequest request = null;
    HttpServletResponse response = null;
    GroupList gList = new GroupList();
    NewsFeed feed = new NewsFeed();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.request = request;
        this.response = response;

        String requestType = request.getParameter("request");

        switch (requestType) {
            case "Create":
                CreateGroup();
                break;
            case "Delete":
                break;
            case "EmailInvite":
                inviteUser();
                break;
            case "Update":
                gList.updateList();
                response.sendRedirect("viewgroup.jsp?id=" + request.getParameter("id"));
                break;
            case "AddGoalProgress":
                addGroupGoalProgress();
                break;
            case "JoinGroup":
                Join();
                break;
            case "LeaveGroup":
                Leave();
                break;
            case "Search":
                Search();
                break;
            case "Edit":
                Edit();
                break;
                    
               

        }

        response.getOutputStream().flush();

    }
    
    private void Edit() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp?error=10");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp?error=11");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("id"));
        String description = request.getParameter("description");

        ArrayList<Group> list = gList.getGroupList();

        Group g = null;
        try {

            for (Group group : list) {
                if (group.getGroupID() == groupID) {
                    g = group;
                    g.updateDescription(description);
                    response.sendRedirect("viewgroup.jsp?id=" + groupID);
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect("profile.jsp");
        }

    }

    private void inviteUser() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp?error=10");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp?error=11");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("id"));

        ArrayList<Group> list = gList.getGroupList();

        Group g = null;
        try {

            for (Group group : list) {
                if (group.getGroupID() == groupID) {
                    g = group;
                    user.joinGroup(groupID);
                    g.addUser(user);
                    UpdateUserGroups();
                    response.sendRedirect("viewgroup.jsp?id=" + groupID);
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect("profile.jsp");
        }

    }

    private void CreateGroup() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        String name = request.getParameter("name");
        String description = request.getParameter("description");

        String sql = "SELECT * from UserGroup where groupname = '"
                + name + "';";
        try {

            rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                response.sendRedirect("creategroup.jsp?error=1");
                return;
            }
        } catch (SQLException e) {

        }

        Group group = new Group(name, description, user.getUserID());

        try {
            group.CreateGroup();
            UpdateUserGroups();
            group.setCreator(user);
            gList.updateList();
            response.sendRedirect("editgroup.jsp?id=" + group.getGroupID());
            return;
        } catch (SQLException ex) {
            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void UpdateUserGroups() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;
        ResultSet rs2 = null;

        ArrayList<Group> userCreatedGroups = new ArrayList<>();
        ArrayList<Group> userMemberGroups = new ArrayList<>();

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        String sql = "Select * from groupmembers where userid = " + user.getUserID();
        rs = databaseConnection.getData(sql, connection);

        int groupID, groupGoalID, goalProgress;
        String groupName, groupDescription, goalStatus;

        try {
            while (rs.next()) {

                sql = "Select * from Usergroup where groupid = " + rs.getInt(2);
                rs2 = databaseConnection.getData(sql, connection);

                while (rs2.next()) {
                    groupID = rs2.getInt(1);
                    groupName = rs2.getString(2);
                    groupGoalID = rs2.getInt(4);
                    groupDescription = rs2.getString(5);
                    goalStatus = rs2.getString(7);
                    goalProgress = rs2.getInt(6);

                    if (goalStatus == null) {
                        Group group = new Group(groupName, groupDescription, user.getUserID());
                        userMemberGroups.add(group);
                    } else {

                        ResultSet goalData;
                        String goalSQL = "Select * from groupgoal where groupgoalid = " + groupGoalID;
                        goalData = databaseConnection.getData(goalSQL, connection);

                        int id, gID, start, target, progress;
                        String type, startDate, completionDate, status;

                        GroupGoal groupGoal = null;

                        if (goalData.next()) {
                            id = goalData.getInt(1);
                            gID = goalData.getInt(2);
                            type = goalData.getString(3);
                            start = goalData.getInt(4);
                            target = goalData.getInt(5);
                            startDate = goalData.getString(6);
                            completionDate = goalData.getString(7);
                            status = goalData.getString(8);
                            progress = goalData.getInt(9);
                            groupGoal = new GroupGoal(type, target, startDate, completionDate, gID, id, progress, status, start);
                        }

                        //public GroupGoal(String goalType, int target, String start, String end, int groupID, int goalID, int progress, String status, int startValue)
                        Group group = new Group(groupName, groupDescription, user.getUserID(), groupID, groupGoal);
                        userMemberGroups.add(group);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "Select * from Usergroup where userid = " + user.getUserID();
        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                groupID = rs.getInt(1);
                groupName = rs.getString(2);
                groupGoalID = rs.getInt(4);
                groupDescription = rs.getString(5);
                goalStatus = rs.getString(7);
                goalProgress = rs.getInt(6);

                if (goalStatus == null) {
                    Group group = new Group(groupName, groupDescription, user.getUserID());
                    userCreatedGroups.add(group);
                } else {
                    Group group = new Group(groupName, groupDescription, user.getUserID(), groupID, groupGoalID, goalProgress, goalStatus);
                    userCreatedGroups.add(group);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        user.setUserCreatedGroups(userCreatedGroups);
        user.setUserMemberGroups(userMemberGroups);
        gList.updateList();

    }

    public void Join() throws ServletException, IOException {
        
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("groupID"));
        
        ArrayList<Group> list = gList.getGroupList();

        Group g = null;
        try {

            for (Group group : list) {
                if (group.getGroupID() == groupID) {
                    g = group;
                    user.joinGroup(groupID);
                    g.addUser(user);
                    UpdateUserGroups();
                    gList.updateList();
                    response.sendRedirect("viewgroup.jsp?id=" + groupID);
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect("profile.jsp");
        }
        
    }
    
    public void Search() throws ServletException, IOException {
        
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        
        String search = request.getParameter("search");
        
        String sql = "Select * from usergroup where groupname like '%"+search+"%'";
        
        ResultSet rs = databaseConnection.getData(sql, connection);
        
        ArrayList<Group> searchResults = new ArrayList();

        
        try {
            while(rs.next())
            {
                int id = rs.getInt(1);
                
                for(Group group : gList.getGroupList())
                {
                    if(id == group.getGroupID())
                        searchResults.add(group);
                }
            }
        }catch(Exception e)
        {
            
        }
        
        session.setAttribute("searchResults", searchResults);
        response.sendRedirect("search.jsp");
    }
    
    public void Leave() throws ServletException, IOException {
        
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("groupID"));
        
        ArrayList<Group> list = gList.getGroupList();

        Group g = null;
        try {

            for (Group group : list) {
                if (group.getGroupID() == groupID) {
                    g = group;
                    user.leaveGroup(groupID);
                    g.removeUser(user);
                    UpdateUserGroups();
                    gList.updateList();
                    response.sendRedirect("viewgroup.jsp?id=" + groupID);
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            response.sendRedirect("profile.jsp");
        }
    }
    
    public void addGroupGoalProgress() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("groupID"));
        int groupGoalID = Integer.parseInt(request.getParameter("id"));
        int userID = user.getUserID();
        double progressValue = Double.parseDouble(request.getParameter("value"));
        double targetValue = Double.parseDouble(request.getParameter("target"));
        double currentValue = Double.parseDouble(request.getParameter("current"));

        Calendar cal = Calendar.getInstance();
        int intMonth = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int intDay = cal.get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(intDay);
        String month = String.valueOf(intMonth);

        if (intDay < 10) {
            day = "0" + intDay;
        }

        if (intMonth < 10) {
            month = "0" + intMonth;
        }

        String date = (day + "/" + month + "/" + year);

        String sql = "INSERT INTO groupgoalprogress(groupid, groupgoalid, userid, progressvalue, entrydate) VALUES (" + groupID + ", " + groupGoalID + ", " + userID + ", " + progressValue + ", '" + date + "');";
        databaseConnection.runUpdateQuery(sql, connection);

        if ((currentValue + progressValue) < targetValue) {
            sql = "UPDATE usergroup SET goalprogress=" + (currentValue + progressValue) + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
        } else {
            sql = "UPDATE usergroup SET goalprogress=" + (currentValue + progressValue) + ", goalstatus='Completed' WHERE groupgoalid =" + groupGoalID;
        }

        databaseConnection.runUpdateQuery(sql, connection);

        if ((currentValue + progressValue) < targetValue) {
            sql = "UPDATE groupgoal SET goalprogress=" + (currentValue + progressValue) + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
        } else {
            sql = "UPDATE groupgoal SET goalprogress=" + (currentValue + progressValue) + ", goalstatus='Completed', completiondate='" + date + "'  WHERE groupgoalid =" + groupGoalID;
        }

        databaseConnection.runUpdateQuery(sql, connection);

        gList.updateList();

        if ((currentValue + progressValue) > targetValue) {
            response.sendRedirect("EmailController?request=EmailGroupCompletion&id=" + groupID);
        } else {
            response.sendRedirect("EmailController?request=EmailGroup&id=" + groupID);
        }
        
        return;

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
