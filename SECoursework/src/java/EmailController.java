/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.DatabaseConnection;
import classes.Mailer;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Josh
 */
@WebServlet(urlPatterns = {"/EmailController"})
public class EmailController extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String requestType = request.getParameter("request");

        this.request = request;
        this.response = response;

        switch (requestType) {
            case "EmailUser":
                emailUser();
                break;
            case "EmailGroup":
                emailGroup();
                break;
            case "EmailGroupCompletion":
                emailGroupCompletion();
                break;
            case "GoalProgress":
                emailUserProgress();
                break;
            case "GoalCompletion":
                emailUserCompletion();
                break;
            case "UserLogin":
                //emailUserCompletion();
                break;
            case "InviteUser":
                inviteUser();
                break;
        }

    }

    private void emailUserCompletion() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        String userID = request.getParameter("userid");

        String subject = "Goal Completion!";
        String msg = "Hi, \n\nWe would like to congratulate you on completing your"
                + " goal!\n\nNow that you've completed this goal, we suggest"
                + " creating a new goal!\n\nTo add a new goal you can do it"
                + " from your profile page!\nClick the link below:\n\n"
                + "http://localhost:8080/SECoursework/profile.jsp\n\nYork Street Health Tracker Team.";

        String sql = "Select email from usertable where id = " + userID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                Mailer.send(rs.getString(1), subject, msg);
            }
        } catch (SQLException e) {

        }

        
        response.sendRedirect("UserController?request=bmiRedirect");
    }

    private void emailUserProgress() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        //userid=1?goalid=2?progress=20
        String userID = request.getParameter("userid");

        String subject = "Goal Progress!";
        String msg = "Hi, \n\nWe would like to congratulate you on your progress"
                + " towards you goal!\n\nYork Street Health Tracker Team.";

        String sql = "Select email from usertable where id = " + userID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                Mailer.send(rs.getString(1), subject, msg);
            }
        } catch (SQLException e) {

        }

        response.sendRedirect("profile.jsp");
    }

    private void emailUser() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        String userID = request.getParameter("userID");
        String subject = request.getParameter("subject");
        String msg = request.getParameter("msg");

        String sql = "Select email from usertable where id = " + userID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                Mailer.send(rs.getString(1), subject, msg);
            }
        } catch (SQLException e) {

        }

        response.sendRedirect("admin.jsp");
    }

    private void inviteUser() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        String userEmail = request.getParameter("email");
        String groupName = request.getParameter("groupName");
        String groupID = request.getParameter("groupID");
        String subject = "Group Invitation";
        String msg = "Hi,\n\nYou have been invited to join the group " + groupName + "."
                + "\n\nIf you wish to join this group please click the following link:"
                + "\n\nlocalhost:8080/SECoursework/GroupController?request=EmailInvite&id=" + groupID
                + "\n\nRemember you will need to be logged in to accept group invites!"
                + "\n\nYork Street Health Tracker Team";

        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(userEmail);
        boolean matchFound = m.matches();
        if (matchFound) {
            Mailer.send(userEmail, subject, msg);
        } else {
            response.sendRedirect("editgroup.jsp?id=" + groupID + "&error=5");
            return;
        }

        response.sendRedirect("editgroup.jsp?id=" + groupID);
    }

    private void emailGroup() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        String groupID = request.getParameter("id");
        String subject = "Group Goal Progress";
        String msg = "Hi, \n\nA member in your group has contributed towards"
                + " the group goal!\n\nYork Street Health Tracker Team.";

        String sql = "Select userid from groupmembers where groupid = " + groupID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (SQLException e) {
            //dont do anything
        }

        sql = "Select userid from usergroup where groupid = " + groupID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (SQLException e) {
            //dont do anything
        }

        response.sendRedirect("viewgroup.jsp?id=" + groupID);
    }

    private void emailGroupCompletion() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        String groupID = request.getParameter("id");
        String subject = "Group Goal Completion";
        String msg = "Hi, \n\nWe would like to congratulate you and your group on completing the"
                + " goal!\n\nNow that you've completed this goal, we suggest"
                + " creating a new goal!\n\nTo add a new goal you can do it"
                + " from your profile page!\nClick the link below:\n\n"
                + "http://localhost:8080/SECoursework/profile.jsp\n\nYork Street Health Tracker Team.";

        String sql = "Select userid from groupmembers where groupid = " + groupID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (SQLException e) {
            //dont do anything
        }

        sql = "Select userid from usergroup where groupid = " + groupID;

        try {

            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (SQLException e) {
            //dont do anything
        }

        response.sendRedirect("viewgroup.jsp?id=" + groupID);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
