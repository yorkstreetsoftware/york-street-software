/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 *
 * @author Josh
 */
public class Group {

    String groupName, groupDescription;
    int creatorID, groupID;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    ResultSet rs;
    int groupGoalID;
    int goalProgress;
    String goalStatus;
    GroupGoal goal;
    User creator;
    ArrayList<User> userList = new ArrayList();
    User highestContributor;

    public Group(String name, String description, int userID) {
        this.groupName = name;
        this.groupDescription = description;
        this.creatorID = userID;
        this.groupID = 0;
    }

    public Group(String name, String description, int userID, int groupID, int groupGoalID, int goalProgress, String goalStatus) {
        this.groupName = name;
        this.groupDescription = description;
        this.creatorID = userID;
        this.groupID = groupID;
        this.groupGoalID = groupGoalID;
        this.goalProgress = goalProgress;
        this.goalStatus = goalStatus;

    }

    public Group(String name, String description, int userID, int groupID, GroupGoal goal) {
        this.groupName = name;
        this.groupDescription = description;
        this.creatorID = userID;
        this.groupID = groupID;
        this.goal = goal;

    }

    public void CreateGroup() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO UserGroup (groupname, userid, description) "
                + "VALUES ('" + groupName + "', " + creatorID + ", '" + groupDescription + "');";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public int getGroupID() throws ServletException, SQLException {

        if (groupID != 0) {
            return groupID;
        } else {
            connection = databaseConnection.getConnection();
            String sql = "SELECT groupid from UserGroup where groupname = '"
                    + groupName + "';";
            try {
                rs = databaseConnection.getData(sql, connection);
                if (rs.next()) {
                    groupID = rs.getInt(1);
                    return groupID;
                }
            } catch (Exception e) {

            }
        }

        return 0;

    }

    public int getGroupGoalID() {
        return goal.getGoalID();
    }

    public double getGoalProgress() {
        return goal.getGoalProgress();
    }

    public String getGoalStatus() {
        return goal.getGoalStatus();
    }

    public GroupGoal getGoal() {
        return goal;
    }

    public void addUser(User u) {
        userList.add(u);
    }

    public void setCreator(User u) {
        this.creator = u;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public User getCreator() {
        return creator;
    }

    public int getNumberOfMembers() {
        return userList.size();
    }

    public void setGroupGoal(GroupGoal g) {
        this.goal = g;

        String sql = "UPDATE usergroup SET groupgoalid=" + goal.getGoalID() + ", goalprogress= " + goal.getGoalProgress() + ", goalstatus='" + goal.getGoalStatus() + "' WHERE groupid =" + groupID;
        databaseConnection.runUpdateQuery(sql, connection);

    }
    
    public void updateDescription(String desc) {
        
        String sql = "UPDATE usergroup SET description='" + desc + "' WHERE groupid = " + this.groupID;
        databaseConnection.runUpdateQuery(sql, connection);

    }

    public User getHighestContributor() throws ServletException {
        ArrayList<GroupGoalProgress> progressLog = goal.getProgressList();

        int progress = 0;
        int highestProgress = 0;

        for (User u : userList) {
            for (GroupGoalProgress p : progressLog) {
                if (u.getUserID() == p.getUserID()) {
                    progress += p.getProgressValue();
                }
            }
            if (progress > highestProgress) {
                highestContributor = u;
                highestProgress = progress;
            }
        }

        return highestContributor;
    }

    public void removeUser(User user) {
        try {
            String sql = "Delete from groupmembers where groupID=" + groupID + " AND userid=" + user.getUserID();
            databaseConnection.runUpdateQuery(sql, connection);
        } catch (Exception e) {

        }
    }
}
