/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Josh
 */
public class Exercise {

    protected double distance;
    protected int minutes, userID, exerciseID;
    protected String date, type;

    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;

    public Exercise(int exerciseID, int userID, double distance, String date, int time) {
       this.exerciseID = exerciseID;
        this.userID = userID;
        this.distance = distance;
        this.date = date;
        this.minutes = time;
    }
    public Exercise( int userID, double distance, String date, int time) {
       
        this.userID = userID;
        this.distance = distance;
        this.date = date;
        this.minutes = time;
    }

    public double getDistance() {
        return distance;
    }

    public String getDate() {
        return date;
    }

    public String getExerciseType() {
        return type;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getExerciseID() {
        return exerciseID;
    }
    
    public void updateDistance(double distance) throws ServletException, SQLException{
        connection = databaseConnection.getConnection();
        String sql = "UPDATE exercise set distance="+ distance + " WHERE exerciseid="+this.exerciseID + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }
    
    public void updateTime(int time) throws ServletException, SQLException{
        connection = databaseConnection.getConnection();
        String sql = "UPDATE exercise set time="+ time + " WHERE exerciseid="+this.exerciseID + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
        
    }
    

    public void insertExercise() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
         String sql = "INSERT INTO exercise(userid, type, distance, time, date) VALUES ("+userID+", '" + type + "', " + distance + ", " + minutes + ", '" + date + "');";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

}
