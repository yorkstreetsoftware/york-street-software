/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class FoodList {

    static ArrayList<Food> foodList = new ArrayList<>();

    public FoodList() {

        if (foodList.isEmpty()) {
            try {
                populateList();
            } catch (ServletException ex) {
                Logger.getLogger(FoodList.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public ArrayList<Food> getFoodList() {
        return foodList;
    }

    public  void updateList(){
        try {
            populateList();
        } catch (ServletException ex) {
            Logger.getLogger(FoodList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void populateList() throws ServletException {

        foodList.clear();
        ResultSet rs = null;
        int foodID, calories;
        String foodName, mealType;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        sql = "Select * from food WHERE userid = 0 order by foodid asc;";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                foodID = rs.getInt(1);
                foodName = rs.getString(2);
                mealType = rs.getString(3);
                calories = rs.getInt(4);
                protein = rs.getDouble(5);
                carbohydrates = rs.getDouble(6);
                sugar = rs.getDouble(7);
                fat = rs.getDouble(8);
                satFat = rs.getDouble(9);
                salt = rs.getDouble(10);
                userID = rs.getInt(11);

                Food food = new Food(foodID, foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt,userID);

                foodList.add(food);

            }
        } catch (SQLException ex) {
            Logger.getLogger(FoodList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
