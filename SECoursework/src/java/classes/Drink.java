/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class Drink {

    String drinkName, date;
    double protein, carbohydrates, sugar, fat, satFat, salt;
    int id, userID, calories;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;

    public Drink(String drinkName, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID) {

        this.drinkName = drinkName;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.sugar = sugar;
        this.fat = fat;
        this.satFat = satFat;
        this.salt = salt;
        this.userID = userID;
    }
     public Drink(int id, String drinkName, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID) {
        this.id = id;
        this.drinkName = drinkName;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.sugar = sugar;
        this.fat = fat;
        this.satFat = satFat;
        this.salt = salt;
        this.userID = userID;

    }
     public Drink(int id, String drinkName, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID, String date) {
        this.id = id;
        this.drinkName = drinkName;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.sugar = sugar;
        this.fat = fat;
        this.satFat = satFat;
        this.salt = salt;
        this.userID = userID;
        this.date = date;

    }
     
      public void deleteDrink() throws ServletException, SQLException{
        
        connection = databaseConnection.getConnection();
        String sql = "delete FROM drink where drinkid= "+id+";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
       // delete FROM food WHERE foodid=id;
    }

    public void createDrink() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();

        String sql = "INSERT INTO drink (drinkname, calories, protein, carbohydrates, sugar, fat, satfat, salt, userid)"
                + "VALUES ('" + drinkName + "', " + calories + ",  " + protein + ", " + carbohydrates + ", " + sugar + ", " + fat + ", " + satFat + ", " + salt + ", " + userID + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public String getDate() {
        return date;
    }
    

    public String getDrinkName() {
        return drinkName;
    }

    public int getCalories() {
        return calories;
    }

    public double getProtein() {
        return protein;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getSugar() {
        return sugar;
    }

    public double getFat() {
        return fat;
    }

    public double getSatFat() {
        return satFat;
    }

    public double getSalt() {
        return salt;
    }

    public int getId() {
        return id;
    }

    public void updateDrinkName(String newDrinkName) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET drinkname = '" + newDrinkName + "' WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }
    
    public void updateCalories(int newCalories) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET calories = " + newCalories + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

     public void updateProtein(double newProtein) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET protein = " + newProtein + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }
     public void updateCarbohydrates(double newCarbohydrates) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET carbohydrates = " + newCarbohydrates + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void updateSugar(double newSugar) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET sugar = " + newSugar + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateFat(double newFat) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET fat = " + newFat + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateSatFat(double newSatFat) throws SQLException, ServletException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET satfat = " + newSatFat + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void updateSalt(double newSalt) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE drink SET salt = " + newSalt + " WHERE drinkid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }
   
    

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setSatFat(double satFat) {
        this.satFat = satFat;
    }

    public void setSalt(double salt) {
        this.salt = salt;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
   

}
