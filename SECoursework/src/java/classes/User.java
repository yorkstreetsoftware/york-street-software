package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class User {

    String firstName, lastName, email, password, sql, gender;
    Calendar lastLogInDate;
    int daysToNotify, calorieCount;
    int userID, age, heightInCM;
    double weightInKG, BMI;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    MD5Encryption encrypt = new MD5Encryption();
    boolean completedProfile, isStaff;
    ArrayList<Goal> userGoals = new ArrayList<>();
    ArrayList<Goal> userNotificationGoals = new ArrayList<>();
    ArrayList<Group> userCreatedGroups = new ArrayList<>();
    ArrayList<Group> userMemberGroups = new ArrayList<>();
    FoodList foodList;
    DrinkList drinkList;
    GroupList groupList;
    ArrayList<Food> usersConsumedFood = new ArrayList<>();
    ArrayList<Food> customFoodList = new ArrayList<>();
    ArrayList<Drink> customDrinkList = new ArrayList<>();
    ArrayList<Drink> usersConsumedDrink = new ArrayList<>();
    ArrayList<Exercise> usersExercise = new ArrayList<>();

    String usersPhoto = null;
    double janDistance = 0;
    double febDistance = 0;
    double marDistance = 0;
    double aprDistance = 0;
    double mayDistance = 0;
    double junDistance = 0;
    double julDistance = 0;
    double augDistance = 0;
    double sepDistance = 0;
    double octDistance = 0;
    double novDistance = 0;
    double decDistance = 0;

    public User(String firstName, String surname, String email, int age, String password) {

        this.foodList = new FoodList();
        this.drinkList = new DrinkList();
        this.groupList = new GroupList();
        this.firstName = firstName;
        this.lastName = surname;
        this.email = email;
        this.age = age;
        this.password = encrypt.cryptWithMD5(encrypt.cryptWithMD5(password));
        completedProfile = false;
        isStaff = false;

    }

    public User(String firstName, String surname, String email, int id) {

        this.firstName = firstName;
        this.lastName = surname;
        this.email = email;
        this.userID = id;

    }

    public User(String firstName, String surname, String email, int age, String password,
            int id, String gender, double weight, int height, double bmi, boolean staff, String image) {
        this.foodList = new FoodList();
        this.drinkList = new DrinkList();
        this.groupList = new GroupList();
        this.firstName = firstName;
        this.lastName = surname;
        this.email = email;
        this.password = encrypt.cryptWithMD5(encrypt.cryptWithMD5(email));
        this.age = age;
        this.userID = id;
        this.gender = gender;
        this.weightInKG = weight;
        this.heightInCM = height;
        this.BMI = bmi;
        completedProfile = true;
        this.isStaff = staff;
        this.usersPhoto = image;

    }
    public void banUser() throws ServletException, SQLException{
        connection = databaseConnection.getConnection();
        String sql = "UPDATE usertable SET isbanned = ' TRUE ' WHERE id  ='" + this.userID + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public ArrayList<Goal> getUserNotificationGoals() {
        return userNotificationGoals;
    }
    
    
    public void setUserNotificationGoals(ArrayList<Goal> userNotificationGoals) {
        this.userNotificationGoals = userNotificationGoals;
    }

    public void goalDatesComparison(String date) {

    }

    public int getDaysToNotify() {
        return daysToNotify;
    }

    public void updateUserDays(int days) throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "UPDATE usertable SET notificationdays  = '" + days + "' WHERE email ='" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void setDaysToNotify(int daysToNotify) {
        this.daysToNotify = daysToNotify;
    }

    //update users login date to today
    public void updateLoginDate(Date date) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE usertable SET lastlogin  = '" + date + "' WHERE email ='" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    //set users login date to today
    public void setLoginDate(Calendar date) {

        lastLogInDate = date;

    }

    public boolean checkDate() {
        Calendar c = lastLogInDate;
        System.out.println("TEST " + c);
        //System.out.println("TEST 222   : " + lastLogInDate );
        Calendar cal = Calendar.getInstance();

        c.add(Calendar.DATE, daysToNotify);

        Date x = c.getTime();

        Date y = cal.getTime();

        if ((y.after(x)) || (y.equals(x))) {
            try {
                updateLoginDate(y);
            } catch (ServletException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {
            return false;
        }

    }

    public String getUsersPhoto() {
        return usersPhoto;
    }

    public void setUsersPhoto(String usersPhoto) {
        this.usersPhoto = usersPhoto;
    }

    public void updateUsersPhoto(String usersPhoto) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE usertable SET image  = '" + usersPhoto + "' WHERE id =" + this.userID + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateImage(String i) throws ServletException, SQLException {
        usersPhoto = i;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET image= " + i + " WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public double getJanDistance() {
        return janDistance;
    }

    public double getFebDistance() {
        return febDistance;
    }

    public double getMarDistance() {
        return marDistance;
    }

    public double getAprDistance() {
        return aprDistance;
    }

    public double getMayDistance() {
        return mayDistance;
    }

    public double getJunDistance() {
        return junDistance;
    }

    public double getJulDistance() {
        return julDistance;
    }

    public double getAugDistance() {
        return augDistance;
    }

    public double getSepDistance() {
        return sepDistance;
    }

    public double getOctDistance() {
        return octDistance;
    }

    public double getNovDistance() {
        return novDistance;
    }

    public double getDecDistance() {
        return decDistance;
    }

    public ArrayList<Exercise> getUsersExercise() {
        return usersExercise;
    }

    public void setUsersExercise(ArrayList<Exercise> usersExercise) {
        this.usersExercise = usersExercise;
    }

    public void setUsersConsumedDrink(ArrayList<Drink> usersConsumedDrink) {
        this.usersConsumedDrink = usersConsumedDrink;
    }

    public ArrayList<Drink> getUsersConsumedDrink() {
        return usersConsumedDrink;
    }

    public void setCustomDrinkList(ArrayList<Drink> customDrinkList) {
        this.customDrinkList = customDrinkList;
    }

    public ArrayList<Drink> getDrinkList() {
        return drinkList.getDrinkList();
    }

    public ArrayList<Drink> getCustomDrinkList() {
        return customDrinkList;
    }

    public ArrayList<Food> getFoodList() {
        return foodList.getFoodList();
    }

    public ArrayList<Food> getCustomFoodList() {
        return customFoodList;
    }

    public void setCustomFoodList(ArrayList<Food> customFoodList) {
        this.customFoodList = customFoodList;
    }

    public ArrayList<Food> getUsersConsumedFood() {
        return usersConsumedFood;
    }

    public void setUsersConsumedFood(ArrayList<Food> usersConsumedFood) {
        this.usersConsumedFood = usersConsumedFood;
    }

    public void createUser() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO UserTable (Email, Password, Firstname, Lastname, "
                + "IsBanned, age, completedprofile, isStaff) "
                + "VALUES ('" + email + "', '" + password + "', '" + firstName + "', '" + lastName + "', " + false + ", " + age + ", " + false + ", " + false + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void createCompleteUser() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO UserTable (Email, Password, Firstname, Lastname, IsBanned, age) "
                + "VALUES ('" + email + "', '" + password + "', '" + firstName + "', '" + lastName + "', " + false + ", " + age + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public ArrayList<Group> getGroupList() {
        return groupList.getGroupList();
    }

    public int getUserID() throws ServletException {
        ResultSet rs = null;

        if (userID != 0) {
            return userID;
        } else {
            connection = databaseConnection.getConnection();
            String sql = "SELECT id from usertable where email = '"
                    + email + "';";
            try {
                rs = databaseConnection.getData(sql, connection);
                if (rs.next()) {
                    userID = rs.getInt(1);
                    return userID;
                }
            } catch (Exception e) {

            }
        }

        return 0;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void updateWeight(double d) throws ServletException, SQLException {
        weightInKG = d;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET weight= " + d + " WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateHeight(int i) throws ServletException, SQLException {
        heightInCM = i;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET height= " + i + " WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateAge(int i) throws ServletException, SQLException {
        age = i;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET age= " + i + " WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateGender(String s) throws ServletException, SQLException {
        gender = s;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET gender= '" + s + "' WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateBMI(double d) throws ServletException, SQLException {
        BMI = d;

        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET bmi= " + d + " WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void userCompletedProfile() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        sql = "UPDATE UserTable SET completedprofile= 'true' WHERE email= '" + this.email + "';";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
        completedProfile = true;
    }

    public double getMaximumWeight() {

        double maximumBMI = (((24.9 * heightInCM) / 100) * heightInCM) / 100;
        return maximumBMI;
    }

    public double getMinimumWeight() {
        double minimumBMI = (((18.5 * heightInCM) / 100) * heightInCM) / 100;
        return minimumBMI;
    }

    public boolean hasCompletedProfile() {
        return this.completedProfile;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public int getHeightInCM() {
        return heightInCM;
    }

    public double getWeightInKG() {
        return weightInKG;
    }

    public double getBMI() {
        return BMI;
    }

    public ArrayList<Goal> getUserGoals() {
        return userGoals;
    }

    public ArrayList<Group> getUserCreatedGroups() {
        return userCreatedGroups;
    }

    public ArrayList<Group> getUserMemberGroups() {
        return userMemberGroups;
    }

    public void setUserGoals(ArrayList<Goal> userGoals) {
        this.userGoals = userGoals;
    }

    public void setUserCreatedGroups(ArrayList<Group> userCreatedGroups) {
        this.userCreatedGroups = userCreatedGroups;
    }

    public void setUserMemberGroups(ArrayList<Group> userMemberGroups) {
        this.userMemberGroups = userMemberGroups;
    }

    public boolean getIsStaff() {
        return isStaff;
    }

    public void joinGroup(int groupID) throws SQLException, ServletException {
        connection = databaseConnection.getConnection();
        String joinSQL = "INSERT INTO groupmembers (groupid, userid) "
                + "VALUES (" + groupID + ", " + userID + ");";
        databaseConnection.runUpdateQuery(joinSQL, connection);
        databaseConnection.closeConnection(connection);
    }

    public void getMonth() {

        if (!usersExercise.isEmpty()) {
            for (int i = 0; i < usersExercise.size(); i++) {

                String date = usersExercise.get(i).getDate();

                String[] split = date.split("/");

                String day = split[0];

                String month = split[1];

                String year = split[2];

                switch (month) {

                    case "01":
                        janDistance = janDistance + usersExercise.get(i).getDistance();
                        break;

                    case "02":
                        febDistance = febDistance + usersExercise.get(i).getDistance();
                        break;

                    case "03":
                        marDistance = marDistance + usersExercise.get(i).getDistance();
                        break;

                    case "04":
                        aprDistance = aprDistance + usersExercise.get(i).getDistance();
                        break;

                    case "05":
                        mayDistance = mayDistance + usersExercise.get(i).getDistance();
                        break;

                    case "06":
                        junDistance = junDistance + usersExercise.get(i).getDistance();
                        break;

                    case "07":
                        julDistance = julDistance + usersExercise.get(i).getDistance();
                        break;

                    case "08":
                        augDistance = augDistance + usersExercise.get(i).getDistance();
                        break;

                    case "09":
                        sepDistance = sepDistance + usersExercise.get(i).getDistance();
                        break;

                    case "10":
                        octDistance = octDistance + usersExercise.get(i).getDistance();
                        break;

                    case "11":
                        novDistance = novDistance + usersExercise.get(i).getDistance();
                        break;

                    case "12":
                        decDistance = decDistance + usersExercise.get(i).getDistance();
                        break;
                }
            }

        }

    }

    public int getCalorieCount(){
        return this.calorieCount;
        
    }
    
    public void setCalorieCount(int calories) {
        
        this.calorieCount= calories;
        

    }

    public void leaveGroup(int groupID) {
        try {
            for (Group g : userMemberGroups) {
                if (g.getGroupID() == groupID) {
                    userMemberGroups.remove(g);
                }
            }
        } catch (Exception e) {

        }
    }

}
