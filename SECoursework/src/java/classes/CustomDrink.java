/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class CustomDrink {
    
    String drinkName;
    double calories, protein, carbohydrates, sugar, fat, satFat, salt;
    int id;
    int userID;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    
    
    public CustomDrink(int userID, String drinkName, double calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt) {
        this.userID = userID;
        this.drinkName = drinkName;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.sugar = sugar;
        this.fat = fat;
        this.satFat = satFat;
        this.salt = salt;

    }
     public CustomDrink(int id, int userID, String drinkName, double calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt) {
        this.id = id;
        this.userID = userID;
        this.drinkName = drinkName;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.sugar = sugar;
        this.fat = fat;
        this.satFat = satFat;
        this.salt = salt;

    }
     
     public void createDrink() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();

        String sql = "INSERT INTO drink ( drinkname, calories, protein, carbohydrates, sugar, fat, satfat, salt, userid)"
                + "VALUES ('" + drinkName + "', " + calories + ",  " + protein + ", " + carbohydrates + ", " + sugar + ", " + fat + ", " + satFat + ", " + salt + "," + userID + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public String getDrinkName() {
        return drinkName;
    }

    public double getCalories() {
        return calories;
    }

    public double getProtein() {
        return protein;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getSugar() {
        return sugar;
    }

    public double getFat() {
        return fat;
    }

    public double getSatFat() {
        return satFat;
    }

    public double getSalt() {
        return salt;
    }

    public int getId() {
        return id;
    }

    public int getUserID() {
        return userID;
    }
     
     
    
    
}
