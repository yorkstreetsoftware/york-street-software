/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author Liam
 */
public class NewsFeed {
    ArrayList<FeedMessage> feed = new ArrayList();
    
    public NewsFeed() {
            if (feed.isEmpty()) {
            try {
                populateList();
            } catch (ServletException | SQLException ex) {
                Logger.getLogger(NewsFeed.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
  
    private void populateList() throws ServletException, SQLException {
        feed.clear();
        ResultSet rs = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        
        String tFName, tSName, tGName, tMsg;
        FeedMessage fmsg;
        
        String sql = "SELECT * FROM newsfeed;";
        rs = databaseConnection.getData(sql, connection);
        
        try {
            while(rs.next()) {
                tFName = rs.getString("firstname");
                tSName = rs.getString("lastname");
                tGName = rs.getString("groupname");
                tMsg = rs.getString("message");
                
                fmsg = new FeedMessage(tFName, tSName, tGName, tMsg);
                feed.add(fmsg);
            }
        } catch(SQLException ex) {
            
        }
        
    }
      
    public void updateList() {
        try {
            populateList();
        } catch (ServletException | SQLException ex) {
            Logger.getLogger(NewsFeed.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<FeedMessage> getFeed() {
        return feed;
    }
}
