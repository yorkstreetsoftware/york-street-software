/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 *
 * @author Josh
 */
public class GroupGoalProgress {

    int groupProgressID, groupID, userID, progressValue, groupGoalID;
    String entryDate;

    public GroupGoalProgress(int gID, int uID, int pValue, int gGoalID, String date) {
        this.groupProgressID = 0;
        this.groupID = gID;
        this.userID = uID;
        this.progressValue = pValue;
        this.groupGoalID = gGoalID;
        this.entryDate = date;
    }
    
    public GroupGoalProgress(int progressID, int gID, int uID, int pValue, int gGoalID, String date) {
        this.groupProgressID = progressID;
        this.groupID = gID;
        this.userID = uID;
        this.progressValue = pValue;
        this.groupGoalID = gGoalID;
        this.entryDate = date;
    }

    public int getGroupProgressID() {
        return groupProgressID;
    }

    public int getGroupID() {
        return groupID;
    }

    public int getUserID() {
        return userID;
    }

    public int getProgressValue() {
        return progressValue;
    }

    public int getGroupGoalID() {
        return groupGoalID;
    }

    public String getEntryDate() {
        return entryDate;
    }

}
