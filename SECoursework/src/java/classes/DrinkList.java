/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class DrinkList {
    
    static ArrayList<Drink> drinkList = new ArrayList<>();

    public DrinkList() {
        
        
        
        if (drinkList.isEmpty()){
            try {
                populateList();
            } catch (ServletException ex) {
                Logger.getLogger(DrinkList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public  ArrayList<Drink> getDrinkList() {
        return drinkList;
    }
    
    
    public void updateList(){
        
        try {
            populateList();
        } catch (ServletException ex) {
            Logger.getLogger(DrinkList.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    private void populateList() throws ServletException{
        drinkList.clear(); 
        ResultSet rs = null;
        int drinkID,calories;
        String drinkName;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        sql = "Select * from Drink WHERE userid = 0;";
        
        rs = databaseConnection.getData(sql, connection);
        
        
        try {
            while (rs.next()){
               
                drinkID = rs.getInt(1);
                drinkName = rs.getString(2);
                calories = rs.getInt(3);
                protein = rs.getDouble(4);
                carbohydrates = rs.getDouble(5);
                sugar = rs.getDouble(6);
                fat = rs.getDouble(7);
                satFat = rs.getDouble(8);
                salt = rs.getDouble(9);
                userID = rs.getInt(10);  
                
                Drink drink = new Drink(drinkID, drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID);
                drinkList.add(drink);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DrinkList.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
}
