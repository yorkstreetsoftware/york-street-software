package classes;

import java.sql.*;
import javax.servlet.ServletException;

public class DatabaseConnection {

    public DatabaseConnection() {
    }

    public Connection getConnection() throws ServletException {

        // Using try {...} catch {...} for error control


        try {
            // Make sure the required sql driver exists
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            // Throw an error if it doesn't
            throw new ServletException(String.format(
                    "Error: Cannot find JDBC driver"), e);

        }

        // Using try {...} catch {...} for error control
        try {
            // Try and make a connection to the given database
            Connection connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/HealthTracker", "postgres", "dbpassword");
            // Return the connection once made
            return (connection);
        } catch (SQLException e) {
            // Throw an error message if any connection problems occur
            throw new ServletException(String.format(
                    "Error: Database connection failed"), e);

        }
    }

// A method for running a given query through a given connection
    public boolean runUpdateQuery(String sql, Connection connection) {

        // Using try {...} catch {...} for error control
        try {
            // Create a statement variable to be used for the sql query
            Statement statement = connection.createStatement();
            // Perform the update
            statement.executeUpdate(sql);
            // Completed successfully
            return true;
        } catch (SQLException e) {
            // Problem encountered
            return false;
        }



    }

    public ResultSet getData(String sql, Connection connection) {
        try {
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(sql);
            return rs;
        } catch (Exception e) {
            return null;
        }

    }
    
    public void closeConnection(Connection connection) throws SQLException{
        
        connection.close();
        
    }
}
