/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;


/**
 *
 * @author Liam
 */
public class FeedMessage {
    String fName, sName, gName, msg;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    
    public FeedMessage(String fName, String sName, String gName, String msg) {
        this.fName = fName;
        this.sName = sName;
        this.gName = gName;
        this.msg = msg;
    }
    
    public void createFeedPost() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO newsfeed (firstname, lastname, groupname, message) "
                + "VALUES ('" + fName + "', '" + sName + "', '" + gName + "', '" + msg + "');";
        databaseConnection.runUpdateQuery(sql, connection);

    }
    
    public String getName() {
        return fName + " " + sName;
    }
    
    public String getGroup() {
        return gName;
    }
    
    public String getMsg() {
        return msg;
    }
    
    public void setFName(String in) {
        fName = in;
    }
    
    public void setLName(String in) {
        sName = in;
    }
    
    public void setGroup(String in) {
        gName = in;
    }
    
    public void setMsg(String in) {
        msg = in;
    }
}
