package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailer {

    static boolean isInit = false;
    static Properties props = new Properties();
    static Session session;
    static String username = "yorkstreethealthtracker@gmail.com";
    static String password = "Coursework123";
    

    public static void send(String to, String subject, String msg) {

        if(!isInit)
            init();
        
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(msg);

            //3rd step)send message
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

    private static void init() {
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        
        isInit = true;
    }
    
    public static void sendWeightCompletion(int groupID) {

        String subject = "Group Weight Goal Completion";
        String msg = "Hi,\n\nCongratulations, your group has completed the group weight goal, the total weight for all users in the group has reached the target weight!\n\nYork Street Software Team.";
        
        String sql = "Select userid from groupmembers where groupid = " + groupID;

        try {
            DatabaseConnection databaseConnection = new DatabaseConnection();
            Connection connection = databaseConnection.getConnection();
            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (Exception e) {
            //dont do anything
        }
        
        try {
            sql = "Select userid from usergroup where groupid = " + groupID;
            DatabaseConnection databaseConnection = new DatabaseConnection();
            Connection connection = databaseConnection.getConnection();
            ResultSet rs = databaseConnection.getData(sql, connection);
            while (rs.next()) {
                sql = "Select email from usertable where id = " + rs.getInt(1);

                try {

                    ResultSet result = databaseConnection.getData(sql, connection);
                    if (result.next()) {
                        Mailer.send(result.getString(1), subject, msg);
                    }
                } catch (SQLException e) {

                }
            }
        } catch (Exception e) {
            //dont do anything
        }

    }
}
