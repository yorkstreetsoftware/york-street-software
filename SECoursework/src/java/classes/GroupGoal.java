/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 *
 * @author Josh
 */
public class GroupGoal {

    String goalType, startDate, endDate, goalStatus;
    double targetValue, startValue, goalProgress;
    int groupID, goalID;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    ResultSet rs;
    ArrayList<GroupGoalProgress> goalProgressList = new ArrayList();
   

    public GroupGoal(String goalType, int target, String start, int groupID, double startValue) {
        this.goalType = goalType;
        this.targetValue = target;
        this.startDate = start;
        this.groupID = groupID;
        this.goalProgress = 0;
        goalStatus = "In Progress";
        this.startValue = startValue;
    }

    public GroupGoal(String goalType, double target, String start, String end, int groupID, int goalID, double progress, String status, double startValue) {
        this.goalType = goalType;
        this.targetValue = target;
        this.startDate = start;
        this.endDate = end;
        this.groupID = groupID;
        this.goalProgress = progress;
        this.goalStatus = status;
        this.goalID = goalID;
        this.startValue = startValue;

    }

    public void CreateGoal() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO GroupGoal (groupid, goaltype, startvalue, targetvalue, startdate, goalprogress, goalstatus) "
                + "VALUES (" + groupID + ", '" + goalType + "',  " + startValue + ", " + targetValue + ", '" + startDate + "', " + startValue + ", 'In Progress');";
        databaseConnection.runUpdateQuery(sql, connection);

        sql = "Select groupgoalid from groupgoal order by groupgoalid desc";

        rs = databaseConnection.getData(sql, connection);
        if (rs.next()) {
            goalID = rs.getInt(1);
        }
    }

    public String getGoalType() {
        return goalType;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getGoalStatus() {
        return goalStatus;
    }

    public double getTargetValue() {
        return targetValue;
    }

    public double getStartValue() {
        return startValue;
    }

    public double getGoalProgress() {
        return goalProgress;
    }

    public int getGroupID() {
        return groupID;
    }

    public int getGoalID() {
        return goalID;
    }

    public void addGoalProgressList(ArrayList<GroupGoalProgress> list) {
        this.goalProgressList = list;
    }

    public ArrayList<GroupGoalProgress> getProgressList() {
        return this.goalProgressList;
    }

    public void addProgress(GroupGoalProgress p) {
        goalProgressList.add(p);
    }

}
