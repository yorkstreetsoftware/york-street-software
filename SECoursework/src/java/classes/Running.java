/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Josh
 */
public class Running extends Exercise{
    
    

    public Running(int exerciseID, int userID, double distance, String date, int time) {
        super(exerciseID, userID, distance, date, time);
        type = "Running";
    }
     public Running(int userID, double distance, String date, int time) {
        super(userID, distance, date, time);
        type = "Running";
    }
}
