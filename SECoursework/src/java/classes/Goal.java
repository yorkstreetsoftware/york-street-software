/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Josh
 */
public class Goal {
    
    String goalType, startDate, endDate, goalStatus;
    double targetValue, startValue, goalProgress;
    int userID, goalID;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    ResultSet rs;
    ArrayList<GoalProgress> goalProgressList;
    
    public Goal(String goalType, double target, String start, String end, int userID, double startVal)
    {
        this.goalType = goalType;
        this.targetValue = target;
        this.startDate = start;
        this.endDate = end;
        this.userID = userID;
        this.goalProgress = startVal;
        this.startValue = startVal;
        goalStatus = "In Progress";
        goalProgressList = new ArrayList();
    }
    
    public Goal(String goalType, double target, String start, String end, int userID, int goalID, double progress, String status, double startVal)
    {
        this.goalType = goalType;
        this.targetValue = target;
        this.startValue = startVal;
        this.startDate = start;
        this.endDate = end;
        this.userID = userID;
        this.goalProgress = progress;
        this.goalStatus = status;
        this.goalID = goalID;
        goalProgressList = new ArrayList();
        
    }
    
    public void CreateGoal() throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO UserGoals (userid, goaltype, startvalue, targetvalue, startdate, enddate, goalprogress, goalstatus) "
                + "VALUES (" + userID + ", '" + goalType + "',  " + startValue + ", " + targetValue + ", '" + startDate + "', '" + endDate + "', " + startValue + ", 'In Progress');";
        databaseConnection.runUpdateQuery(sql, connection);
        
        
        sql = "Select userid from usergoals order by goalid desc";
        
        rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                goalID = rs.getInt(1);
            }
    }

    public String getGoalType() {
        return goalType;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getGoalStatus() {
        return goalStatus;
    }

    public double getTargetValue() {
        return targetValue;
    }

    public double getStartValue() {
        return startValue;
    }

    public double getGoalProgress() {
        return goalProgress;
    }

    public int getUserID() {
        return userID;
    }

    public int getGoalID() {
        return goalID;
    }
    
    public void addGoalProgress(GoalProgress p)
    {
        goalProgressList.add(p);
    }
    
    public ArrayList<GoalProgress> getGoalProgressList()
    {
        return goalProgressList;
    }
    
    public void updateDistance(double d)
    {
       
        try {
            connection = databaseConnection.getConnection();
        } catch (ServletException ex) {
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
        }
        String sql = "UPDATE usergoals set targetvalue=" + d + " where goalid="+goalID;
        databaseConnection.runUpdateQuery(sql, connection);
        
    }
    
    public void updateDate(String date)
    {
        try {
            connection = databaseConnection.getConnection();
        } catch (ServletException ex) {
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
        }
        String sql = "UPDATE usergoals set enddate='" + date + "' where goalid="+goalID;
        databaseConnection.runUpdateQuery(sql, connection);
        
    }
}
