/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class Food {

    String foodName, mealType, date;
    double protein, carbohydrates, sugar, fat, satFat, salt;
    int id, userID,calories;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;

    public Food(String foodName, String mealType, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID) {

        this.foodName = foodName;
        this.mealType = mealType;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.salt = salt;
        this.satFat = satFat;
        this.sugar = sugar;
        this.userID = userID;
    }

    public Food(int id, String foodName, String mealType, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID) {
        this.id = id;
        this.foodName = foodName;
        this.mealType = mealType;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.salt = salt;
        this.satFat = satFat;
        this.sugar = sugar;
        this.userID = userID;
    }
    
     public Food(int id, String foodName, String mealType, int calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt, int userID, String date) {
        this.id = id;
        this.foodName = foodName;
        this.mealType = mealType;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.salt = salt;
        this.satFat = satFat;
        this.sugar = sugar;
        this.userID = userID;
        this.date = date;
    }

    public void deleteFood() throws ServletException, SQLException{
        
        connection = databaseConnection.getConnection();
        String sql = "delete FROM food where foodid= "+id+";";
                 
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
       // delete FROM food WHERE foodid=id;
    }
    public void createFood() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO food (foodname, mealtype, calories, protein, carbohydrates, sugar, fat, satfat, salt, userID)"
                + "VALUES ('" + foodName + "', '" + mealType + "', " + calories + ", " + protein + ", " + carbohydrates + ", " + sugar + ", " + fat + ", " + satFat + ", " + salt + ", " + userID + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

       // INSERT INTO food VALUES (foodName, mealType, calories, carbohydrates, sugar, fat, satFat, salt);   
    }

    public String getDate() {
        return date;
    }
    

    public String getFoodName() {
        return foodName;
    }

    public String getMealType() {
        return mealType;
    }

    public int getCalories() {
        return calories;
    }

    public double getProtein() {
        return protein;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getSugar() {
        return sugar;
    }

    public double getFat() {
        return fat;
    }

    public double getSatFat() {
        return satFat;
    }

    public double getSalt() {
        return salt;
    }

    public int getId() {
        return id;
    }

//    public int displayFoodID(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT foodid from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//        return Integer.parseInt(sql);
//    }
//
//    public void displayFoodName(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT foodname from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//    }
//
//    public String displayCalories(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT mealtye from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//        return sql;
//    }
//
//    public int displayProtein(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT protein from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//        return Integer.parseInt(sql);
//    }
//
//    public int displayCarbohydrates(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT carbohydrates from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//         return Integer.parseInt(sql);
//    }
//
//    public int displaySugar(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT sugar from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//         return Integer.parseInt(sql);
//    }
//
//    public int displayFat(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT fat from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//         return Integer.parseInt(sql);
//    }
//
//    public int displaySatFat(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT satfat from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//         return Integer.parseInt(sql);
//    }
//
//    public int displaySalt(int id) throws ServletException, SQLException {
//        connection = databaseConnection.getConnection();
//        String sql = "SELECT salt from food WHERE foodid = '" + id + ";";
//        databaseConnection.runUpdateQuery(sql, connection);
//        databaseConnection.closeConnection(connection);
//         return Integer.parseInt(sql);
//    }

//    public void editFood(String newMealType, double newCalories, double newProtein, double newCarbohydrates,
//            double newSugar, double newFat, double newSatFat, double newSalt) throws ServletException {
//        connection = databaseConnection.getConnection();
//        String sql = "UPDATE food SET mealtype = '" + newMealType + "', calories = '"
//                + newCalories + "', protein = '" + newProtein + "', carbohydrates = '"
//                + newCarbohydrates + "', sugar = '" + newSugar + "', fat = '" + newFat
//                + "', satFat = '" + newSatFat + "', salt = '" + newSalt + "; ";
//    }

    public void updateFoodName(String newFoodName) throws ServletException, SQLException {
       
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET foodname = '" + newFoodName + "' WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateMealType(String newMealType) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET mealtype = '" + newMealType + "' WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateCalories(int newCalories) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET calories = " + newCalories + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void updateProtein(double newProtein) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET protein = " + newProtein + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateCarbohydrates(double newCarbohydrates) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET carbohydrates = " + newCarbohydrates + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void updateSugar(double newSugar) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET sugar = " + newSugar + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateFat(double newFat) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET fat = " + newFat + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

    public void updateSatFat(double newSatFat) throws SQLException, ServletException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET satfat = " + newSatFat + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);

    }

    public void updateSalt(double newSalt) throws ServletException, SQLException {
        connection = databaseConnection.getConnection();
        String sql = "UPDATE food SET salt = " + newSalt + " WHERE foodid =" + this.id + ";";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection);
    }

}
