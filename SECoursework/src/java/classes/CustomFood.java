/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;

/**
 *
 * @author Max
 */
public class CustomFood {
    
    String foodName, mealType;
    double calories, protein, carbohydrates, sugar, fat, satFat, salt;
    int id, userID;
    DatabaseConnection databaseConnection = new DatabaseConnection();
    Connection connection;
    
    
    public CustomFood(int userID, String foodName, String mealType, double calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt) {
        this.userID = userID;
        this.foodName = foodName;
        this.mealType = mealType;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.salt = salt;
        this.satFat = satFat;
        this.sugar = sugar;
    }
    public CustomFood(int id, int userID, String foodName, String mealType, double calories, double protein, double carbohydrates, double sugar, double fat, double satFat, double salt) {
        this.id = id;
        this.userID = userID;
        this.foodName = foodName;
        this.mealType = mealType;
        this.calories = calories;
        this.protein = protein;
        this.carbohydrates = carbohydrates;
        this.fat = fat;
        this.salt = salt;
        this.satFat = satFat;
        this.sugar = sugar;
    }
    
    public void createCustomFood() throws ServletException, SQLException {

        connection = databaseConnection.getConnection();
        String sql = "INSERT INTO customfood (userid, foodname, mealtype, calories, protein, carbohydrates, sugar, fat, satfat, salt)"
                + "VALUES (" + userID + ",'" + foodName + "', '" + mealType + "', " + calories + ", " + protein + ", " + carbohydrates + ", " + sugar + ", " + fat + ", " + satFat + ", " + salt + ");";
        databaseConnection.runUpdateQuery(sql, connection);
        databaseConnection.closeConnection(connection); 
    }

    public String getFoodName() {
        return foodName;
    }

    public String getMealType() {
        return mealType;
    }

    public double getCalories() {
        return calories;
    }

    public double getProtein() {
        return protein;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public double getSugar() {
        return sugar;
    }

    public double getFat() {
        return fat;
    }

    public double getSatFat() {
        return satFat;
    }

    public double getSalt() {
        return salt;
    }

    public int getId() {
        return id;
    }

    public int getUserID() {
        return userID;
    }
    
    
    
    
    
}
