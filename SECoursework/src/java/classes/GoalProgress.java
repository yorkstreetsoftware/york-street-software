/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;


/**
 *
 * @author Josh
 */
public class GoalProgress {

    int progressID, userID, goalID;
    double progressValue;
    String entryDate;
    
    public GoalProgress(int pID, int gID, int uID, double value, String date)
    {
        this.progressID = pID;
        this.goalID = gID;
        this.userID = uID;
        this.progressValue = value;
        this.entryDate = date;
    }

    public int getProgressID() {
        return progressID;
    }

    public int getUserID() {
        return userID;
    }

    public double getProgressValue() {
        return progressValue;
    }

    public String getEntryDate() {
        return entryDate;
    }
    
    public int getGoalID()
    {
        return goalID;
    }
    
    
   
}
