/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author Josh
 */
public class GroupList {

    static ArrayList<Group> groupList = new ArrayList();

    public GroupList() {
        if (groupList.isEmpty()) {
            try {
                populateList();
            } catch (ServletException ex) {
                Logger.getLogger(GroupList.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public ArrayList<Group> getGroupList() {
        return groupList;
    }

    public void updateList() {
        try {
            populateList();
        } catch (ServletException ex) {
            Logger.getLogger(GroupList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void populateList() throws ServletException {

        groupList.clear();
        ResultSet rs = null;
        //group variables goes here
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();

        int groupID, groupGoalID, userID;
        double goalProgress;
        String groupName, groupDescription, goalStatus;
        Group group;

        Calendar cal = Calendar.getInstance();
        int intMonth = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int intDay = cal.get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(intDay);
        String month = String.valueOf(intMonth);

        if (intDay < 10) {
            day = "0" + intDay;
        }

        if (intMonth < 10) {
            month = "0" + intMonth;
        }

        String date = (day + "/" + month + "/" + year);

        String sql = "select * from usergroup order by groupid asc;";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                groupID = rs.getInt(1);
                groupName = rs.getString(2);
                userID = rs.getInt(3);
                groupGoalID = rs.getInt(4);
                groupDescription = rs.getString(5);
                goalStatus = rs.getString(7);
                goalProgress = rs.getDouble(6);

                if (goalStatus == null) {
                    group = new Group(groupName, groupDescription, userID);
                    groupList.add(group);
                    sql = "Select userid from groupmembers where groupid = " + groupID;

                    ResultSet groupMembers = databaseConnection.getData(sql, connection);

                    while (groupMembers.next()) {
                        sql = "SELECT * from usertable where id = " + groupMembers.getInt(1);
                        ResultSet userData = databaseConnection.getData(sql, connection);

                        while (userData.next()) {
                            int uID = userData.getInt(1);
                            String email = userData.getString(2);
                            String firstName = userData.getString(4);
                            String surname = userData.getString(5);

                            User u = new User(firstName, surname, email, uID);
                            group.addUser(u);
                        }
                    }

                    sql = "Select userid from usergroup where groupid = " + groupID;

                    ResultSet groupCreator = databaseConnection.getData(sql, connection);

                    while (groupCreator.next()) {
                        sql = "SELECT * from usertable where id = " + groupCreator.getInt(1);
                        ResultSet userData = databaseConnection.getData(sql, connection);

                        while (userData.next()) {
                            int uID = userData.getInt(1);
                            String email = userData.getString(2);
                            String firstName = userData.getString(4);
                            String surname = userData.getString(5);

                            User u = new User(firstName, surname, email, uID);
                            group.setCreator(u);
                            group.addUser(u);
                        }
                    }
                } else {
                    ResultSet goalData;
                    String goalSQL = "Select * from groupgoal where groupgoalid = " + groupGoalID;
                    goalData = databaseConnection.getData(goalSQL, connection);

                    int id, gID;
                    double start, target, progress;
                    String type, startDate, completionDate, status;

                    GroupGoal groupGoal = null;

                    if (goalData.next()) {
                        id = goalData.getInt(1);
                        gID = goalData.getInt(2);
                        type = goalData.getString(3);
                        start = goalData.getDouble(4);
                        target = goalData.getDouble(5);
                        startDate = goalData.getString(6);
                        completionDate = goalData.getString(7);
                        status = goalData.getString(8);
                        progress = goalData.getDouble(9);

                        if (type.equals("Weight")) {

                            double total = 0;
                            
                            //If goal is type Weight, total the weight of all of the users in the group

                            try {
                                if (status.equals("In Progress")) {
                                    sql = "select sum(usertable.weight) FROM usertable INNER JOIN groupmembers ON usertable.id=groupmembers.userid where groupid = " + groupID;
                                    rs = databaseConnection.getData(sql, connection);

                                    if (rs.next()) {
                                        total += rs.getDouble(1);
                                    }

                                    sql = "select sum(usertable.weight) FROM usertable INNER JOIN usergroup ON usertable.id=userid where groupid = " + groupID;
                                    rs = databaseConnection.getData(sql, connection);

                                    if (rs.next()) {
                                        total += rs.getDouble(1);
                                    }

                                    progress = total;

                                    if (target < start) {
                                        if (progress <= target) {
                                            sql = "UPDATE usergroup SET goalprogress=" + progress + ", goalstatus='Completed' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            sql = "UPDATE groupgoal SET goalprogress=" + progress + ", goalstatus='Completed', completiondate='" + date + "'  WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            Mailer.sendWeightCompletion(groupID);
                                        } else {
                                            sql = "UPDATE usergroup SET goalprogress=" + progress + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            sql = "UPDATE groupgoal SET goalprogress=" + progress + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                        }
                                    } else if (target > start) {
                                        if (progress >= target) {
                                            sql = "UPDATE usergroup SET goalprogress=" + progress + ", goalstatus='Completed' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            sql = "UPDATE groupgoal SET goalprogress=" + progress + ", goalstatus='Completed', completiondate='" + date + "'  WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            Mailer.sendWeightCompletion(groupID);
                                        } else {
                                            sql = "UPDATE usergroup SET goalprogress=" + progress + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                            sql = "UPDATE groupgoal SET goalprogress=" + progress + ", goalstatus='In Progress' WHERE groupgoalid =" + groupGoalID;
                                            databaseConnection.runUpdateQuery(sql, connection);
                                        }

                                    }
                                }
                            } catch (SQLException ex) {

                            }

                            groupGoal = new GroupGoal(type, target, startDate, completionDate, gID, id, progress, status, start);
                        } else {
                            groupGoal = new GroupGoal(type, target, startDate, completionDate, gID, id, progress, status, start);
                        }
                    }

                    ResultSet goalProgressList;
                    String progressSQL = "Select * from groupgoalprogress where groupgoalid = " + groupGoalID;
                    goalProgressList = databaseConnection.getData(progressSQL, connection);

                    int groupProgressID = 0;
                    int progressValue = 0;
                    String entryDate = "";
                    GroupGoalProgress groupGoalProgress;

                    while (goalProgressList.next()) {
                        groupProgressID = goalProgressList.getInt(1);
                        progressValue = goalProgressList.getInt(5);
                        entryDate = goalProgressList.getString(6);

                        groupGoalProgress = new GroupGoalProgress(groupProgressID, groupID, userID, progressValue, groupGoal.getGoalID(), entryDate);
                        groupGoal.addProgress(groupGoalProgress);
                    }

                    group = new Group(groupName, groupDescription, userID, groupID, groupGoal);
                    groupList.add(group);

                    sql = "Select userid from groupmembers where groupid = " + groupID;

                    ResultSet groupMembers = databaseConnection.getData(sql, connection);

                    while (groupMembers.next()) {
                        sql = "SELECT * from usertable where id = " + groupMembers.getInt(1);
                        ResultSet userData = databaseConnection.getData(sql, connection);

                        while (userData.next()) {
                            int uID = userData.getInt(1);
                            String email = userData.getString(2);
                            String firstName = userData.getString(4);
                            String surname = userData.getString(5);

                            User u = new User(firstName, surname, email, uID);
                            group.addUser(u);
                        }
                    }

                    sql = "Select userid from usergroup where groupid = " + groupID;

                    ResultSet groupCreator = databaseConnection.getData(sql, connection);

                    while (groupCreator.next()) {
                        sql = "SELECT * from usertable where id = " + groupCreator.getInt(1);
                        ResultSet userData = databaseConnection.getData(sql, connection);

                        while (userData.next()) {
                            int uID = userData.getInt(1);
                            String email = userData.getString(2);
                            String firstName = userData.getString(4);
                            String surname = userData.getString(5);

                            User u = new User(firstName, surname, email, uID);
                            group.setCreator(u);
                            group.addUser(u);
                        }
                    }

                }
            }
            databaseConnection.closeConnection(connection);
        } catch (SQLException ex) {
        }

    }

}
