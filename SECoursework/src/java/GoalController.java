/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.DatabaseConnection;
import classes.Goal;
import classes.GoalProgress;
import classes.Group;
import classes.GroupGoal;
import classes.GroupList;
import classes.User;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Josh
 */
@WebServlet(urlPatterns = {"/GoalController"})
public class GoalController extends HttpServlet {

    HttpServletRequest request = null;
    HttpServletResponse response = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;

        String requestType = request.getParameter("request");

        switch (requestType) {
            case "CreateUserGoal":
                CreateUserGoal();
                break;
            case "CreateGroupGoal":
                CreateGroupGoal();
                break;
            case "AddExerciseProgress":
                AddExerciseProgress();
                break;
            case "AddWeightProgress":
                AddWeightProgress();
                break;
            case "Delete":
                Delete();
                break;
            case "EditExercise":
                EditExercise();
                break;

        }
    }

    private void Delete() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session;
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        
        int id = Integer.parseInt(request.getParameter("id"));

        ArrayList<Goal> list = user.getUserGoals();

        Goal goal = null;

        for (Goal g : list) {
            if (id == g.getGoalID()) {
                goal = g;
            }
        }
        
        list.remove(goal);
        
       String sql = "delete from usergoals where goalid="+id;
       databaseConnection.runUpdateQuery(sql, connection);
       response.sendRedirect("profile.jsp");
    }

    private void EditExercise() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session;
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int id = Integer.parseInt(request.getParameter("id"));

        double distance = Double.parseDouble(request.getParameter("distance"));
        String date = request.getParameter("date");

        ArrayList<Goal> list = user.getUserGoals();

        Goal goal = null;

        for (Goal g : list) {
            if (id == g.getGoalID()) {
                goal = g;
            }
        }
        
        goal.updateDate(date);
        goal.updateDistance(distance);
        
        UpdateUserGoals();
        
        response.sendRedirect("UserController?request=UpdateGoalList");
        return;
        
    }

    private void CreateUserGoal() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session;
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int userID = user.getUserID();

        String targetValue = request.getParameter("target");
        String goalType = request.getParameter("type");

        double targetValueInt = Double.parseDouble(targetValue);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        Goal goal;

        //Data Validation Checking goes here
        if (goalType.equals("Weight")) {
            goal = new Goal(goalType, targetValueInt, startDate, endDate, userID, user.getWeightInKG());
        } else {
            goal = new Goal(goalType, targetValueInt, startDate, endDate, userID, 0);
        }

        try {
            goal.CreateGoal();
            UpdateUserGoals();
        } catch (SQLException ex) {
            Logger.getLogger(GoalController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("UserController?request=UpdateGoalList");
        return;

    }

    private void UpdateUserGoals() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;
        ResultSet rs2 = null;

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        ArrayList<Goal> goals = new ArrayList<>();

        String sql = "Select * from UserGoals where userid = " + user.getUserID() + " order by goalid asc";

        rs = databaseConnection.getData(sql, connection);

        int goalID;
        double startValue, targetValue, goalProgress;
        String goalType, startDate, endDate, status;

        try {
            while (rs.next()) {
                goalID = rs.getInt(1);
                goalType = rs.getString(3);
                startValue = rs.getDouble(4);
                targetValue = rs.getDouble(5);
                startDate = rs.getString(6);
                endDate = rs.getString(7);
                goalProgress = rs.getDouble(8);
                status = rs.getString(9);

                Goal goal = new Goal(goalType, targetValue, startDate, endDate, user.getUserID(), goalID, goalProgress, status, startValue);
                goals.add(goal);

                String progressSQL = "Select * from userprogress where goalid =" + goalID;
                ResultSet progressResults;
                progressResults = databaseConnection.getData(progressSQL, connection);
                while (progressResults.next()) {
                    int pID = progressResults.getInt(1);
                    int gID = progressResults.getInt(2);
                    int uID = progressResults.getInt(3);
                    double progressV = progressResults.getInt(4);
                    String date = progressResults.getString(5);

                    GoalProgress progress = new GoalProgress(pID, gID, uID, progressV, date);
                    goal.addGoalProgress(progress);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        user.setUserGoals(goals);
    }

    private void AddExerciseProgress() throws ServletException, IOException {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int goalID = Integer.parseInt(request.getParameter("id"));
        double progressValue = Double.parseDouble(request.getParameter("value"));
        double currentProgress = Double.parseDouble(request.getParameter("current"));
        double targetValue = Double.parseDouble(request.getParameter("target"));

        Calendar cal = Calendar.getInstance();
        int intMonth = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int intDay = cal.get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(intDay);
        String month = String.valueOf(intMonth);

        if (intDay < 10) {
            day = "0" + intDay;
        }

        if (intMonth < 10) {
            month = "0" + intMonth;
        }

        String date = (day + "/" + month + "/" + year);

        if ((progressValue + currentProgress) < targetValue) {
            String sql = "UPDATE usergoals SET goalprogress = " + (progressValue + currentProgress) + ", goalstatus = 'In Progress' WHERE goalid = " + goalID + ";";
            databaseConnection.runUpdateQuery(sql, connection);
            response.sendRedirect("EmailController?request=GoalProgress&userid=" + user.getUserID());
        } else {
            String sql = "UPDATE usergoals SET goalprogress = " + (progressValue + currentProgress) + ", goalstatus = 'Completed' WHERE goalid = " + goalID + ";";
            databaseConnection.runUpdateQuery(sql, connection);
            response.sendRedirect("EmailController?request=GoalCompletion&userid=" + user.getUserID());
        }

        String sql = "INSERT INTO userprogress(goalid, userid, progressvalue, entrydate) VALUES (" + goalID + ", " + user.getUserID() + ", " + progressValue + ", '" + date + "');";
        databaseConnection.runUpdateQuery(sql, connection);

        UpdateUserGoals();

        return;

    }

    private void AddWeightProgress() throws ServletException, IOException {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int goalID = Integer.parseInt(request.getParameter("id"));
        double newWeight = Double.parseDouble(request.getParameter("value"));
        double currentWeight = Double.parseDouble(request.getParameter("current"));
        double targetValue = Double.parseDouble(request.getParameter("target"));

        Calendar cal = Calendar.getInstance();
        int intMonth = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int intDay = cal.get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(intDay);
        String month = String.valueOf(intMonth);

        if (intDay < 10) {
            day = "0" + intDay;
        }

        if (intMonth < 10) {
            month = "0" + intMonth;
        }

        String date = (day + "/" + month + "/" + year);

        try {

            if (newWeight > targetValue) {
                String sql = "UPDATE usergoals SET goalprogress = " + newWeight + ", goalstatus = 'In Progress' WHERE goalid = " + goalID + ";";
                databaseConnection.runUpdateQuery(sql, connection);
                user.updateWeight(newWeight);
                response.sendRedirect("EmailController?request=GoalProgress&userid=" + user.getUserID());
            } else {
                String sql = "UPDATE usergoals SET goalprogress = " + newWeight + ", goalstatus = 'Completed' WHERE goalid = " + goalID + ";";
                databaseConnection.runUpdateQuery(sql, connection);
                user.updateWeight(newWeight);
                response.sendRedirect("EmailController?request=GoalCompletion&userid=" + user.getUserID());
            }

            String sql = "INSERT INTO userprogress(goalid, userid, progressvalue, entrydate) VALUES (" + goalID + ", " + user.getUserID() + ", " + newWeight + ", '" + date + "');";
            databaseConnection.runUpdateQuery(sql, connection);

            UpdateUserGoals();
        } catch (Exception e) {
            //process
        }

        return;

    }

    private void CreateGroupGoal() throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int groupID = Integer.parseInt(request.getParameter("id"));
        String type = request.getParameter("type");
        int targetValue = Integer.parseInt(request.getParameter("target"));

        Calendar cal = Calendar.getInstance();
        int intMonth = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int intDay = cal.get(Calendar.DAY_OF_MONTH);
        String day = String.valueOf(intDay);
        String month = String.valueOf(intMonth);

        if (intDay < 10) {
            day = "0" + intDay;
        }

        if (intMonth < 10) {
            month = "0" + intMonth;
        }

        String date = (day + "/" + month + "/" + year);

        if (type.equals("Weight")) {

            double total = 0;

            try {

                String sql = "select sum(usertable.weight) FROM usertable INNER JOIN groupmembers ON usertable.id=groupmembers.userid where groupid = " + groupID;
                rs = databaseConnection.getData(sql, connection);

                if (rs.next()) {
                    total += rs.getDouble(1);
                }

                sql = "select sum(usertable.weight) FROM usertable INNER JOIN usergroup ON usertable.id=userid where groupid = " + groupID;
                rs = databaseConnection.getData(sql, connection);

                if (rs.next()) {
                    total += rs.getDouble(1);
                }
            } catch (SQLException ex) {
                Logger.getLogger(GoalController.class.getName()).log(Level.SEVERE, null, ex);
            }

            GroupGoal goal = new GroupGoal(type, targetValue, date, groupID, total);

            GroupList gList = new GroupList();

            ArrayList<Group> groups = gList.getGroupList();

            Group group = null;

            try {
                goal.CreateGoal();
                for (Group g : groups) {
                    if (groupID == g.getGroupID() && group == null) {
                        group = g;
                        group.setGroupGoal(goal);
                    }

                }
            } catch (Exception e) {
            }

        } else {
            GroupGoal goal = new GroupGoal(type, targetValue, date, groupID, 0);

            GroupList gList = new GroupList();

            ArrayList<Group> groups = gList.getGroupList();

            Group group = null;

            try {
                goal.CreateGoal();
                for (Group g : groups) {
                    if (groupID == g.getGroupID() && group == null) {
                        group = g;
                        group.setGroupGoal(goal);
                    }

                }
            } catch (Exception e) {
            }
        }
        response.sendRedirect("GroupController?request=Update&id=" + groupID);
        return;

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
