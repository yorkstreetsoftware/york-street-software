/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.CustomDrink;
import classes.CustomFood;
import classes.DatabaseConnection;
import classes.Drink;
import classes.DrinkList;
import classes.Exercise;
import classes.Food;
import classes.FoodList;
import classes.Goal;
import classes.GoalProgress;
import classes.Group;
import classes.GroupGoal;
import classes.GroupList;
import classes.MD5Encryption;
import classes.Running;
import classes.Swimming;
import classes.User;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Josh
 */
@WebServlet(urlPatterns = {"/UserController"})
public class UserController extends HttpServlet {

    HttpServletRequest request = null;
    HttpServletResponse response = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.request = request;
        this.response = response;

        String requestType = request.getParameter("request");

        switch (requestType) {
            case "login":
                Login();
                break;
            case "register":
                Register();
                break;
            case "completeprofile":
                CompleteProfile();
                break;
            case "Logout":
                Logout();
                break;
            case "createCustomFood":
                createCustomFood();
                break;
            case "CreateCustomDrink":
                CreateCustomDrink();
                break;
            case "AddFood":
                AddFood();
                break;
            case "AddDrink":
                AddDrink();
                break;
            case "AddExercise":
                AddExercise();
                break;
            case "updateFood":
                updateFood();
                break;
            case "updateDrink":
                updateDrink();
                break;
            case "updateExercise":
                updateExercise();
                break;
            case "setnotificationdays":
                updateNotificationDays();
                break;
            case "UpdateWeight":
                UpdateWeight();
                break;
            case "UpdateHeight":
                UpdateHeight();
                break;
            case "UpdateAge":
                UpdateAge();
                break;
            case "UpdateImage":
                UpdateImage();
                break;
            case "Getcalories":
                getCalories();
                break;
            case "UpdateGoalList":
                goalDatesComparison();
                response.sendRedirect("profile.jsp");
                break;
            case "bmiRedirect":
                bmiRedirect();
                break;

        }

        response.getOutputStream().flush();

    }

    private void bmiRedirect() throws ServletException, IOException {

        User user = null;

        HttpSession session = request.getSession(true);

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        ArrayList<Goal> list = user.getUserGoals();
        boolean hasWeightGoal = false;
        
        for(Goal g : list)
            if(g.getGoalType().equals("Weight"))
                hasWeightGoal = true;
        
        
        if(!hasWeightGoal)
            response.sendRedirect("profile.jsp");
        else
            response.sendRedirect("suggestedgoal.jsp");
                        
               

    }
    
    private void getCalories() throws ServletException, IOException {
        ResultSet rs, rs2 = null;
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql, sql2;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int userCalories = 0;

        String date = request.getParameter("date");

        sql = "Select foodid from userfood where userid = " + user.getUserID() + "  AND date = '" + date + "';";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                sql2 = "select calories from food where foodid = " + rs.getString(1) + ";";

                rs2 = databaseConnection.getData(sql2, connection);

                while (rs2.next()) {

                    int x = rs2.getInt(1);

                    userCalories += x;
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "Select drinkid from userdrink where userid =  " + user.getUserID() + "  AND date = '" + date + "';";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                sql2 = "select calories from drink where drinkid = " + rs.getString(1) + ";";

                rs2 = databaseConnection.getData(sql2, connection);

                while (rs2.next()) {

                    int x = rs2.getInt(1);

                    userCalories += x;
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        user.setCalorieCount(userCalories);
        response.sendRedirect("profile.jsp");

    }

    private void goalDatesComparison() throws IOException, ServletException {
        ResultSet rs;
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        ArrayList<Goal> list = new ArrayList<>();

        ArrayList<Goal> usersGoals = user.getUserGoals();
        

        for (int i = 0; i < usersGoals.size(); i++) {
            if (usersGoals.get(i).getGoalStatus().equals("In Progress")) {

                String dateString = usersGoals.get(i).getEndDate();
                Date deadlineDate = null;
                Date currentDate = null;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Calendar cal = Calendar.getInstance();

                    cal.add(Calendar.DAY_OF_MONTH, 7);

                    currentDate = cal.getTime();

                    deadlineDate = sdf.parse(dateString);

                    //formatteddate = sdf.format(date);
                } catch (ParseException ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (currentDate.after(deadlineDate)) {

                    list.add(usersGoals.get(i));
                }

            }
        }
        user.setUserNotificationGoals(list);

    }

    private void UpdateWeight() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        double days = Double.parseDouble(request.getParameter("weight"));

        try {
            user.updateWeight(days);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("account.jsp");
    }

    private void UpdateHeight() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int height = Integer.parseInt(request.getParameter("height"));

        try {
            user.updateHeight(height);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("account.jsp");
    }

    private void UpdateImage() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        String image = request.getParameter("image");

        try {
            user.updateImage(image);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("account.jsp");
    }

    private void UpdateAge() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int age = Integer.parseInt(request.getParameter("age"));

        try {
            user.updateAge(age);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("account.jsp");
    }

    private void updateNotificationDays() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int notificationDays;
        String days;
        days = request.getParameter("days");

        notificationDays = Integer.parseInt(days);

        try {
            user.updateUserDays(notificationDays);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("account.jsp");
    }

    private void setLoginDate() throws ServletException, IOException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        Calendar date = Calendar.getInstance();

    }

    private void setImage() throws ServletException, IOException, SQLException {
        User user = null;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        ResultSet rs = null;

        sql = "Select image from usertable where id = " + user.getUserID() + ";";

        rs = databaseConnection.getData(sql, connection);

        while (rs.next()) {
            user.setUsersPhoto(rs.getString(1));

        }

    }

    private void updateExercise() throws ServletException, IOException {
        String distance, time, id;

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        User user;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        ArrayList<Exercise> exerciseList = user.getUsersExercise();
        id = request.getParameter("exerciseid");
        try {
            distance = request.getParameter("distance");
            time = request.getParameter("time");

            if (ValidateInteger(distance) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;
            }
            if (ValidateInteger(time) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;

        }

        int eTime = Integer.parseInt(time);

        double eDistance = Double.parseDouble(distance);

        int exerciseID = Integer.parseInt(id);

        Exercise exercise = null;

        for (int i = 0; i < exerciseList.size(); i++) {

            if (exerciseList.get(i).getExerciseID() == exerciseID) {

                exercise = exerciseList.get(i);
            }
        }

        try {
            exercise.updateDistance(eDistance);
            exercise.updateTime(eTime);
            SetExerciseList();
            response.sendRedirect("index.jsp");
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void updateDrink() throws ServletException, IOException {
        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        HttpSession session = request.getSession(true);

        User user = null;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }
        ArrayList<Drink> drinkList = user.getCustomDrinkList();
        String drinkID = request.getParameter("drinkid");
        int id = Integer.parseInt(drinkID);
        try {

            drinkName = request.getParameter("drinkName");
            calories = request.getParameter("dcalories");
            protein = request.getParameter("dprotein");
            carbohydrates = request.getParameter("dcarbohydrates");
            sugar = request.getParameter("dsugar");
            fat = request.getParameter("dfat");
            satFat = request.getParameter("dsatFat");
            salt = request.getParameter("dsalt");

            if (ValidateString(drinkName) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }

            if (ValidateInteger(calories) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }
            if (ValidateDouble(protein) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(carbohydrates) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(sugar) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(fat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(satFat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(salt) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;
        }

        int dCalories = Integer.parseInt(calories);
        double dProtein = Double.parseDouble(protein);
        double dCarbohydrates = Double.parseDouble(carbohydrates);
        double dSugar = Double.parseDouble(sugar);
        double dFat = Double.parseDouble(fat);
        double dSatFat = Double.parseDouble(satFat);
        double dSalt = Double.parseDouble(salt);
        Drink drink = null;

        for (int i = 0; i < drinkList.size(); i++) {

            if (drinkList.get(i).getId() == id) {
                drink = drinkList.get(i);
            }
        }

        try {
            drink.updateDrinkName(drinkName);
            drink.updateCalories(dCalories);
            drink.updateCarbohydrates(dCarbohydrates);
            drink.updateSugar(dSugar);
            drink.updateFat(dFat);
            drink.updateSatFat(dSatFat);
            drink.updateSalt(dSalt);
            drink.updateProtein(dProtein);
            setDrinkList();
            setConsumedDrink();
            response.sendRedirect("index.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(ConsumableController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void updateFood() throws IOException, ServletException {
        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        User user = null;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }
        String foodID = request.getParameter("foodid");
        try {
            foodName = request.getParameter("foodName");
            mealType = request.getParameter("mealType");
            calories = request.getParameter("calories");
            protein = request.getParameter("protein");
            carbohydrates = request.getParameter("carbohydrates");
            sugar = request.getParameter("sugar");
            fat = request.getParameter("fat");
            satFat = request.getParameter("satFat");
            salt = request.getParameter("salt");

            if (ValidateString(foodName) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }
            if (ValidateString(mealType) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }
            if (ValidateInteger(calories) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }
            if (ValidateDouble(protein) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(carbohydrates) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(sugar) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(fat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(satFat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(salt) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;
        }

        int id = Integer.parseInt(foodID);
        int fCalories = Integer.parseInt(calories);
        double fProtein = Double.parseDouble(protein);
        double fCarbohydrates = Double.parseDouble(carbohydrates);
        double fSugar = Double.parseDouble(sugar);
        double fFat = Double.parseDouble(fat);
        double fSatFat = Double.parseDouble(satFat);
        double fSalt = Double.parseDouble(salt);
        Food food = null;

        for (int i = 0; i < user.getCustomFoodList().size(); i++) {
            if (user.getCustomFoodList().get(i).getId() == id) {
                food = user.getCustomFoodList().get(i);
            }
        }
        try {
            food.updateFoodName(foodName);
            food.updateMealType(mealType);
            food.updateCalories(fCalories);
            food.updateCarbohydrates(fCarbohydrates);
            food.updateSugar(fSugar);
            food.updateFat(fFat);
            food.updateSatFat(fSatFat);
            food.updateSalt(fSalt);
            food.updateProtein(fProtein);
            SetFoodList();
            setConsumedFood();

        } catch (SQLException ex) {
            Logger.getLogger(ConsumableController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("index.jsp");

    }

    private void SetExerciseList() throws ServletException, IOException {
        int exerciseID;
        int userID = 0;
        String userExercise;
        double exerciseDistance;
        int exerciseTime;
        String exerciseDate;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        User user;
        String sql;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        ResultSet rs;
        ArrayList<Exercise> userExerciseList = new ArrayList<>();

        sql = "Select * from exercise where userid=" + user.getUserID() + " order by exerciseid asc;";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {

                if (rs.getString(3).equals("Running")) {

                    exerciseID = rs.getInt(1);
                    userID = rs.getInt(2);
                    userExercise = rs.getString(3);
                    exerciseDistance = rs.getDouble(4);
                    exerciseTime = rs.getInt(5);
                    exerciseDate = rs.getString(6);
                    Exercise running = new Running(exerciseID, userID, exerciseDistance, exerciseDate, exerciseTime);
                    userExerciseList.add(running);
                } else if (rs.getString(3).equals("Swimming")) {
                    exerciseID = rs.getInt(1);

                    userExercise = rs.getString(3);
                    exerciseDistance = rs.getDouble(4);
                    exerciseTime = rs.getInt(5);
                    exerciseDate = rs.getString(6);
                    Exercise swimming = new Swimming(exerciseID, userID, exerciseDistance, exerciseDate, exerciseTime);
                    userExerciseList.add(swimming);
                }

                user.setUsersExercise(userExerciseList);

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void AddExercise() throws ServletException, IOException {

        int exerciseID, userID;
        String userExercise;
        String  date;
        String exerciseDistance, exerciseTime, exerciseDate;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        User user = null;
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        try {

            userID = user.getUserID();
            userExercise = request.getParameter("exerciseType");
            exerciseDistance = request.getParameter("exerciseDistance");
            exerciseTime = request.getParameter("exerciseTime");
            date = request.getParameter("date");

            if (ValidateInteger(exerciseDistance) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;
            }
            if (ValidateInteger(exerciseTime) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;

        }

        double distance = Double.parseDouble(exerciseDistance);
        int time = Integer.parseInt(exerciseTime);
        userExercise = userExercise.toLowerCase();

        if (userExercise.equals("running")) {
            Exercise running = new Running(userID, distance, date, time);
            try {
                running.insertExercise();
            } catch (SQLException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (userExercise.equals("swimming")) {
            Exercise swimming = new Swimming(userID, distance, date, time);
            try {
                swimming.insertExercise();
            } catch (SQLException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        SetExerciseList();
        user.getMonth();

        response.sendRedirect("profile.jsp");

    }

    private void setDrinkList() throws ServletException, IOException {
        int drinkID, calories;
        String drinkName;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        ResultSet rs = null;
        User user = null;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        ArrayList<Drink> userCustDrinkList = new ArrayList<>();

        sql = "Select * from drink where userid = " + user.getUserID() + " order by drinkid asc;";

        rs = databaseConnection.getData(sql, connection);

        try {
            while (rs.next()) {
                drinkID = rs.getInt(1);
                drinkName = rs.getString(2);
                calories = rs.getInt(3);
                protein = rs.getDouble(4);
                carbohydrates = rs.getDouble(5);
                sugar = rs.getDouble(6);
                fat = rs.getDouble(7);
                satFat = rs.getDouble(8);
                salt = rs.getDouble(9);
                userID = rs.getInt(10);

                Drink drink = new Drink(drinkID, drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID);
                userCustDrinkList.add(drink);

            }
            user.setCustomDrinkList(userCustDrinkList);
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void SetFoodList() throws ServletException, IOException {

        int foodID, calories;
        String foodName, mealType;
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        int userID;
        User user = null;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        ArrayList<Food> userFoodList = new ArrayList<>();
        ResultSet rs = null;

        sql = "Select * from food where userid = " + user.getUserID() + " order by foodid asc;";

        rs = databaseConnection.getData(sql, connection);
        try {
            while (rs.next()) {

                foodID = rs.getInt(1);

                foodName = rs.getString(2);
                mealType = rs.getString(3);
                calories = rs.getInt(4);
                protein = rs.getDouble(5);
                carbohydrates = rs.getDouble(6);
                sugar = rs.getDouble(7);
                fat = rs.getDouble(8);
                satFat = rs.getDouble(9);
                salt = rs.getDouble(10);
                userID = rs.getInt(11);

                Food food = new Food(foodID, foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID);

                userFoodList.add(food);

            }
            user.setCustomFoodList(userFoodList);
        } catch (SQLException ex) {
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setConsumedDrink() throws ServletException, IOException {
        int drinkID, userID, calories;

        String drinkName;
        String date = "00/00/0000";
        double protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        String sql;
        ResultSet rs = null;
        User user = null;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int id = user.getUserID();
        String sql2;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        sql = "SELECT drinkid,userdrink from userdrink where userid=" + id + "; ";
        rs = databaseConnection.getData((sql), connection);
        ArrayList<Drink> consumedDrink = new ArrayList<>();

        try {
            while (rs.next()) {

                sql2 = "SELECT * from drink where drinkid= " + rs.getInt(1) + ";";
                rs2 = databaseConnection.getData((sql2), connection);

                while (rs2.next()) {
                    String sqlDate = "SELECT date from userdrink where userdrink = " + rs.getInt(2) + ";";
                    rs3 = databaseConnection.getData(sqlDate, connection);
                    if (rs3.next()) {
                        date = rs3.getString(1);

                    }

                    drinkID = rs2.getInt(1);
                    drinkName = rs2.getString(2);
                    calories = rs2.getInt(3);
                    protein = rs2.getDouble(4);
                    carbohydrates = rs2.getDouble(5);
                    sugar = rs2.getDouble(6);
                    fat = rs2.getDouble(7);
                    satFat = rs2.getDouble(8);
                    salt = rs2.getDouble(9);
                    userID = rs2.getInt(10);

                    Drink drink = new Drink(drinkID, drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID, date);

                    consumedDrink.add(drink);

                }

                user.setUsersConsumedDrink(consumedDrink);

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setConsumedFood() throws ServletException, IOException {
        int foodID, calories;
        String foodName, mealType;
        String date = "00/00/0000";
        double protein, carbohydrates, sugar, fat, satFat, salt;
        int userID;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        ResultSet rs = null;
        User user = null;
        String sql;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        int id = user.getUserID();
        String sql2;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        sql = "SELECT foodid,userfood from userfood where userid=" + id + "; ";
        rs = databaseConnection.getData((sql), connection);
        ArrayList<Food> consumedFood = new ArrayList<>();

        try {
            while (rs.next()) {

                sql2 = "SELECT * from food where foodid= " + rs.getInt(1) + ";";

                rs2 = databaseConnection.getData(sql2, connection);
                while (rs2.next()) {
                    String sqlDate = "SELECT date from userfood where userfood = " + rs.getInt(2) + ";";
                    rs3 = databaseConnection.getData(sqlDate, connection);
                    if (rs3.next()) {
                        date = rs3.getString(1);

                    }
                    foodID = rs2.getInt(1);

                    foodName = rs2.getString(2);
                    mealType = rs2.getString(3);
                    calories = rs2.getInt(4);
                    protein = rs2.getDouble(5);
                    carbohydrates = rs2.getDouble(6);
                    sugar = rs2.getDouble(7);
                    fat = rs2.getDouble(8);
                    satFat = rs2.getDouble(9);
                    salt = rs2.getDouble(10);
                    userID = rs2.getInt(11);

                    Food food = new Food(foodID, foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt, userID, date);

                    consumedFood.add(food);

                }
                user.setUsersConsumedFood(consumedFood);

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void AddFood() throws IOException, ServletException {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        ResultSet rs = null;
        String foodID = request.getParameter("foodID");
        String date = request.getParameter("date");
        Integer.parseInt(foodID);
        User user = null;
        String sql;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        ArrayList<Food> userFoodList = new ArrayList<>();
        Food food;

        sql = "Insert into userfood(userid,foodid,date) values (" + user.getUserID() + "," + foodID + ",'" + date + "');";
        databaseConnection.runUpdateQuery(sql, connection);

        setConsumedFood();
        response.sendRedirect("index.jsp");

    }

    private void AddDrink() throws IOException, ServletException {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        ResultSet rs = null;
        String drinkID = request.getParameter("drinkID");
        String date = request.getParameter("date");
        Integer.parseInt(drinkID);
        User user = null;
        String sql;
        HttpSession session = request.getSession(true);
        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }
        ArrayList<Drink> userDrinkList = new ArrayList<>();
        Drink drink;
        sql = "INSERT into userdrink(userid,drinkid,date) values  (" + user.getUserID() + "," + drinkID + ",'" + date + "');";
        databaseConnection.runUpdateQuery(sql, connection);
        setConsumedDrink();
        response.sendRedirect("index.jsp");

    }

    private void Login() throws ServletException, IOException {

        String email, password, sql, encryptedPassword;
        String[] userInformation = new String[14];
        MD5Encryption encrypt = new MD5Encryption();
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;
        GroupList groupList = new GroupList();
        String D = null;
        Calendar cal = null;
        int notificationDays = 0;

        email = request.getParameter("loginemail");
        email = email.toLowerCase();
        password = request.getParameter("loginpassword");

        //Encrypt password
        encryptedPassword = encrypt.cryptWithMD5(encrypt.cryptWithMD5(password));

        String isStaff = "f";
        boolean staff = false;

        sql = "Select * from usertable where email = '" + email + "';";

        try {
            rs = databaseConnection.getData(sql, connection);
            if (rs.next()) {
                userInformation[0] = rs.getString("email");
                userInformation[1] = rs.getString("password");
                userInformation[2] = rs.getString("firstname");
                userInformation[3] = rs.getString("lastname");
                userInformation[4] = rs.getString("age");
                userInformation[9] = rs.getString("isbanned");
                userInformation[10] = rs.getString("completedprofile");
                userInformation[11] = rs.getString(1);
                userInformation[12] = rs.getString("image");
            } else {
                response.sendRedirect("index.jsp?error=3");
                return;
            }

        } catch (Exception e) {
            response.sendRedirect("error.jsp");
        }
        if (encryptedPassword.equals(userInformation[1])) {
            if ((userInformation[10]).equals("f")) {
                User sessionUser = new User(userInformation[2], userInformation[3],
                        userInformation[0], Integer.parseInt(userInformation[4]),
                        password);

                session.setAttribute("userSession", sessionUser);
                response.sendRedirect("addinfo.jsp");
            } //If the user has already entered the information required on first login
            else if ((userInformation[10]).equals("t")) {
                try {
                    //Get the updated information and add it to the original array
                    sql = "Select * from usertable where email = '" + userInformation[0] + "';";
                    rs = databaseConnection.getData(sql, connection);
                    while (rs.next()) {

                        userInformation[5] = rs.getString("weight");
                        userInformation[6] = rs.getString("height");
                        userInformation[7] = rs.getString("gender");
                        userInformation[8] = rs.getString("bmi");
                        userInformation[12] = rs.getString("image");
                        isStaff = rs.getString("isStaff");
                        D = rs.getString("lastlogin");
                        notificationDays = rs.getInt("notificationdays");

                    }
                    try {
                        cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

                        cal.setTime(sdf.parse(D));
                    } catch (ParseException ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (isStaff.equals("t")) {
                        staff = true;
                    }

                    User sessionUser = new User(userInformation[2], userInformation[3],
                            userInformation[0], Integer.parseInt(userInformation[4]),
                            password, Integer.parseInt(userInformation[11]), userInformation[7],
                            Double.parseDouble(userInformation[5]), Integer.parseInt(userInformation[6]),
                            Double.parseDouble(userInformation[8]), staff, userInformation[12]);

                    session.setAttribute("userSession", sessionUser);

                    ArrayList<Goal> userGoals
                            = getUserGoals(Integer.parseInt(userInformation[11]));

                    sessionUser.setUserGoals(userGoals);

                    ArrayList<Group> userCreatedGroups
                            = getUserCreatedGroups(Integer.parseInt(userInformation[11]));

                    sessionUser.setUserCreatedGroups(userCreatedGroups);

                    ArrayList<Group> userMemberGroups
                            = getUserMemberGroups(Integer.parseInt(userInformation[11]));

                    sessionUser.setUserMemberGroups(userMemberGroups);

                    SetFoodList();
                    setConsumedFood();
                    setDrinkList();
                    setConsumedDrink();
                    FoodList f = new FoodList();
                    f.updateList();
                    DrinkList d = new DrinkList();
                    d.updateList();
                    SetExerciseList();
                    sessionUser.setLoginDate(cal);
                    Date test = cal.getTime();
                    sessionUser.setDaysToNotify(notificationDays);
                    sessionUser.getMonth();
                    setImage();
                    goalDatesComparison();
                    if (sessionUser.checkDate()) {
                        response.sendRedirect("profile.jsp?error=420");
                        return;
                    } else {
                        response.sendRedirect("profile.jsp");
                    }
                    return;
                } catch (SQLException e) {
                    response.sendRedirect("index.jsp");
                    return;
                }
            }
        } else {
            response.sendRedirect("index.jsp?error=2");
            return;
        }
    }

    private ArrayList<Goal> getUserGoals(int userID) throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;

        ArrayList<Goal> goals = new ArrayList<Goal>();

        String sql = "Select * from UserGoals where userid = " + userID + " order by goalid asc";

        rs = databaseConnection.getData(sql, connection);

        int goalID;
        double startValue, targetValue, goalProgress;
        String goalType, startDate, endDate, status;

        try {
            while (rs.next()) {
                goalID = rs.getInt(1);
                goalType = rs.getString(3);
                startValue = rs.getDouble(4);
                targetValue = rs.getDouble(5);
                startDate = rs.getString(6);
                endDate = rs.getString(7);
                goalProgress = rs.getDouble(8);
                status = rs.getString(9);

                Date deadlineDate = null;
                Date currentDate = null;

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                Calendar cal = Calendar.getInstance();

                currentDate = cal.getTime();

                try {
                    deadlineDate = sdf.parse(endDate);
                } catch (ParseException ex) {
                    //do nothing
                }

                if (currentDate.after(deadlineDate) && status.equals("In Progress")) {
                    String update = "update usergoals set goalstatus = 'Failed' where goalid="+goalID;
                    databaseConnection.runUpdateQuery(update, connection);
                    status = "Failed";
                }
                
                Goal goal = new Goal(goalType, targetValue, startDate, endDate, userID, goalID, goalProgress, status, startValue);
                goals.add(goal);

                String progressSQL = "Select * from userprogress where goalid =" + goalID;
                ResultSet progressResults;
                progressResults = databaseConnection.getData(progressSQL, connection);
                while (progressResults.next()) {
                    int pID = progressResults.getInt(1);
                    int gID = progressResults.getInt(2);
                    int uID = progressResults.getInt(3);
                    double progressV = progressResults.getInt(4);
                    String date = progressResults.getString(5);

                    GoalProgress progress = new GoalProgress(pID, gID, uID, progressV, date);
                    goal.addGoalProgress(progress);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return goals;

    }

    private ArrayList<Group> getUserCreatedGroups(int userID) throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;

        ArrayList<Group> groups = new ArrayList<>();

        String sql = "Select * from Usergroup where userid = " + userID;
        rs = databaseConnection.getData(sql, connection);

        int groupID, groupGoalID;
        double goalProgress;
        String groupName, groupDescription, goalStatus;

        try {
            while (rs.next()) {

                groupID = rs.getInt(1);
                groupName = rs.getString(2);
                groupGoalID = rs.getInt(4);
                groupDescription = rs.getString(5);
                goalStatus = rs.getString(7);
                goalProgress = rs.getDouble(6);

                if (goalStatus == null) {
                    Group group = new Group(groupName, groupDescription, userID);
                    groups.add(group);
                } else {
                    ResultSet goalData;
                    String goalSQL = "Select * from groupgoal where groupgoalid = " + groupGoalID;
                    goalData = databaseConnection.getData(goalSQL, connection);

                    int id, gID;
                    double start, target, progress;
                    String type, startDate, completionDate, status;

                    GroupGoal groupGoal = null;

                    if (goalData.next()) {
                        id = goalData.getInt(1);
                        gID = goalData.getInt(2);
                        type = goalData.getString(3);
                        start = goalData.getDouble(4);
                        target = goalData.getDouble(5);
                        startDate = goalData.getString(6);
                        completionDate = goalData.getString(7);
                        status = goalData.getString(8);
                        progress = goalData.getDouble(9);
                        groupGoal = new GroupGoal(type, target, startDate, completionDate, gID, id, progress, status, start);
                    }

                    //public GroupGoal(String goalType, int target, String start, String end, int groupID, int goalID, int progress, String status, int startValue)
                    Group group = new Group(groupName, groupDescription, userID, groupID, groupGoal);
                    groups.add(group);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return groups;

    }

    private ArrayList<Group> getUserMemberGroups(int userID) throws ServletException, IOException {

        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs = null;
        ResultSet rs2 = null;

        ArrayList<Group> groups = new ArrayList<>();

        String sql = "Select * from groupmembers where userid = " + userID;
        rs = databaseConnection.getData(sql, connection);

        int groupID, groupGoalID;
        double goalProgress;
        String groupName, groupDescription, goalStatus;

        try {
            if (rs.next()) {

                sql = "Select * from Usergroup where groupid = " + rs.getInt(2);
                rs2 = databaseConnection.getData(sql, connection);

                if (rs2.next()) {
                    groupID = rs2.getInt(1);
                    groupName = rs2.getString(2);
                    groupGoalID = rs2.getInt(4);
                    groupDescription = rs2.getString(5);
                    goalStatus = rs2.getString(7);
                    goalProgress = rs2.getDouble(6);

                    if (goalStatus == null) {
                        Group group = new Group(groupName, groupDescription, userID);
                        groups.add(group);
                    } else {
                        ResultSet goalData;
                        String goalSQL = "Select * from groupgoal where groupgoalid = " + groupGoalID;
                        goalData = databaseConnection.getData(goalSQL, connection);

                        int id, gID;
                        double start, target, progress;
                        String type, startDate, completionDate, status;

                        GroupGoal groupGoal = null;

                        if (goalData.next()) {
                            id = goalData.getInt(1);
                            gID = goalData.getInt(2);
                            type = goalData.getString(3);
                            start = goalData.getDouble(4);
                            target = goalData.getDouble(5);
                            startDate = goalData.getString(6);
                            completionDate = goalData.getString(7);
                            status = goalData.getString(8);
                            progress = goalData.getDouble(9);
                            groupGoal = new GroupGoal(type, target, startDate, completionDate, gID, id, progress, status, start);
                        }

                        //public GroupGoal(String goalType, int target, String start, String end, int groupID, int goalID, int progress, String status, int startValue)
                        Group group = new Group(groupName, groupDescription, userID, groupID, groupGoal);
                        groups.add(group);
                    }
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return groups;

    }

    private void CompleteProfile() throws ServletException, IOException {

        String gender, sql;
        double weight;
        int height;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        weight = Double.parseDouble(request.getParameter("weight"));
        height = Integer.parseInt(request.getParameter("height"));
        gender = request.getParameter("gender");

        session = request.getSession(true);
        User user = (User) session.getAttribute("userSession");

        double heightInM = ((double) height) / 100;
        double bmi = (weight / heightInM) / heightInM;
        Calendar date = Calendar.getInstance();
        Date d = date.getTime();

        try {
            user.updateWeight(weight);
            user.updateHeight(height);
            user.updateGender(gender);
            user.updateBMI(bmi);
            user.userCompletedProfile();
            user.updateLoginDate(date.getTime());
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        double minBMI = user.getMinimumWeight();
        double maxBMI = user.getMaximumWeight();

        if (bmi <= 18.5 || bmi >= 24.9) {
            response.sendRedirect("suggestedgoal.jsp");
        } else {
            response.sendRedirect("profile.jsp");
        }
    }

    private boolean ValidateString(String string) {
        return string.matches("[a-zA-Z\\s]*$+");

    }

    private boolean ValidateInteger(String givenInt) {
        return givenInt.matches("[0-9]+");
    }

    private boolean ValidateDouble(String givenDouble) throws IOException {

        try {
            Double.parseDouble(givenDouble);
        } catch (Exception e) {

            return false;

        }
        return true;

    }

    private void Register() throws ServletException, IOException {

        String email, firstname, surname, password, confirmPassword, sql, dob, age;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);
        ResultSet rs;

        try {

            email = request.getParameter("email");
            email = email.toLowerCase();
            firstname = request.getParameter("firstname");
            surname = request.getParameter("lastname");
            age = request.getParameter("age");
            password = request.getParameter("password");
            confirmPassword = request.getParameter("confirmpassword");

            int usersAge = Integer.parseInt(age);

            User sessionUser = null;

            if (ValidateString(firstname) == false) {
                response.sendRedirect("index.jsp?error=6");
                return;

            }
            if (ValidateString(surname) == false) {
                response.sendRedirect("index.jsp?error=6");
                return;

            }

            try {
                sql = "SELECT email from usertable where email = '" + email + "';";
                rs = databaseConnection.getData(sql, connection);
                if (!rs.next()) {
                    User user = new User(firstname, surname, email, usersAge, password);
                    user.createUser();
                    sessionUser = user;
                } else {
                    response.sendRedirect("index.jsp?error=4");
                    return;
                }
            } catch (SQLException e) {
                response.sendRedirect("SQLerror.jsp");
                return;
            }

            session.setAttribute("userSession", sessionUser);
            response.sendRedirect("addinfo.jsp");
        } catch (Exception e) {
            response.sendRedirect("index.jsp?error=5");
            return;
        }
    }

    private void Logout() throws IOException {
        HttpSession session = request.getSession(true);
        session.invalidate();
        response.sendRedirect("index.jsp");
    }

    private void createCustomFood() throws ServletException, IOException {
        String foodName, mealType, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        try {
            foodName = request.getParameter("foodName");
            mealType = request.getParameter("mealType");
            calories = request.getParameter("calories");
            protein = request.getParameter("protein");
            carbohydrates = request.getParameter("carbohydrates");
            sugar = request.getParameter("sugar");
            fat = request.getParameter("fat");
            satFat = request.getParameter("satFat");
            salt = request.getParameter("salt");

            if (ValidateString(foodName) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }
            if (ValidateString(mealType) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }
            if (ValidateInteger(calories) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }
            if (ValidateDouble(protein) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(carbohydrates) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(sugar) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(fat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(satFat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(salt) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;
        }

        int fCalories = Integer.parseInt(calories);
        double fProtein = Double.parseDouble(protein);
        double fCarbohydrates = Double.parseDouble(carbohydrates);
        double fSugar = Double.parseDouble(sugar);
        double fFat = Double.parseDouble(fat);
        double fSatFat = Double.parseDouble(satFat);
        double fSalt = Double.parseDouble(salt);
        int userID = user.getUserID();
        Food food = new Food(foodName, mealType, fCalories, fProtein, fCarbohydrates, fSugar, fFat, fSatFat, fSalt, userID);
        try {
            food.createFood();
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        SetFoodList();
        FoodList f = new FoodList();
        f.updateList();

        response.sendRedirect("profile.jsp");

    }

    private void CreateCustomDrink() throws ServletException, IOException {
        String drinkName, calories, protein, carbohydrates, sugar, fat, satFat, salt;
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = databaseConnection.getConnection();
        HttpSession session = request.getSession(true);

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.hasCompletedProfile() == false) {
            response.sendRedirect("addinfo.jsp");
            return;
        }

        try {
            drinkName = request.getParameter("drinkName");
            calories = request.getParameter("dcalories");
            protein = request.getParameter("dprotein");
            carbohydrates = request.getParameter("dcarbohydrates");
            sugar = request.getParameter("dsugar");
            fat = request.getParameter("dfat");
            satFat = request.getParameter("dsatFat");
            salt = request.getParameter("dsalt");

            if (ValidateString(drinkName) == false) {
                response.sendRedirect("profile.jsp?error=7");
                return;

            }

            if (ValidateInteger(calories) == false) {
                response.sendRedirect("profile.jsp?error=8");
                return;

            }
            if (ValidateDouble(protein) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(carbohydrates) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(sugar) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(fat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(satFat) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }
            if (ValidateDouble(salt) == false) {
                response.sendRedirect("profile.jsp?error=9");
                return;

            }

        } catch (Exception e) {
            response.sendRedirect("profile.jsp?error=5");
            return;
        }

        double dCalories = Integer.parseInt(calories);
        double dProtein = Double.parseDouble(protein);
        double dCarbohydrates = Double.parseDouble(carbohydrates);
        double dSugar = Double.parseDouble(sugar);
        double dFat = Double.parseDouble(fat);
        double dSatFat = Double.parseDouble(satFat);
        double dSalt = Double.parseDouble(salt);
        int userID = user.getUserID();

        try {
            CustomDrink drink = new CustomDrink(userID, drinkName, dCalories, dProtein, dCarbohydrates, dSugar, dFat, dSatFat, dSalt);
            drink.createDrink();
            response.sendRedirect("profile.jsp");
        } catch (SQLException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        setDrinkList();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
