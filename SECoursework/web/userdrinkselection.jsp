<%-- 
    Document   : userdrinkselection
    Created on : 12-Apr-2015, 12:14:22
    Author     : Max
--%>

<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    session = request.getSession(true);

    User user = null;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }
    ArrayList<Drink> drinkList = user.getDrinkList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add a drink!</h1>
        <%
            if ((drinkList.isEmpty()) && (custDrinkList.isEmpty())) {

                out.println("<a href='addcustomfood.jsp'>You have no drink, add a drink</a>");

            }
        %>


        <%
            if ((!drinkList.isEmpty()) || (!custDrinkList.isEmpty())) {
                out.println("<form action ='UserController?request=AddDrink' method='post' class='dark-matter'>");
                out.println("<select id='drinkID' name='drinkID' required>");

                if (!drinkList.isEmpty()) {

                    for (int i = 0; i < drinkList.size(); i++) {

                        out.println(drinkList.get(i).getId());
                        out.println(drinkList.get(i).getDrinkName());
                        out.println(drinkList.get(i).getCalories());
                        out.println("<option label='" + drinkList.get(i).getDrinkName() + "' value = '" + drinkList.get(i).getId() + "''>'" + drinkList.get(i).getDrinkName() + "'</option>");

                    }
                } else if (!custDrinkList.isEmpty()) {
                    for (int i = 0; i < custDrinkList.size(); i++) {

                        out.println(custDrinkList.get(i).getId());
                        out.println(custDrinkList.get(i).getDrinkName());
                        out.println(custDrinkList.get(i).getCalories());
                        out.println("<option label='" + custDrinkList.get(i).getDrinkName() + "' value = '" + custDrinkList.get(i).getId() + "''>'" + custDrinkList.get(i).getDrinkName() + "'</option>");

                    }
                }
                out.println("<input type='submit' name ='submit' value='Add this Drink!' class='button1'>");
            }
        %>



    </form>  
</body>
</html>
