<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();

    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }

    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }
    ArrayList<Food> foodList = user.getFoodList();
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> drinkList = user.getDrinkList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);
  String paramID = request.getParameter("drinkid");

        int id = Integer.parseInt(paramID);
    Drink drink = null;
    for (int i = 0; i < drinkList.size(); i++) {

        if (drinkList.get(i).getId() == id) {
            drink = drinkList.get(i);

        }
    }

    if (drink == null) {
        response.sendRedirect("admin.jsp");
        return;
    }

%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <title>Health Tracker - <%=fullName%></title>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                    <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <center>
                            <div class="border">
                                <form action="AdminController?request=updateDrink" method="post">
                                    <h1>Edit Drink</h1>
                                    <p><input type="hidden" name="drinkid" value=<%=drink.getId()%> </p>
                                    <p>Drink Name: <input type="text" maxlength="50" size="40" name="drinkName" value="<%=drink.getDrinkName()%>" required></p>
                                    <p>Calories:<input type="text" maxlength="50" size="40" name="dcalories" value=<%=drink.getCalories()%>  required></p>
                                    <p>Protein:<input type="text" maxlength="50" size="40" name="dprotein" value=<%=drink.getProtein()%>  required></p>
                                    <p>Carbohydrates:<input type="text" maxlength="50" size="40" name="dcarbohydrates" value=<%=drink.getCarbohydrates()%>  required></p>
                                    <p>Sugar:<input type="text" maxlength="50" size="40" name="dsugar" value=<%=drink.getSugar()%> required></p>
                                    <p>Food fat: <input type="text" maxlength="50" size="40" name="dfat" value=<%=drink.getFat()%>  required></p>
                                    <p>Saturated fat: <input type="text" maxlength="50" size="40" name="dsatFat" value=<%=drink.getSatFat()%> required></p>
                                    <p>Salt:<input type="text" maxlength="50" size="40" name="dsalt" value=<%=drink.getSalt()%>  required></p>


                                    <input type="submit" value="Edit Drink" class="button2">
                                </form>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>   
    </body>

</html>