<%@page import="classes.User"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Food"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
        <title>Health tracker </title>
    </head>
    <%

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.getIsStaff()== false) {
            response.sendRedirect("index.jsp");
            return;
        }

        ArrayList<Food> foodList = null;

        if (session.getAttribute("foodList") != null) {
            foodList = (ArrayList<Food>) session.getAttribute("foodList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }
    %>

    <body>
        <div class="header-wrap">
            <header>
                <a href="admin.jsp">
                    <hgroup>

                    </hgroup>
                </a>

                <nav>
                    <span class="spanFormat">
                        <form action="account.jsp" method="get">
                            Welcome! You are logged in as an Admin&nbsp;&nbsp;
                        </form>
                    </span>
                    <span class="spanFormat">
                        <form action="index.jsp" method="get">
                            <input type="submit" value="Logout" class="button">
                        </form>
                    </span>
                </nav>
            </header>
        </div>
        <div class="content-wrap">

            <hr>
            
            <form>
                <%
                
                    for (int i = 0; i < foodList.size(); i++) {
                    
                        out.println(foodList.get(i).getId());
                        out.println(foodList.get(i).getFoodName());
                        out.println(foodList.get(i).getMealType());
                        out.println("<a href=\"editfood.jsp?foodid=" + foodList.get(i).getId() + "\">Edit Food</a>");

              
                        out.println("<a href=\"deletefood.jsp?foodid=" + foodList.get(i).getId() + "\">Delete Food</a>");
                        out.print("<br>");

                    }


                %> 



            </form>

            <hr>


        </div>
        <div class="clear"></div>

        <div class="footer">
            <p>Copyright School Alumni Site (SAS) - 2014</p>
        </div>
    </body>
</html>