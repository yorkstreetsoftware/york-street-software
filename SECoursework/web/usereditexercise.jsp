<%-- 
    Document   : usereditexercise
    Created on : 13-Apr-2015, 20:34:19
    Author     : Max
--%>

<%@page import="classes.Exercise"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
        <title>Health tracker </title>
    </head>
    <%

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }
        
        String paramID = request.getParameter("foodid");

        int id = Integer.parseInt(paramID);
       ArrayList<Exercise> exerciseList = user.getUsersExercise();
       Exercise exercise = null;
       
       for (int i = 0; i < exerciseList.size(); i++) {
            if (exerciseList.get(i).getExerciseID() == id) {
                exercise = exerciseList.get(i);
            }
        }
       if (exercise== null){
           response.sendRedirect("profile.jsp");
            return;
           
       }
       
         %>
       
    <body>
        <div class="header-wrap">
            <header>
                <a href="admin.jsp">
                    <hgroup>

                    </hgroup>
                </a>

                <nav>
                    <span class="spanFormat">
                        <form action="account.jsp" method="get">
                            Welcome! You are logged in as an Admin&nbsp;&nbsp;
                        </form>
                    </span>
                    <span class="spanFormat">
                        <form action="index.jsp" method="get">
                            <input type="submit" value="Logout" class="button">
                        </form>
                    </span>
                </nav>
            </header>
        </div>
        <div class="content-wrap">
            <div class="content-left">
                <hr>
                <form action="UserController?request=updateExercise" method="post">

                    <h1>Edit exercise</h1>
                    <p><input type="hidden" name="exerciseid" value=<%=exercise.getExerciseID()%> </p>
                    <br>
                    <p>Dist:<input type="text" maxlength="50" size="40" name="distance" value="<%= exercise.getDistance()  %>" required></p>
                    <p>Time:<input type="text" maxlength="50" size="40" name="time" value="<%= exercise.getMinutes()%>"  required></p>
                    
                    

                    <input type="submit" value="Edit Exercise" class="button">
                </form>
                <hr>
            </div>
            <div class="content-right">
                <hr>

                <hr>
            </div>
        </div>
        <div class="clear"></div>

        <div class="footer">
            <p>Copyright School Alumni Site (SAS) - 2014</p>
        </div>
    </body>
</html>
