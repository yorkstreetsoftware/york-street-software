<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);
    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();

    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }

    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }
    ArrayList<Food> foodList = user.getFoodList();
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();
    ArrayList<User> userList = new ArrayList<>();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);

    ArrayList<Drink> drinkList = null;

    if (session.getAttribute("userlist") != null) {
        userList = (ArrayList<User>) session.getAttribute("userlist");
    } else {
        response.sendRedirect("admin.jsp");
        return;

    }

    if (session.getAttribute("drinkList") != null) {
        drinkList = (ArrayList<Drink>) session.getAttribute("drinkList");
    } else {
        response.sendRedirect("admin.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="javascript/SweetAlert/dist/sweetalert2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="javascript/SweetAlert/dist/sweetalert2.css">
        <script type="text/javascript" src="javascript/canvasjs.min.js"></script>
        <script type="text/javascript" src="javascript/jquery.totemticker.js"></script>

        <title>Health Tracker - <%=fullName%></title>
        <script>
            function createDrink() {
                swal({
                    title: 'Create Drink',
                    html: '<form action="AdminController?request=Drink" method="post"><p>Drink Name: <input type="text" maxlength="50" size="40" name="drinkName" required></p><p>Calories:<input type="number" maxlength="50" size="40" name="dcalories" required></p><p>Protein:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dprotein" required></p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dcarbohydrates" required></p><p>Sugar:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dsugar" required></p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="dfat" required></p><p>Saturated fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="dsatFat" required></p><p>Salt:<input type="number" maxlength="50" step="0.01" min="0" size="40" name="dsalt" required></p><input type="submit" value="Add drink" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function createFood() {
                swal({
                    title: 'Create Food',
                    html: '<form action="AdminController?request=Food" method="post"><p>Food Name: <input type="text" maxlength="50" size="40" name="foodName" required></p><p>Food Type: <input type="text" maxlength="50" size="40" name="mealType" required></p><p>Calories:<input type="number" maxlength="50" size="40" name="calories" required></p><p>Protein: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="protein" required></p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="carbohydrates" required></p><p>Sugar: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="sugar" required></p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="fat" required></p> <p> Saturated fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="satFat" required></p><p>Salt: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="salt" required></p><input type="submit" value="Add food" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function banUser(id) {
                swal({
                    title: 'Ban User',
                    html: '<form action="AdminController?request=banUser" method="post"><h3>Are you sure you want to ban this user?</h3><p>User ID: ' + id + '</p><input type="hidden" name="userid" value="' + id + '"><input type="submit" value="Ban User" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false,
                    type: 'warning'
                });
            }
        </script>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li><a href="index.jsp" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                <li class="active"><a href='admin.jsp' class='scroll'>Admin Settings</a></li>
                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <center>
                            <div class="border">
                                <button type="submit" class="button2" onClick="createFood()">Create Food</button> 
                                <button type="submit" class="button2" onClick="createDrink()">Create Drink</button> 
                                <div id="accordion">
                                    <h3>Ban User</h3>
                                    <div>
                                        <%
                                            for (int i = 0; i < userList.size(); i++) {
                                                //out.println("<li>");
                                                out.println("<div id ='goals'>");

                                                out.println("<button type='button' class='button2' onClick='banUser(" + userList.get(i).getUserID() + ")'>" + userList.get(i).getUserID() + " - " + userList.get(i).getFirstName() + " " + userList.get(i).getLastName() + "</button>");
                                                // out.println("</li>");

                                                out.println("</div>");

                                                out.print("<hr>");
                                            }
                                        %>
                                    </div>
                                    <h3>Edit/Delete Food</h3>
                                    <div>
                                        <%
                                            for (int i = 0; i < foodList.size(); i++) {
                                                out.println(foodList.get(i).getId());
                                                out.println(foodList.get(i).getFoodName());
                                                out.println(foodList.get(i).getProtein());
                                                out.println("<a href=\"editfood.jsp?foodid=" + foodList.get(i).getId() + "\">Edit Food</a>");
                                                out.println("<a href=\"deletefood.jsp?foodid=" + foodList.get(i).getId() + "\">Delete Food</a>");
                                                out.print("<br>");

                                            }


                                        %> 
                                    </div>
                                    <h3>Edit/Delete Drink</h3>
                                    <div>
                                        <%                                            for (int i = 0; i < drinkList.size(); i++) {
                                                out.println(drinkList.get(i).getId());
                                                out.println(drinkList.get(i).getDrinkName());
                                                out.println(drinkList.get(i).getProtein());
                                                out.println("<a href=\"editdrink.jsp?drinkid=" + drinkList.get(i).getId() + "\">Edit Drink</a>");
                                                out.println("<a href=\"deletedrink.jsp?drinkid=" + drinkList.get(i).getId() + "\">Delete Drink</a>");
                                                out.print("<br>");

                                            }


                                        %> 
                                    </div>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>   
    </body>
    <script>
        $(function () {
            $("#accordion:nth-child(1n)").accordion({
                heightStyle: "content",
                active: false,
                collapsible: true
            });
        });</script>
</html>