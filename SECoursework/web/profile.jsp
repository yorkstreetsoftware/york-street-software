<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Goal> userExpiringGoals = null;

    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userExpiringGoals = user.getUserNotificationGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();
    String userPhoto = user.getUsersPhoto();
    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }
    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }
    ArrayList<Food> foodList = user.getFoodList();
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> drinkList = user.getDrinkList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);
    String theBMIColor = null;
    if (bmi < 18.5) {
        theBMIColor = "<span class='bmiy'>Underweight</span>";
    } else if (bmi >= 18.5 && bmi < 25) {
        theBMIColor = "<span class='bmig'>Normal Weight</span>";
    } else if (bmi >= 25 && bmi < 30) {
        theBMIColor = "<span class='bmiy'>Overweight</span>";
    } else if (bmi >= 30 && bmi < 35) {
        theBMIColor = "<span class='bmio'>Obese</span>";
    } else if (bmi >= 35) {
        theBMIColor = "<span class='bmir'>Morbidly Obese</span>";
    }
%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="javascript/SweetAlert/dist/sweetalert2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="javascript/SweetAlert/dist/sweetalert2.css">
        <script type="text/javascript" src="javascript/canvasjs.min.js"></script>
        <script type="text/javascript" src="javascript/jquery.totemticker.js"></script>


        <title>Health Tracker - <%=fullName%></title>

        <script>
            $(document).ready(function () {
                $("#content").find("[id^='tab']").hide(); // Hide all content
                $("#tabs li:first").attr("id", "current"); // Activate the first tab
                $("#content #tab1").fadeIn(); // Show first tab's content

                $('#tabs a').click(function (e) {
                    e.preventDefault();
                    if ($(this).closest("li").attr("id") === "current") { //detection for current tab
                        return;
                    }
                    else {
                        $("#content").find("[id^='tab']").hide(); // Hide all content
                        $("#tabs li").attr("id", ""); //Reset id's
                        $(this).parent().attr("id", "current"); // Activate this
                        $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
                    }
                });
            });</script>
        <script>
            function addProgressExerciseGoal(progress, id, target) {
                swal({
                    title: 'Add progress of Exercise Goal',
                    html: '<form action="GoalController?request=AddExerciseProgress" method="post"><p>Current Progress: ' + progress + 'm</p></br><p>Progress Value:<input type="number" maxlength="10" size="10" name="value" required>m</p></br><input type="hidden" name="id" value="' + id + '"><input type="hidden" name="target" value="' + target + '"><input type="hidden" name="current" value="' + progress + '"><input type="submit" value="Add Progress" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function editGoal(id, distance) {
                swal({
                    title: 'Edit Goal',
                    html: '<form action="GoalController?request=EditExercise" method="post"><p>Current Distance: ' + distance + 'm</p></br><p>New distance:<input type="number" maxlength="10" size="10" name="distance" required>m</p><p>New Date:<input type="text" maxlength="10" size="10" name="date"required></p></br></br><input type="hidden" name="id" value="' + id + '"><input type="submit" value="Edit Goal" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function deleteGoal(id) {
                swal({
                    title: 'Delete Goal',
                    html: '<form action="GoalController?request=Delete" method="post"><h3>Are you sure you want to delete this goal?</h3><p>Goal ID: ' + id + '</p><input type="hidden" name="id" value="' + id + '"><input type="submit" value="Delete Goal" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false,
                    type: 'warning'
                });
            }
        </script>
        <script>
            function addProgressWeightGoal(progress, id, target) {
                swal({
                    title: 'Add progress to Weight Goal',
                    html: '<form action="GoalController?request=AddWeightProgress" method="post"><p>Current Progress: ' + progress + 'kg</p></br><p>Progress Value(New Weight):<input type="number" step="0.01" min="0" maxlength="10" size="10" name="value" required>kg</p></br><input type="hidden" name="id" value="' + id + '"><input type="hidden" name="target" value="' + target + '"><input type="hidden" name="current" value="' + progress + '"><input type="submit" value="Add Progress" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function createGroup() {
                swal({
                    title: 'Create New Group',
                    html: '<form action="GroupController?request=Create" method="post"><p>Group Name: <input type="text" maxlength="75" size="40" name="name" required></p></br><p>Group Description:<input type="text" maxlength="500" size="40" name="description" required></p></br><input type="submit" value="Create Group" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function joinGroup() {
                swal({
                    title: 'Group List - Select one to join',
                    html: '<form action="GroupController?request=Join" method="post"><p>List of groups here</p></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function createFood() {
                swal({
                    title: 'Create New Food',
                    html: '<form action="UserController?request=createCustomFood" method="post"><p>Food Name: <input type="text" maxlength="50" size="40" name="foodName" required></p><p>Meal Type:<select name="mealType" required><option value="" selected disabled></option><option value="snack">Snack</option><option value="breakfast">Breakfast</option><option value="lunch">Lunch</option><option value="dinner">Dinner</option></select></p><p>Calories:<input type="number" maxlength="10" size="10" name="calories" required>Cal</p><p>Protein:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="protein" required>g</p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="carbohydrates" required>g</p><p>Sugar:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="sugar" required>g</p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="10" size="10" name="fat" required>g</p><p>Saturated fat: <input type="number" step="0.01" min="0" maxlength="10" size="10" name="satFat" required>g</p><p>Salt:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="salt" required>g</p></br><input type="submit" value="Create Food" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function createDrink() {
                swal({
                    title: 'Create New Drink',
                    html: '<form action="UserController?request=CreateCustomDrink" method="post"><p>Drink Name: <input type="text" maxlength="50" size="40" name="drinkName" required></p><p>Calories:<input type="number" maxlength="10" size="10" name="dcalories" required>Cal</p><p>Protein:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="dprotein" required>g</p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="dcarbohydrates" required>g</p><p>Sugar:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="dsugar" required>g</p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="10" size="10" name="dfat" required>g</p><p>Saturated fat: <input type="number" step="0.01" min="0" maxlength="10" size="10" name="dsatFat" required>g</p><p>Salt:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="dsalt" required>g</p></br><input type="submit" value="Create Drink" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            $(function () {
                $('#exercisedatepicker').datepicker({
                    dateFormat: "dd/mm/yy",
                    onSelect: function (test, obj) {
                        swal({
                            title: 'Add Exercise to ' + test,
                            html: '<form action="UserController?request=AddExercise" method="post"><p>Exercise Type:<select name="exerciseType" required><option value="" selected disabled></option><option value="running">Running</option><option value="swimming">Swimming</option></select></p><p>Distance:<input type="number" step="0.01" min="0" maxlength="10" size="10" name="exerciseDistance" required>km</p><p>Time:<input type="number" maxlength="10" size="10" name="exerciseTime" required>mins</p><input type="hidden" value='+test+' name="date"></br><input type="submit" value="Add Entry" class="button2"></form>',
                            showCancelButton: true,
                            showConfirmButton: false,
                            closeOnConfirm: false
                        });
                    }
                });
            });</script>
        <script type="text/javascript">
            window.onload = function () {
                var chart = new CanvasJS.Chart("chartContainer",
                        {
                            animationEnabled: true,
                            title: {
                                text: "Combined exercise distance travelled per month (Metres)"
                            },
                            data: [
                                {
                                    type: "column", //change type to bar, line, area, pie, etc
                                    dataPoints: [
                                        {label: "Jan", y: <%=user.getJanDistance()%>},
                                        {label: "Feb", y: <%=user.getFebDistance()%>},
                                        {label: "Mar", y: <%=user.getMarDistance()%>},
                                        {label: "Apr", y: <%=user.getAprDistance()%>},
                                        {label: "May", y: <%=user.getMayDistance()%>},
                                        {label: "June", y: <%=user.getJunDistance()%>},
                                        {label: "July", y: <%=user.getJulDistance()%>},
                                        {label: "Aug", y: <%=user.getAugDistance()%>},
                                        {label: "Sept", y: <%=user.getSepDistance()%>},
                                        {label: "Oct", y: <%=user.getOctDistance()%>},
                                        {label: "Nov", y: <%=user.getNovDistance()%>},
                                        {label: "Dec", y: <%=user.getDecDistance()%>}

                                    ]
                                }
                            ]
                        });
                chart.render();
            };


        </script>
        <script>
            var today = new Date();
            var week = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var dd2 = today.getDate() + 7;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;
            week = dd2 + '/' + mm + '/' + yyyy;</script>
        <script type="text/javascript">
            $(function () {
                $('.vertical-ticker').totemticker({
                    row_height: '44px',
                    speed: 500,
                    interval: 2000,
                    mousestop: true,
                    max_items: 1,
                    direction: 'down'
                });
            });</script>
        <script>
            var dateToday = new Date();
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    maxDate: dateToday
                });
                $("#datepicker2").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    onSelect: function (selected) {
                        $("#datepicker1").datepicker("option", "minDate", selected);
                    }
                });
                $("#datepicker1").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    onSelect: function (selected) {
                        $("#datepicker2").datepicker("option", "maxDate", selected);
                    }
                });
                $("#datepicker3").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    onSelect: function (selected) {
                        $("#datepicker4").datepicker("option", "minDate", selected);
                    }
                });
                $("#datepicker4").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    onSelect: function (selected) {
                        $("#datepicker3").datepicker("option", "maxDate", selected);
                    }
                });
            });</script>
        <style type="text/css">
            #datepicker
            {
                position: relative;
                z-index: 9999999;
            }
        </style>
        <script>
            $(function () {
                $("#drinkdatepicker").datepicker({dateFormat: "dd/mm/yy"}).val();
                $("#fooddatepicker").datepicker({dateFormat: "dd/mm/yy"}).val();
            });</script>
        <style>.ui-progressbar{text-align:center; line-height: 125%;}
            .ui-progressbar .ui-progressbar-value{height: 15px; position: relative;}
            .ui-progressbar .ui-widget-header{background: #0F0}
        </style>
        <script>
            $(function () {
                var value;
                $('.progressbar').each(function () {
                    value = parseInt($(this).text());
                    $(this).empty().progressbar({'value': value});
                    $(this).append("" + value + "%</span>");
                });
            });</script>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                    <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='AdminController?request=method' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>
                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <div id="content-left">
                            <strong><%=fullName%></strong><br>
                            <img src="<%=userPhoto%>" alt="No profile picture" width="175px" height="175px">
                            <div class="info">
                                <p>Current Age: <%=age%></p>
                                <p>Current Weight: <%=weight%>kg</p>
                                <p>Current Height: <%=heightInM%>m</p>
                                <p>Current BMI: <%=bmi%></p>
                                <p>BMI Status: <%=theBMIColor%></p>
                                <p>Number of Groups Created: <%=numberOfGroups%></p>
                                <p>Number of Goals Created: <%=numberOfGoals%></p><br/>
                                <p>Calorie count for selected day: <%=user.getCalorieCount()%></p>

                            </div>
                        </div>
                        <div id="content-right">
                            <ul id="tabs">
                                <li><a href="#" name="tab1">Food/Drink Diary</a></li>
                                <li><a href="#" name="tab2">Exercise Diary</a></li>
                                <li><a href="#" name="tab3">Exercise Goals</a></li>
                                <li><a href="#" name="tab4">Weight Goals</a></li>
                                <li><a href="#" name="tab5">Groups</a></li>    
                                <li><a href="#" name ="tab6">Statistics</a></li>
                            </ul>

                            <div id="content"> 
                                <div id="tab1">
                                    <div id="accordion">
                                        <h3>Add Food to diary</h3>
                                        <div>
                                            <%            if ((foodList.isEmpty()) && (custFoodList.isEmpty())) {
                                                    out.println("You have no food, <button type='button' class='button2' onClick='createFood()'>Create Food</button>?");
                                                }
                                            %>

                                            <%
                                                if ((!foodList.isEmpty()) || (!custFoodList.isEmpty())) {
                                                    out.println("<form action ='UserController?request=AddFood' method='post' >");
                                                    out.println("Food: <select id='foodID' name='foodID' required>");

                                                    if (!foodList.isEmpty()) {

                                                        for (int i = 0; i < foodList.size(); i++) {

                                                            out.println(foodList.get(i).getId());
                                                            out.println(foodList.get(i).getFoodName());
                                                            out.println(foodList.get(i).getMealType());
                                                            out.println("<option label='" + foodList.get(i).getFoodName() + "' value = '" + foodList.get(i).getId() + "''>'" + foodList.get(i).getFoodName() + "'</option>");

                                                        }
                                                    } else if (!custFoodList.isEmpty()) {

                                                        for (int i = 0; i < custFoodList.size(); i++) {

                                                            out.println(custFoodList.get(i).getId());
                                                            out.println(custFoodList.get(i).getFoodName());
                                                            out.println(custFoodList.get(i).getMealType());
                                                            out.println("<option label='" + custFoodList.get(i).getFoodName() + "' value = '" + custFoodList.get(i).getId() + "''>'" + custFoodList.get(i).getFoodName() + "'</option>");

                                                        }
                                                    }
                                                    out.println("</select>");
                                                    out.println("Date: <input type='text' name='date' value='" + date + "' id='fooddatepicker' readonly >");
                                                    out.println("<p>You may also select a date, by clicking on the input field</p>");
                                                    out.println("</br>");
                                                    out.println("<button type='button' class='button2' onClick='createFood()'>Create new food</button><input type='submit' name ='submit' value='Add this Food' class='button2'>");
                                                    out.println("</form>");
                                                }

                                            %>
                                        </div>
                                        <h3>Add Drink to diary</h3>
                                        <div>
                                            <%if ((drinkList.isEmpty()) && (custDrinkList.isEmpty())) {
                                                    out.println("You have no drink, <button type='button' class='button2' onClick='createDrink()'>Create Drink</button>?");
                                                }
                                            %>


                                            <%
                                                if ((!drinkList.isEmpty()) || (!custDrinkList.isEmpty())) {
                                                    out.println("<form action ='UserController?request=AddDrink' method='post'>");
                                                    out.println("Drink: <select id='drinkID' name='drinkID' required>");

                                                    if (!drinkList.isEmpty()) {

                                                        for (int i = 0; i < drinkList.size(); i++) {

                                                            out.println(drinkList.get(i).getId());
                                                            out.println(drinkList.get(i).getDrinkName());
                                                            out.println(drinkList.get(i).getCalories());
                                                            out.println("<option label='" + drinkList.get(i).getDrinkName() + "' value = '" + drinkList.get(i).getId() + "''>'" + drinkList.get(i).getDrinkName() + "'</option>");

                                                        }
                                                    } else if (!custDrinkList.isEmpty()) {
                                                        for (int i = 0; i < custDrinkList.size(); i++) {

                                                            out.println(custDrinkList.get(i).getId());
                                                            out.println(custDrinkList.get(i).getDrinkName());
                                                            out.println(custDrinkList.get(i).getCalories());
                                                            out.println("<option label='" + custDrinkList.get(i).getDrinkName() + "' value = '" + custDrinkList.get(i).getId() + "''>'" + custDrinkList.get(i).getDrinkName() + "'</option>");

                                                        }
                                                    }
                                                    out.println("</select>");
                                                    out.println("Date: <input type='text' name='date' value='" + date + "' id='drinkdatepicker' readonly >");
                                                    out.println("<p>You may also select a date, by clicking on the input field</p>");

                                                    out.println("</br>");
                                                    out.println("<button type='button' class='button2' onClick='createDrink()'>Create new drink</button><input type='submit' name ='submit' value='Add drink to diary!' class='button2'>");
                                                    out.println("</form>");

                                                }
                                            %>
                                        </div>                               
                                        <h3>Food history</h3>
                                        <div>
                                            <p><a target="_blank" href="printfoodlist.jsp">Click here for a printable version</a></p>
                                            <hr>
                                            <ul class="vertical-ticker">
                                                <%
                                                    ArrayList<Food> consumedFood = new ArrayList<>();
                                                    consumedFood = user.getUsersConsumedFood();

                                                    for (int i = 0; i < consumedFood.size(); i++) {
                                                        out.println("<li>");
                                                        out.println(consumedFood.get(i).getDate());
                                                        out.println(" - You ate: ");
                                                        out.println(consumedFood.get(i).getFoodName());
                                                        out.println(" as a ");
                                                        out.println(consumedFood.get(i).getMealType());
                                                        out.println(" therefore you consumed ");
                                                        out.println(consumedFood.get(i).getCalories() + " calories.");
                                                        out.println("</li>");
                                                    }
                                                    if (consumedFood.isEmpty()) {
                                                        out.println("You have no food history");
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                        <h3>Drink history</h3>
                                        <div>
                                            <p><a target="_blank" href="printdrinklist.jsp">Click here for a printable version</a></p>
                                            <hr>
                                            <ul class="vertical-ticker">
                                                <%
                                                    ArrayList<Drink> consumedDrink = new ArrayList<>();
                                                    consumedDrink = user.getUsersConsumedDrink();

                                                    for (int i = 0; i < consumedDrink.size(); i++) {
                                                        out.println("<li>");
                                                        out.println(consumedDrink.get(i).getDate());
                                                        out.println(" - You drank: ");
                                                        out.println(consumedDrink.get(i).getDrinkName());
                                                        out.println(" therefore you consumed ");
                                                        out.println(consumedDrink.get(i).getCalories() + " calories.");
                                                        out.println("</li>");
                                                    }
                                                    if (consumedDrink.isEmpty()) {
                                                        out.println("You have no drink history");
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                        <h3>Calorie Intake</h3>
                                        <div>
                                            <form action="UserController?request=Getcalories" method="post">
                                                <p>Select a date to see that days calorie intake in the user information on the left</p>
                                                <input type="text" id="datepicker" name="date" size="10" value="<%=date%>" required readonly></p>
                                                <input type="submit" class="button2" name="Submit"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                                <div id="tab2">
                                    <h3>Exercise Diary</h3>
                                    <p>Click on a date in the calendar to add an exercise to that date</p>
                                    <center><div id="exercisedatepicker"></div></center>
                                    <hr>
                                    <div id="accordion">
                                        <h3>Exercise history</h3>
                                        <div>
                                            <p><a target="_blank" href="printexerciselist.jsp">Click here for a printable version</a></p>

                                            <ul class="vertical-ticker">

                                                <%if (user.getUsersExercise() != null) {
                                                        ArrayList<Exercise> exerciseList = user.getUsersExercise();

                                                        for (int i = 0; i < exerciseList.size(); i++) {
                                                            out.println("<li>");
                                                            out.println(exerciseList.get(i).getDate());
                                                            out.println(" - ");
                                                            out.println("You went ");
                                                            out.println(exerciseList.get(i).getExerciseType());
                                                            out.println(" for ");
                                                            out.println(exerciseList.get(i).getDistance());
                                                            out.println("metres.");
                                                            out.println("It took you ");
                                                            out.println(exerciseList.get(i).getMinutes());
                                                            out.println("minutes.");
                                                            out.println("</li>");

                                                        }
                                                        if (exerciseList.isEmpty()) {
                                                            out.println("You have no exercise history");
                                                        }
                                                    }
                                                %>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab3">
                                    <div id="accordion">
                                        <h3> Create Exercise Goal!</h3>
                                        <div>
                                            <form action="GoalController?request=CreateUserGoal" method="post"><p>Goal Type:<select name="type" required><option value="" selected disabled><option value="Running">Running</option><option value="Swimming">Swimming</option></select></p></br><p>Target Value:<input type="number" maxlength="50" size="40" name="target" required></p></br><p>Start Date:<input type="text"  id="datepicker2" maxlength="15" size="40" name="startDate" value="<%=date%>" required readonly></p></br><p>End Date:<input type="text" id="datepicker1" maxlength="15" size="40" name="endDate" value="<%=date%>" required readonly></p></br><input type="submit" value="Create Exercise Goal" class="button2"></form>
                                        </div>
                                    </div>
                                    <br/>
                                    <%
                                        if (userGoals != null) {
                                            if (userGoals.size() == 0) {
                                                out.println("<h3>You don't have any exercise goals, create one?</h3>");
                                            } else {
                                                out.println("<h5>In Progress Goals</h5>");
                                                out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");
                                                for (Goal g : userGoals) {
                                                    if (!(g.getGoalType().equals("Weight"))) {
                                                        if (g.getGoalStatus().equals("In Progress")) {
                                                            boolean expire = false;
                                                            for (Goal f : userExpiringGoals) {
                                                                if (g.getGoalID() == f.getGoalID()) {
                                                                    expire = true;
                                                                }
                                                            }
                                                            if (!expire) {
                                                                out.println("<li>");
                                                                out.println("<div id ='goals'>");
                                                                double theValue = (g.getGoalProgress() / g.getTargetValue()) * 100;
                                                                if (theValue > 100) {
                                                                    theValue = 100;
                                                                }
                                                                out.println("<div class='progressbar'>" + theValue + "</div>");
                                                                out.println(g.getGoalType() + " - Complete " + g.getTargetValue() + "m by " + g.getEndDate() + "</br>");
                                                                out.println("<button type='button' class='button2' onClick='addProgressExerciseGoal(" + g.getGoalProgress() + ", " + g.getGoalID() + ", " + g.getTargetValue() + ")'>Add Progress</button>");
                                                                out.println("<button type='button' class='button2' onClick='editGoal(" + g.getGoalID() + ", " + g.getTargetValue() + ")'>Edit Goal</button>");
                                                                out.println("<a target='_blank' href='goalLog.jsp?id="+g.getGoalID()+"'><button class='button2'>Goal Logs</button></a>");
                                                                out.println("<button type='button' class='button2' onClick='deleteGoal(" + g.getGoalID() + ")'>Delete Goal</button>");

                                                                out.println("</div>");

                                                                out.println("</li>");

                                                                out.println("<hr>");
                                                            } else {
                                                                out.println("<li>");
                                                                out.println("<div id ='goalstwo'>");
                                                                double theValue = (g.getGoalProgress() / g.getTargetValue()) * 100;
                                                                if (theValue > 100) {
                                                                    theValue = 100;
                                                                }
                                                                out.println("<h3><img src='images/expire.png; alt='Expiring Soon' height='30' width='30'>  Warning: Expires in less than 7 days  <img src='images/expire.png; alt='Expiring Soon' height='30' width='30'></h3>");
                                                                out.println("<div class='progressbar'>" + theValue + "</div>");
                                                                out.println(g.getGoalType() + " - Complete " + g.getTargetValue() + "m by " + g.getEndDate() + "</br>");
                                                                out.println("<button type='button' class='button2' onClick='addProgressExerciseGoal(" + g.getGoalProgress() + ", " + g.getGoalID() + ", " + g.getTargetValue() + ")'>Add Progress</button>");
                                                                out.println("<button type='button' class='button2' onClick='editGoal(" + g.getGoalID() + ", " + g.getTargetValue() + ")'>Edit Goal</button>");
                                                                out.println("<a target='_blank' href='goalLog.jsp?id="+g.getGoalID()+"'><button>Goal Logs</button></a>");
                                                                out.println("<button type='button' class='button2' onClick='deleteGoal(" + g.getGoalID() + ")'>Delete Goal</button>");
                                                                out.println("</div>");
                                                                out.println("</li>");

                                                                out.println("<hr>");
                                                            }
                                                        }
                                                    }

                                                }
                                                out.println("</ul>");
                                                out.print("<br/>");
                                                out.println("<h5>Completed Exercise Goals</h5>");
                                                out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");
                                                for (Goal g : userGoals) {
                                                    if (!(g.getGoalType().equals("Weight"))) {
                                                        if (g.getGoalStatus().equals("Completed")) {
                                                            out.println("<li>");
                                                            out.println("<div id ='goals'>");
                                                            double theValue = (g.getGoalProgress() / g.getTargetValue()) * 100;
                                                            if (theValue > 100) {
                                                                theValue = 100;
                                                            }
                                                            out.println("<div class='progressbar'>" + theValue + "</div>");
                                                            out.println(g.getGoalType() + " - Complete " + g.getTargetValue() + "m by " + g.getEndDate() + "</br>");
                                                            out.println("</div>");
                                                            out.println("</li>");
                                                            out.println("<hr>");
                                                        }
                                                    }

                                                }
                                                out.println("</ul>");
                                                out.print("<br/>");
                                                out.println("<h5>Failed Exercise Goals</h5>");
                                                out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");
                                                for (Goal g : userGoals) {
                                                    if (!(g.getGoalType().equals("Weight"))) {
                                                        if (g.getGoalStatus().equals("Failed")) {
                                                            out.println("<li>");
                                                            out.println("<div id ='goals'>");
                                                            double theValue = (g.getGoalProgress() / g.getTargetValue()) * 100;
                                                            if (theValue > 100) {
                                                                theValue = 100;
                                                            }
                                                            out.println("<div class='progressbar'>" + theValue + "</div>");
                                                            out.println(g.getGoalType() + " - Complete " + g.getTargetValue() + "m by " + g.getEndDate() + "</br>");
                                                            out.println("</div>");
                                                            out.println("</li>");
                                                            out.println("<hr>");
                                                        }
                                                    }

                                                }
                                                out.println("</ul>");

                                            }
                                        }
                                    %>
                                </div>
                                <div id="tab4">
                                    <div id="accordion"><h3>Create Weight Goal!</h3>
                                        <div><%
                                            boolean hasWeightGoal = false;
                                            if (userGoals != null) {
                                                out.print("<form action=\"GoalController?request=CreateUserGoal\" method=\"post\"><br>");
                                                out.print("<p>Goal Type: Weight<input type=\"hidden\" maxlength=\"75\" size=\"40\" name=\"type\" value=\"Weight\" required readonly></p><br>");
                                                out.print("<p>Current Value: " + user.getWeightInKG() + " <input type=\"hidden\" maxlength=\"50\" size=\"40\" name=\"current\" value=\"" + user.getWeightInKG() + "\" required readonly>kg</p><br>");
                                                out.print("<p>Target Value:<input type=\"number\" step=\"0.01\" min=\"0\" maxlength=\"50\" size=\"40\" name=\"target\" placeholder=\"Enter Target Here...\" required>kg</p><br>");
                                                out.print("<p>Start Date:<input type='text' id='datepicker3' maxlength='15' size='40' name='startDate' value=\"" + date + "\" required readonly></p>");
                                                out.print("<p>End Date:<input type='text' id='datepicker4' maxlength='15' size='40' name='endDate' value=\"" + date + "\" required readonly></p>");
                                                out.print("<input type=\"submit\" value=\"Create Goal\" class=\"button2\"><br><br>");
                                                out.print("</form>");
                                            } else {
                                                for (Goal g : userGoals) {
                                                    if (g.getGoalType().equals("Weight")) {
                                                        hasWeightGoal = true;
                                                    }
                                                }

                                                if (!hasWeightGoal) {

                                                    out.print("<form action=\"GoalController?request=CreateUserGoal\" method=\"post\"><br>");
                                                    out.print("<p>Goal Type: Weight<input type=\"hidden\" maxlength=\"75\" size=\"40\" name=\"type\" value=\"Weight\" required readonly></p><br>");
                                                    out.print("<p>Current Value: " + user.getWeightInKG() + " <input type=\"hidden\" maxlength=\"50\" size=\"40\" name=\"current\" value=\"" + user.getWeightInKG() + "\" required readonly>kg</p><br>");
                                                    out.print("<p>Target Value:<input type=\"number\" step=\"0.01\" min=\"0\" maxlength=\"50\" size=\"40\" name=\"target\" placeholder=\"Enter Target Here...\" required>kg</p><br>");
                                                    out.print("<p>Start Date:<input type='text' id='datepicker3' maxlength='15' size='40' name='startDate' value=\"" + date + "\" required readonly></p>");
                                                    out.print("<p>End Date:<input type='text' id='datepicker4' maxlength='15' size='40' name='endDate' value=\"" + date + "\" required readonly></p>");
                                                    out.print("<input type=\"submit\" value=\"Create Goal\" class=\"button2\"><br><br>");
                                                    out.print("</form>");
                                                } else {
                                                    out.println("Sorry, you cannot create another weight goal until you complete your current weight goal");
                                                }
                                            }
                                            %>
                                        </div>
                                    </div>
                                    <br/>
                                    <%
                                        if (userGoals != null) {

                                            out.println("<h5>In Progress Goal</h5>");
                                            for (Goal g : userGoals) {
                                                if (g.getGoalType().equals("Weight") && g.getGoalStatus().equals("In Progress")) {
                                                    boolean expire = false;
                                                    for (Goal f : userExpiringGoals) {
                                                        if (g.getGoalID() == f.getGoalID()) {
                                                            expire = true;
                                                        }
                                                    }

                                                    if (expire) {
                                                        out.println("<div id ='goalstwo'>");
                                                        out.println("<h3><img src='images/expire.png; alt='Expiring Soon' height='30' width='30'>  Warning: Expires in less than 7 days  <img src='images/expire.png; alt='Expiring Soon' height='30' width='30'></h3>");
                                                        out.println("Your goal is to weigh " + g.getTargetValue() + "kg by " + g.getEndDate() + "</br>");
                                                        out.println("<button type='button' class='button2' onClick='addProgressWeightGoal(" + g.getGoalProgress() + ", " + g.getGoalID() + ", " + g.getTargetValue() + ")'>Add Goal Progress</button>");
                                                        out.println("<button type='button' class='button2' onClick='deleteGoal(" + g.getGoalID() + ")'>Delete Goal</button>");
                                                    } else {
                                                        out.println("<div id ='goals'>");
                                                        out.println("Your goal is to weigh " + g.getTargetValue() + "kg by " + g.getEndDate() + "</br>");
                                                        out.println("<button type='button' class='button2' onClick='addProgressWeightGoal(" + g.getGoalProgress() + ", " + g.getGoalID() + ", " + g.getTargetValue() + ")'>Add Goal Progress</button>");
                                                        out.println("<button type='button' class='button2' onClick='deleteGoal(" + g.getGoalID() + ")'>Delete Goal</button>");

                                                    }
                                                    out.println("</div>");
                                                    out.println("<hr>");

                                                }
                                            }

                                            out.println("<h5>Completed Goals</h5>");
                                            out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");

                                            for (Goal g : userGoals) {
                                                if (g.getGoalType().equals("Weight") && g.getGoalStatus().equals("Completed")) {
                                                    out.println("<li><div id =\"goals\">You completed your goal and weighed " + g.getTargetValue() + "kg by " + g.getEndDate() + "</div></li>");
                                                }

                                            }
                                        }
                                        out.println("</ul>");


                                    %>
                                </div>
                                <div id="tab5">
                                    <form action="GroupController?request=Search" method="post">
                                        <input type="text" name="search"/>
                                        <input type="submit" value="Search for a group" class="button2">
                                        <a href="grouplist.jsp" class="button2">View Group List</a>

                                    </form>

                                    <hr>
                                    <p><button type="button" class="button2" onClick="createGroup()">Create Group</button></p>
                                    <hr>
                                    <%                                    if (userCreatedGroups != null) {
                                            if (userCreatedGroups.size() == 0) {
                                                out.println("<h3>You have not created a group, create one?</h3>");
                                            } else {
                                                int i = 0;
                                                out.println("<h5>Created Groups</h5>");
                                                out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");
                                                for (Group g : userCreatedGroups) {
                                                    i++;
                                                    out.println("<li><div id =\"goals\"><p><a href='editgroup.jsp?id=" + g.getGroupID() + "'> Group " + i + ": " + g.getGroupName() + " - " + g.getGroupDescription() + "</p></a></div></li>");
                                                }
                                                out.println("</ul>");
                                            }
                                        }
                                    %>
                                    <hr>

                                    <%                                            if (userMemberGroups != null) {
                                            if (userMemberGroups.size() == 0) {
                                                out.println("<h5>You have not joined a group, join one?</h5>");
                                            } else {
                                                int i = 0;
                                                out.println("<h5>Joined Groups</h5>");
                                                out.println("<ul style='width: auto; height: auto; max-height:400px; overflow: auto'/>");
                                                for (Group g : userMemberGroups) {
                                                    i++;
                                                    out.println("<li><div id =\"goals\"><p><a href='viewgroup.jsp?id=" + g.getGroupID() + "'> Group " + i + ": " + g.getGroupName() + " - " + g.getGroupDescription() + "</p></a></div></li>");
                                                }
                                                out.println("</ul>");
                                            }
                                        }
                                    %>
                                </div>
                                <div id="tab6">
                                    <center><div id="chartContainer" style="height: 400px; width: 500px;"></div></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>  
    </body>
    <script>
        $(function () {
            $("#accordion:nth-child(1n)").accordion({
                heightStyle: "content",
                active: false,
                collapsible: true
            });
        });</script>
    <script>     errorCode = <%= request.getParameter("error")%>;
        weightPrompt = <%= request.getParameter("weightPrompt")%>
        if (errorCode === 7) {
            swal({
                title: 'Error',
                text: 'For one of the string fields, you entered invalid character(s)',
                showCancelButton: false,
                showConfirmButton: true,
                closeOnConfirm: true,
                type: 'warning'
            });
        }
        else if (errorCode === 8 || errorCode === 9) {
            swal({
                title: 'Error',
                text: 'For one of the number fields, you entered invalid character(s)',
                showCancelButton: false,
                showConfirmButton: true,
                closeOnConfirm: true,
                type: 'warning'
            });
        }
        else if (errorCode === 420) {
            swal({
                title: 'Update Weight!',
                html: '<p>It has been ' + <%=user.getDaysToNotify()%> + 'day(s) since you updated your weight. Go to your account settings and update your weight<p></br><p>You can also modify the number of days inbetween each notification</p>',
                showCancelButton: false,
                showConfirmButton: true,
                closeOnConfirm: true,
                type: 'info'
            });
        }
    </script>
</html>