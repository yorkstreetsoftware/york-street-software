<%@page import="classes.GroupList"%>
<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>  <%
        session = request.getSession(true);
        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("addinfoerror.jsp");
        }
        String fullName = (user.getFirstName() + " " + user.getLastName());

        GroupList gList = new GroupList();

        ArrayList<Group> groups = gList.getGroupList();
        
        ArrayList<Group> userMemberGroups = user.getUserMemberGroups();
        ArrayList<Group> userCreatedGroups = user.getUserCreatedGroups();
        

    %>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <title>Health Tracker - <%=fullName%></title>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                    <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <center>
                            <div class="border">
                                <h5>Below are the list of groups currently in the system</h5><br/>
                                <%
                                    for (Group g : groups) {
                                        out.print("Group Name: " + g.getGroupName());
                                        out.print("<br/>");
                                        out.print("Group Description: " + g.getGroupDescription());
                                        out.print("<br/>");
                                        boolean canJoin = true;
                                        for (Group group : userMemberGroups) {
                                            if (group.getGroupID() == g.getGroupID()) {
                                                out.print("<a href=\"GroupController?request=LeaveGroup&groupID=" + g.getGroupID() + "\">Leave Group</a>");
                                                canJoin = false;
                                            }
                                        }

                                        for (Group createdGroup : userCreatedGroups) {
                                            if (createdGroup.getGroupID() == g.getGroupID()) {
                                                out.print("<a href=\"editgroup.jsp?id=" + g.getGroupID() + "\">Edit Group</a>");
                                                canJoin = false;
                                            }
                                        }
                                        if (canJoin) {
                                            out.print("<a href=\"GroupController?request=JoinGroup&groupID=" + g.getGroupID() + "\">Join Group</a>");
                                        }
                                        out.print("<br/>");
                                        out.print("<br/>");
                                    }
                                %>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>   
    </body>

</html>