<!DOCTYPE HTML>
<html>
    <head>
        <title>Health TrackerSystem</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="javascript/SweetAlert/dist/sweetalert2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="javascript/SweetAlert/dist/sweetalert2.css">
        <script type="text/javascript" src="javascript/canvasjs.min.js"></script>
        <script type="text/javascript" src="javascript/jquery.totemticker.js"></script>
        <link rel="shortcut icon" href="images/favicon.ico" />

        <style type="text/css">
            .acontainer {
                width: 300px;
                clear: both;
                border-radius: 25px;
                background: #ffffff;
                padding: 20px;
                width: 350px;
                height: auto; 
            }
            .acontainer input {
                border-radius: 25px;
                border-style: ridge;
                padding-left: 4px;
                width: 100%;
                clear: both;
            }

        </style>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1200);
                });
            });
        </script>
    </head>
    <%
        session = request.getSession(true);

        if (session.getAttribute("userSession") != null) {
            response.sendRedirect("profile.jsp");
        }
    %>

    <body>
        <!----start-header----------->
        <div class="header_bg">
            <div class="wrap">
                <div class="header">
                    <!--------start-logo------>
                    <div class="logo">
                        <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                    </div>	
                    <!--------end-logo--------->
                    <!----start-nav-------->	
                    <div class="nav">
                        <ul>
                            <li class="active"><a href="#home" class="scroll">Home</a></li>
                            <li><a href="#signup" class="scroll">signup</a></li>
                            <li><a href="#" onclick="login();" class="scroll">Login</a></li>
                            <div class="clear"> </div>
                        </ul>
                    </div>
                    <!-----end-nav-------->
                    <div class="clear"> </div>
                </div>
            </div>
        </div>
        <div class="slider_bg">
            <div class="wrap">
                <div class="da-slide">
                    <h2><span>Health Tracker</span>System</h2>
                    <p>Health Tracking service to keep your health in check! With our system, you can keep an eye on what you eat daily by storing it in an easy to use calendar as well as keeping track on the exercise you have done.</p>
                    <p>Our easy to use group system allows you and your friends to create exercise goals and compete them as a group. Or if you want to just complete your own goals then that can be done as well!</p>

                    <a href="#signup" class="scroll"><span class="da-img"> </span> </a>
                </div>
            </div>
        </div>
        <div class="wrap" id="signup">
            <div class="main">

                <!---start-content----->
                <div class="about">
                    <div class="clear"> </div>
                    <div class="container">
                        <h2>Create Account</h2>
                        <center><form class="acontainer" action ="UserController?request=register" method="post">
                                </br>

                                <label>
                                    <span>First Name:</span>
                                    <input type="text" name="firstname" placeholder="First Name" required/>
                                </label>
                                </br>

                                <label>
                                    <span>Last Name:</span>
                                    <input type="text" name="lastname" placeholder="Last Name" required/>
                                </label>
                                </br>

                                <label>
                                    <span>Email Address:</span>
                                    <input type="email" name="email" placeholder="Email Address" required/>
                                </label>
                                </br>

                                <label>
                                    <span>Age:</span>
                                </label>
                                <label2>
                                    <select id="birthYear" name="age" required>
                                        <option value="" selected disabled></option>                            
                                        <option label="16" value="16">16</option>
                                        <option label="17" value="17">17</option>
                                        <option label="18" value="18">18</option>
                                        <option label="19" value="19">19</option>
                                        <option label="20" value="20">20</option>
                                        <option label="21" value="21">21</option>
                                        <option label="22" value="22">22</option>
                                        <option label="23" value="23">23</option>
                                        <option label="24" value="24">24</option>
                                        <option label="25" value="25">25</option>
                                        <option label="26" value="26">26</option>
                                        <option label="27" value="27">27</option>
                                        <option label="28" value="28">28</option>
                                        <option label="29" value="29">29</option>
                                        <option label="30" value="30">30</option>
                                        <option label="31" value="31">31</option>
                                        <option label="32" value="32">32</option>
                                        <option label="33" value="33">33</option>
                                        <option label="34" value="34">34</option>
                                        <option label="35" value="35">35</option>
                                        <option label="36" value="36">36</option>
                                        <option label="37" value="37">37</option>
                                        <option label="38" value="38">38</option>
                                        <option label="39" value="39">39</option>
                                        <option label="40" value="40">40</option>
                                        <option label="41" value="41">41</option>
                                        <option label="42" value="42">42</option>
                                        <option label="43" value="43">43</option>
                                        <option label="44" value="44">44</option>
                                        <option label="45" value="45">45</option>
                                        <option label="46" value="46">46</option>
                                        <option label="47" value="47">47</option>
                                        <option label="48" value="48">48</option>
                                        <option label="49" value="49">49</option>
                                        <option label="50" value="50">50</option>
                                        <option label="51" value="51">51</option>
                                        <option label="52" value="52">52</option>
                                        <option label="53" value="53">53</option>
                                        <option label="54" value="54">54</option>
                                        <option label="55" value="55">55</option>
                                        <option label="56" value="56">56</option>
                                        <option label="57" value="57">57</option>
                                        <option label="58" value="58">58</option>
                                        <option label="59" value="59">59</option>
                                        <option label="60" value="60">60</option>
                                        <option label="61" value="61">61</option>
                                        <option label="62" value="62">62</option>
                                        <option label="63" value="63">63</option>
                                        <option label="64" value="64">64</option>
                                        <option label="65" value="65">65</option>
                                        <option label="66" value="66">66</option>
                                        <option label="67" value="67">67</option>
                                        <option label="68" value="68">68</option>
                                        <option label="69" value="69">69</option>
                                        <option label="70" value="70">70</option>
                                        <option label="71" value="71">71</option>
                                        <option label="72" value="72">72</option>
                                        <option label="73" value="73">73</option>
                                        <option label="74" value="74">74</option>
                                        <option label="75" value="75">75</option>
                                        <option label="76" value="76">76</option>
                                        <option label="77" value="77">77</option>
                                        <option label="78" value="78">78</option>
                                        <option label="79" value="79">79</option>
                                        <option label="80" value="80">80</option>
                                        <option label="81" value="81">81</option>
                                        <option label="82" value="82">82</option>
                                        <option label="83" value="83">83</option>
                                        <option label="84" value="84">84</option>
                                        <option label="85" value="85">85</option>
                                        <option label="86" value="86">86</option>
                                        <option label="87" value="87">87</option>
                                        <option label="88" value="88">88</option>
                                        <option label="89" value="89">89</option>
                                        <option label="90" value="90">90</option>
                                        <option label="91" value="91">91</option>
                                        <option label="92" value="92">92</option>
                                        <option label="93" value="93">93</option>
                                        <option label="94" value="94">94</option>
                                        <option label="95" value="95">95</option>
                                        <option label="96" value="96">96</option>
                                        <option label="97" value="97">97</option>
                                        <option label="98" value="98">98</option>
                                        <option label="99" value="99">99</option>
                                        <option label="100" value="100">100</option>

                                    </select>
                                </label2>
                                </br></br>

                                <label>
                                    <span>Password:</span>
                                    <input type="password" name="password" id="password" onkeyup="checkPass();
                                            return false;" placeholder="Password" required/>
                                </label>
                                </br>

                                <label>
                                    <span>Confirm Password:</span>
                                    <input type="password" name="confirmpassword" id="confirmpassword" onkeyup="checkPass();
                                            return false;" placeholder="Confirm Password" required/>
                                </label>
                                </br>

                                <label>
                                    <input type="submit" name ="submit" value="Register" class="button2">
                                </label>
                            </form></center>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
                <div class="copy">
                    <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
                </div>
                <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"> </span></a>
        </div>
    </body>
    <script>
        function login() {
            swal({
                title: 'Login',
                html: '<center><form class="acontainer" action="UserController?request=login" method="post">Email: <input type="text" size="40" name="loginemail" required></br></br>Password: <input type="password" size="40" name="loginpassword" required></br></br><input type="submit" value="Login" class="button2"></form></center>',
                showCancelButton: true,
                showConfirmButton: false,
                closeOnConfirm: false
            });
        }
        ;
    </script>
    <script type="text/javascript">
        function checkPass()
        {
            var pass1 = document.getElementById('password');
            var pass2 = document.getElementById('confirmpassword');
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            var emptyColor = "#fff";
            if ((pass1.value === "") && (pass2.value === "")) {
                pass2.style.backgroundColor = emptyColor;
            } else if (pass1.value === pass2.value) {
                pass2.style.backgroundColor = goodColor;
            }
            else {
                pass2.style.backgroundColor = badColor;
            }
        }
    </script>
    <script>
        errorCode = <%= request.getParameter("error")%>;
        if (errorCode === 6) {
            window.alert("You entered an invalid name (Firstname/Lastname)");
        }
        else if (errorCode === 4) {
            window.alert("The email you entered already exists");
        }
        else if (errorCode === 5) {
            window.alert("Email already exists");

        }
        else if (errorCode === 2 || errorCode === 3) {
            window.alert("Login Failed");

        }
    </script>
</html>