<%-- 
    Document   : deletedrink
    Created on : 10-Apr-2015, 12:59:02
    Author     : Max
--%>
<%@page import="classes.User"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
        <title>Health tracker </title>
    </head>
     
          <%
        

        
        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.getIsStaff()== false) {
            response.sendRedirect("index.jsp");
        }
         
        ArrayList<Drink> drinkList = null;

        if (session.getAttribute("drinkList") != null) {
            drinkList = (ArrayList<Drink>) session.getAttribute("drinkList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }

        String paramID = request.getParameter("drinkid");

        int id = Integer.parseInt(paramID);

        Drink drink = null;
        for (int i = 0; i < drinkList.size(); i++) {

            if (drinkList.get(i).getId() == id) {
                drink = drinkList.get(i);

            }
        }

            if (drink == null) {
                response.sendRedirect("admin.jsp");
                return;
            }


    %>
    
    <body>
             <div class="header-wrap">
            <header>
                <a href="admin.jsp">
                    <hgroup>

                    </hgroup>
                </a>

                <nav>
                    <span class="spanFormat">
                        <form action="account.jsp" method="get">
                            Welcome! You are logged in as an Admin&nbsp;&nbsp;
                        </form>
                    </span>
                    <span class="spanFormat">
                        <form action="index.jsp" method="get">
                            <input type="submit" value="Logout" class="button">
                        </form>
                    </span>
                </nav>
            </header>
        </div>
        <div class="content-wrap">
           
                <hr>
                <form action="AdminController?request=deleteDrink" method="post">

                    <h1>Are you sure you want to delete the following Food?</h1>
                   
                    <p><input type="hidden" name="drinkid" value=<%=drink.getId()%> </p>
                    <%
                    
                    out.println("ID: " +drink.getId());
                    out.print("<br>");
                    out.println("Name: " +drink.getDrinkName());
                    out.print("<br>");
                    out.println("Calories: " + drink.getCalories());
                    out.print("<br>");
                    %>

                    <input type="submit" value="Delete this Drink" class="button">
                </form>
                <hr>
            </div>
            
        
        <div class="clear"></div>

        <div class="footer">
            <p>Copyright School Alumni Site (SAS) - 2014</p>
        </div>
    </body>
</html>