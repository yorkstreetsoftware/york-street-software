<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }


    String fullName = (user.getFirstName() + " " + user.getLastName());

%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <title>Health Tracker - <%=fullName%></title>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                <li><a href="usefulwebsites.jsp" class="scroll">Useful Websites</a></li>
                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <center>                        
                        <div class="border">
                            <div class="theContainer" >
                                <p>It appears to be your first time logging in, please enter some information about yourself so you can proceed to your profile</p>
                                <p>The fields with asterisks next to them cannot be left blank</p>

                                <form action="UserController?request=completeprofile" method="post"><br>
                                    <p>Current Weight: <input type="number" step="0.01" min="0" cols="10" rows="1" name="weight" required>kg*</p><br>
                                    <p>Current Height: <input type="number" maxlength="10" size="10" name="height" required>cm*</p><br>
                                    <p>Gender: <select name="gender" required>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <br><br>
                                    </p>
                                    <input type="submit" value="Save Information" class="button2">
                                </form>
                            </div>  
                        </div>
                    </center>
                </div>
            </div>
        </div>
       <div class="footer-bottom">
            <div class="copy">
                    <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
                </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>  
    </body>

</html>