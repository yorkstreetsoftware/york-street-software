<%@page import="classes.User"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Drink"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
        <title>Health tracker </title>
    </head>
    
     <%
        
       session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }

        if (user.getIsStaff()== false) {
            response.sendRedirect("index.jsp");
            return;
        }
        
        ArrayList<Drink> drinkList = null;
        
        
        
        if (session.getAttribute("drinkList") != null) {
            drinkList = (ArrayList<Drink>)session.getAttribute("drinkList");
        } else {
            response.sendRedirect("admin.jsp");
            return;
        }
    %>
    <body>
        <div class="header-wrap">
            <header>
                 <a href="admin.jsp">
                <hgroup>
                        
                </hgroup>
                </a>

                <nav>
                    <span class="spanFormat">
                        <form action="account.jsp" method="get">
                            Welcome! You are logged in as an Admin&nbsp;&nbsp;
                        </form>
                    </span>
                    <span class="spanFormat">
                        <form action="index.jsp" method="get">
                            <input type="submit" value="Logout" class="button">
                        </form>
                    </span>
                </nav>
            </header>
        </div>
        <div class="content-wrap">
            <div class="content-left">
                <hr>
                <form action="AdminController?request=editDrink" method="post">
                   
                    <%
                      for  (int i=0;i<drinkList.size();i++)
          {

              out.println(drinkList.get(i).getId());
              out.println(drinkList.get(i).getDrinkName());
              out.println(drinkList.get(i).getProtein());
              out.println("<a href=\"editdrink.jsp?drinkid=" + drinkList.get(i).getId()+ "\">Edit Drink</a>");
              out.println("<a href=\"deletedrink.jsp?drinkid=" + drinkList.get(i).getId() + "\">Delete Drink</a>");
                        
              out.print("<br>");
              
                        
          }
                      
                      
        
                    %> 
                
                 </form>
                <hr>
            </div>
            <div class="content-right">
                <hr>
                
                <hr>
            </div>
        </div>
        <div class="clear"></div>

        <div class="footer">
            <p>Copyright School Alumni Site (SAS) - 2014</p>
        </div>
                
                
    </body>
</html>
