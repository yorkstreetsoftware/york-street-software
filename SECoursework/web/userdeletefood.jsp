<%-- 
    Document   : userdeletefood
    Created on : 13-Apr-2015, 14:16:50
    Author     : Max
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="classes.Food"%>
<%@page import="classes.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

        session = request.getSession(true);

        User user = null;

        if (session.getAttribute("userSession") != null) {
            user = (User) session.getAttribute("userSession");
        } else {
            response.sendRedirect("index.jsp");
            return;
        }
        ArrayList<Food> foodList = user.getCustomFoodList();
        String paramID = request.getParameter("foodid");

        int id = Integer.parseInt(paramID);
        
         Food food = null;
         
         for (int i = 0; i < foodList.size(); i++) {
            if (foodList.get(i).getId() == id) {
                food = foodList.get(i);
            }
        }
        if (food == null) {
            response.sendRedirect("profile.jsp");
            return;
        }

       
    %>
        
        
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
        <title>Health tracker </title>
    </head>
    <body>
             <div class="header-wrap">
            <header>
                <a href="admin.jsp">
                    <hgroup>

                    </hgroup>
                </a>

                <nav>
                    <span class="spanFormat">
                        <form action="account.jsp" method="get">
                            Welcome! You are logged in as an Admin&nbsp;&nbsp;
                        </form>
                    </span>
                    <span class="spanFormat">
                        <form action="index.jsp" method="get">
                            <input type="submit" value="Logout" class="button">
                        </form>
                    </span>
                </nav>
            </header>
        </div>
        <div class="content-wrap">
           
                <hr>
                <form action="UserController?request=deleteFood" method="post">

                    <h1>Are you sure you want to delete the following Food?</h1>
                   
                    <p><input type="hidden" name="foodid" value=<%=food.getId()%> </p>
                    <%
                    out.println("Name: " +food.getFoodName());
                    out.print("<br>");
                    out.println("Meal type: " + food.getMealType());
                    out.print("<br>");
                    %>

                    <input type="submit" value="Delete this food" class="button">
                </form>
                <hr>
            </div>
            
        
        <div class="clear"></div>

        <div class="footer">
            <p>Copyright Health Tracker 2015</p>
        </div>
    </body>
</html>
