<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();

    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }

    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }

    String suggestion = "";
    double targetWeight = 0;
    double currentWeight = user.getWeightInKG();

    if (bmi > 18.5 && bmi < 24.9) {
        response.sendRedirect("profile.jsp");
        return;
    } else if (bmi > 24.9) {
        suggestion = "over your maximum BMI";
        targetWeight = currentWeight - 10;
    } else {
        suggestion = "under your minimum BMI";
        targetWeight = currentWeight + 10;
    }
    ArrayList<Food> foodList = user.getFoodList();
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> drinkList = user.getDrinkList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);
%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 
        <title>Health Tracker - <%=fullName%></title>
        <script>
            var dateToday = new Date();
            $(function () {
                $("#datepicker").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showButtonPanel: true,
                    minDate: dateToday
                });
            });</script>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
 <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <center>                        
                        <div class="border">
                            <div class="theContainer" >
                                <p>Thanks for giving us your information, from this we can tell you that you are currently <%=suggestion%></p>
                                <p>By using this information, we are able to suggest you an initial goal you can find below.</p>
                                <p>We've given you the goal, you set the date you want to achieve it by!</p>
                                <div class="border">
                                    <form action="GoalController?request=CreateUserGoal" method="post"><br>
                                        <p>Goal Type: Weight<input type="hidden" maxlength="75" size="40" name="type" value="Weight" required readonly></p><br>
                                        <p>Current Value: <%=currentWeight%><input type="hidden" maxlength="50" size="40" name="current" value="<%=currentWeight%>" required readonly>kg</p><br>
                                        <p>Target Value: <%=targetWeight%><input type="hidden" maxlength="50" size="40" name="target" value="<%=targetWeight%>" required readonly>kg</p><br>
                                        <p>Start Date: <%=date%><input type="hidden" maxlength="15" size="40" name="startDate" value="<%=date%>" required readonly></p><br>
                                        <p>End Date: <input type="text" id="datepicker" name="endDate" value="<%=date%>" required readonly></p><br><br>
                                        <input type="submit" value="Create Goal" class="button2"><br><br>
                                    </form>
                                    <p>You can optionally skip this step by clicking <a href="profile.jsp">Here</a>.</p>
                                </div>
                            </div>  
                        </div>
                    </center>
                </div>
            </div>
        </div>
       <div class="footer-bottom">
            <div class="copy">
                    <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
                </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>  
    </body>

</html>