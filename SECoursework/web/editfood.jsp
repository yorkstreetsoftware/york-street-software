<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();

    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }

    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }
    ArrayList<Food> foodList = user.getFoodList();
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> drinkList = user.getDrinkList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);

    String paramID = request.getParameter("foodid");

    int id = Integer.parseInt(paramID);

    Food food = null;

    for (int i = 0; i < foodList.size(); i++) {
        if (foodList.get(i).getId() == id) {
            food = foodList.get(i);
        }
    }

    if (food == null) {
        response.sendRedirect("admin.jsp");
        return;
    }

%>
<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <title>Health Tracker - <%=fullName%></title>
    </head>
    <body>
        <div id ="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li class="active"><a href="#home" class="scroll">Home</a></li>
                                <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                    <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <center>
                            <div class="border">
                                <form action="AdminController?request=updateFood" method="post">

                                    <h1>Edit Food</h1>
                                    <p><input type="hidden" name="foodid" value=<%=food.getId()%> </p>
                                    <p>Food Name: <input type="text" maxlength="50" size="40" name="foodName" value="<%=food.getFoodName()%>" required></p>
                                    <p>Food Type:<input type="text" maxlength="50" size="40" name="mealType" value="<%=food.getMealType()%>"  required></p>
                                    <p>Calories:<input type="text" maxlength="50" size="40" name="calories" value=<%=food.getCalories()%>  required></p>
                                    <p>Protein:<input type="text" maxlength="50" size="40" name="protein" value=<%=food.getProtein()%>  required></p>
                                    <p>Carbohydrates:<input type="text" maxlength="50" size="40" name="carbohydrates" value=<%=food.getCarbohydrates()%>  required></p>
                                    <p>Sugar:<input type="text" maxlength="50" size="40" name="sugar" value=<%=food.getSugar()%> required></p>
                                    <p>Food fat: <input type="text" maxlength="50" size="40" name="fat" value=<%=food.getFat()%>  required></p>
                                    <p>Saturated fat: <input type="text" maxlength="50" size="40" name="satFat" value=<%=food.getSatFat()%> required></p>
                                    <p>Salt:<input type="text" maxlength="50" size="40" name="salt" value=<%=food.getSalt()%>  required></p>


                                    <input type="submit" value="Edit Food" class="button2">
                                </form>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>   
    </body>

</html>