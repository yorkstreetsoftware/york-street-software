<%@page import="classes.Exercise"%>
<%@page import="java.util.Calendar"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="classes.Food"%>
<%@page import="classes.Drink"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    int numberOfGroups = 0;
    int numberOfGoals = 0;

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());
    double weight = user.getWeightInKG();
    double heightInM = (double) user.getHeightInCM() / 100;
    Double bmi = (double) (weight / heightInM) / heightInM;
    int age = user.getAge();
    DecimalFormat oneDigit = new DecimalFormat("#,##0.0");//format to 1 decimal place

    bmi = Double.valueOf(oneDigit.format(bmi));

    if ((userCreatedGroups != null)) {
        numberOfGroups = userCreatedGroups.size();

    }

    if (userGoals != null) {
        numberOfGoals = userGoals.size();
    }
    ArrayList<Food> custFoodList = user.getCustomFoodList();
    ArrayList<Drink> custDrinkList = user.getCustomDrinkList();

    Calendar cal = Calendar.getInstance();
    int intMonth = cal.get(Calendar.MONTH) + 1;
    int year = cal.get(Calendar.YEAR);
    int intDay = cal.get(Calendar.DAY_OF_MONTH);
    String day = String.valueOf(intDay);
    String month = String.valueOf(intMonth);

    if (intDay < 10) {
        day = "0" + intDay;
    }

    if (intMonth < 10) {
        month = "0" + intMonth;
    }

    String date = (day + "/" + month + "/" + year);
    String theBMIColor = null;
    if (bmi < 18.5) {
        theBMIColor = "<span class='bmiy'>Underweight</span>";
    } else if (bmi >= 18.5 && bmi < 25) {
        theBMIColor = "<span class='bmig'>Normal Weight</span>";
    } else if (bmi >= 25 && bmi < 30) {
        theBMIColor = "<span class='bmiy'>Overweight</span>";
    } else if (bmi >= 30 && bmi < 35) {
        theBMIColor = "<span class='bmio'>Obese</span>";
    } else if (bmi >= 35) {
        theBMIColor = "<span class='bmir'>Morbidly Obese</span>";
    }
%>
<!DOCTYPE html>
<html>

    <head>

        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="javascript/SweetAlert/dist/sweetalert2.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="javascript/SweetAlert/dist/sweetalert2.css">


        <title>Health Tracker - <%=fullName%></title>
        <script>
            function editFood(id, foodname, type, calories, protein, carbohydrates, sugar, foodfat, satfat, salt) {
                swal({
                    title: 'Edit Food',
                    html: '<form action="UserController?request=updateFood" method="post"><p><input type="hidden" name="foodid" value=' + id + ' </p><p>Food Name: <input type="text" maxlength="50" size="40" name="foodName" value="' + foodname + '" required></p><p>Meal Type:<select name="mealType" required><option value=' + type + ' selected>' + type + '</option><option value="snack">Snack</option><option value="breakfast">Breakfast</option><option value="lunch">Lunch</option><option value="dinner">Dinner</option></select></p></p><p>Calories:<input type="number" maxlength="50" size="40" name="calories" value=' + calories + '  required></p><p>Protein:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="protein" value=' + protein + '  required></p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="carbohydrates" value=' + carbohydrates + '  required></p><p>Sugar:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="sugar" value=' + sugar + ' required></p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="fat" value=' + foodfat + '  required></p><p>Saturated fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="satFat" value=' + satfat + ' required></p><p>Salt:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="salt" value=' + salt + '  required></p><input type="submit" value="Edit Food" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function editDrink(id, drinkname, calories, protein, carbohydrates, sugar, foodfat, satfat, salt) {
                swal({
                    title: 'Edit Food',
                    html: '<form action="UserController?request=updateDrink" method="post"><p><input type="hidden" name="drinkid" value=' + id + ' </p><p>Drink Name: <input type="text" maxlength="50" size="40" name="drinkName" value="' + drinkname + '" required></p></p><p>Calories:<input type="number" maxlength="50" size="40" name="dcalories" value=' + calories + '  required></p><p>Protein:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dprotein" value=' + protein + '  required></p><p>Carbohydrates:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dcarbohydrates" value=' + carbohydrates + '  required></p><p>Sugar:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dsugar" value=' + sugar + ' required></p><p>Food fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="dfat" value=' + foodfat + '  required></p><p>Saturated fat: <input type="number" step="0.01" min="0" maxlength="50" size="40" name="dsatFat" value=' + satfat + ' required></p><p>Salt:<input type="number" step="0.01" min="0" maxlength="50" size="40" name="dsalt" value=' + salt + '  required></p><input type="submit" value="Edit Food" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function changeAge() {
                swal({
                    title: 'Update Age',
                    html: '<form action="UserController?request=UpdateAge" method="post"><input type="number" name="age" min="16" max="100">years old<input type="submit" value="Submit" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            function changePicture() {
                swal({
                    title: 'Update Picture',
                    html: '<form action="UserController?request=UpdateImage" method="post"><input type="text" name="image"><input type="submit" value="Submit" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>   
        <script>
            function changeWeight() {
                swal({
                    title: 'Update Weight',
                    html: '<form action="UserController?request=UpdateWeight" method="post"><input type="number" step="0.01" min="0" name="weight">kg<input type="submit" value="Submit" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>   
        <script>
            function changeWeightNotification() {
                swal({
                    title: 'Update Notifcation',
                    html: '<form action="UserController?request=setnotificationdays" method="post"><input type="text" name="days"><input type="submit" value="Submit" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script> 
        <script>
            function changeHeight() {
                swal({
                    title: 'Update Height',
                    html: '<form action="UserController?request=UpdateHeight" method="post"><input type="number" name="height">cm<input type="submit" value="Submit" class="button2"></form>',
                    showCancelButton: true,
                    showConfirmButton: false,
                    closeOnConfirm: false
                });
            }
        </script>
        <script>
            $(document).ready(function () {
                $("#content").find("[id^='tab']").hide(); // Hide all content
                $("#tabs li:first").attr("id", "current"); // Activate the first tab
                $("#content #tab1").fadeIn(); // Show first tab's content

                $('#tabs a').click(function (e) {
                    e.preventDefault();
                    if ($(this).closest("li").attr("id") === "current") { //detection for current tab
                        return;
                    }
                    else {
                        $("#content").find("[id^='tab']").hide(); // Hide all content
                        $("#tabs li").attr("id", ""); //Reset id's
                        $(this).parent().attr("id", "current"); // Activate this
                        $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
                    }
                });
            });</script>
        <script>
            $(function () {
                $("#accordion:nth-child(1n)").accordion({
                    heightStyle: "content", active: false,
                    collapsible: true
                });
            });</script>


    </head>
    <body>
        <div id="containnn">
            <div class="header_bg">
                <div class="wrap">
                    <div class="header">
                        <a href="profile.jsp">
                            <hgroup>

                            </hgroup>
                        </a>
                        <!--------start-logo------>
                        <div class="logo">
                            <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                        </div>	
                        <!--------end-logo--------->
                        <!----start-nav-------->	
                        <div class="nav">
                            <ul>
                                <li><a href="profile.jsp" class="scroll">Home</a></li>
                                <li class="active"><a href="account.jsp" class="scroll">Account Settings</a></li>
                                    <% if (user.getIsStaff() == true) {
                                            out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                        } else {
                                            out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                        }%>                                <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            </ul>
                        </div>
                        <!-----end-nav-------->
                        <div class="clear"> </div>
                    </div>
                </div>
            </div>
            <div id="body">
                <div class="wrap" id="signup">
                    <div class="theContainer">
                        <div id="content-left">
                            <strong><%=fullName%></strong><br>
                            <img src="<%=user.getUsersPhoto()%>" alt="No profile picture" width="175px" height="175px">
                            <div class="info">
                                <p>Current Age: <%=age%></p>
                                <p>Current Weight: <%=weight%>kg</p>
                                <p>Current Height: <%=heightInM%>m</p>
                                <p>Current BMI: <%=bmi%></p>
                                <p>BMI Status: <%=theBMIColor%></p>
                                <p>Number of Groups Created: <%=numberOfGroups%></p>
                                <p>Number of Goals Created: <%=numberOfGoals%></p>

                            </div>
                        </div>
                        <div id="content-right">
                            <ul id="tabs">
                                <li><a href="#" name="tab1">Account</a></li>
                                <li><a href="#" name="tab2">Food/Drink</a></li>
                            </ul>

                            <div id="content"> 
                                <div id="tab1">
                                    <form>
                                        Current Age: <%=age%> -
                                        <button type='button' class='button2' onClick='changeAge()'>Update</button>

                                    </form> <br>
                                    <form>
                                        Current Profile Picture: <img src="<%=user.getUsersPhoto()%>" alt="No profile picture" width="175px" height="175px"> - 
                                        <button type='button' class='button2' onClick='changePicture()'>Update</button>

                                    </form> <br>
                                    <form>
                                        Current Weight: <%=weight%>kg - 
                                        <button type='button' class='button2' onClick='changeWeight()'>Update</button>

                                    </form> <br>
                                    <form>
                                        Current Height: <%=heightInM%>m - 
                                        <button type='button' class='button2' onClick='changeHeight()'>Update</button>

                                    </form>   
                                    <form>
                                        Current Weight Notification: - 
                                        <button type='button' class='button2' onClick='changeWeightNotification()'>Update</button>

                                    </form> 
                                </div>

                                <div id="tab2">
                                    <div id="accordion">
                                        <h3>View/Edit a food</h3>
                                        <div>
                                            <p>Click on a food to view/edit it:</p><br>
                                            <center><ul style="width: auto; height: 200px; overflow: auto"/>
                                                <hr>

                                                <%
                                                    for (int i = 0; i < custFoodList.size(); i++) {

                                                        out.println("<li><button type='button' class='button2' onClick='editFood(" + custFoodList.get(i).getId() + ", \"" + custFoodList.get(i).getFoodName() + "\", \"" + custFoodList.get(i).getMealType() + "\", " + custFoodList.get(i).getCalories() + ", " + custFoodList.get(i).getProtein() + ", " + custFoodList.get(i).getCarbohydrates() + ", " + custFoodList.get(i).getSugar() + ", " + custFoodList.get(i).getFat() + ", " + custFoodList.get(i).getSatFat() + ", " + custFoodList.get(i).getSalt() + ")'>" + custFoodList.get(i).getFoodName() + "</button></li>");
                                                        out.print("<hr>");
                                                    }
                                                %>
                                                </ul></center>
                                        </div>
                                        <h3>View/Edit a drink</h3>
                                        <div>
                                            <p>Click on a drink to view/edit it:</p><br>
                                            <center><ul style="width: auto; height: 200px; overflow: auto"/>
                                                <hr>

                                                <%
                                                    for (int i = 0; i < custDrinkList.size(); i++) {

                                                        out.println("<li><button type='button' class='button2' onClick='editDrink(" + custDrinkList.get(i).getId() + ", \"" + custDrinkList.get(i).getDrinkName() + "\", " + custDrinkList.get(i).getCalories() + ", " + custDrinkList.get(i).getProtein() + ", " + custDrinkList.get(i).getCarbohydrates() + ", " + custDrinkList.get(i).getSugar() + ", " + custDrinkList.get(i).getFat() + ", " + custDrinkList.get(i).getSatFat() + ", " + custDrinkList.get(i).getSalt() + ")'>" + custDrinkList.get(i).getDrinkName() + "</button></li>");
                                                        out.print("<hr>");
                                                    }
                                                %>
                                                </ul></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copy">
                <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
            </div>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
        </div>   
    </body>
</html>