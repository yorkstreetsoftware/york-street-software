<%@page import="classes.FeedMessage"%>
<%@page import="classes.NewsFeed"%>
<%@page import="classes.GroupGoalProgress"%>
<%@page import="classes.GroupGoal"%>
<%@page import="classes.GroupList"%>
<%@page import="javax.script.ScriptEngine"%>
<%@page import="javax.script.ScriptEngineManager"%>
<%@page import="classes.Group"%>
<%@page import="classes.Goal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="classes.User"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> <%
    session = request.getSession(true);

    User user = null;
    ArrayList<Goal> userGoals = null;
    NewsFeed nfeed = new NewsFeed();
    ArrayList<Group> userCreatedGroups = null;
    ArrayList<Group> userMemberGroups = null;
    ArrayList<FeedMessage> gfeed = nfeed.getFeed(); 

    if (session.getAttribute("userSession") != null) {
        user = (User) session.getAttribute("userSession");
    } else {
        response.sendRedirect("index.jsp");
        return;
    }

    if (user.hasCompletedProfile() == false) {
        response.sendRedirect("addinfo.jsp");
    }

    userGoals = user.getUserGoals();
    userCreatedGroups = user.getUserCreatedGroups();
    userMemberGroups = user.getUserMemberGroups();

    String fullName = (user.getFirstName() + " " + user.getLastName());

    int groupID = Integer.parseInt(request.getParameter("id"));

    Group group = null;

    boolean isMember = false;

    for (Group g : userCreatedGroups) {
        if (groupID == g.getGroupID()) {
            isMember = true;
        }
    }

    if (!isMember) {
        response.sendRedirect("profile.jsp");
        return;
    }

    GroupList groupList = new GroupList();

    ArrayList<Group> groups = groupList.getGroupList();

    for (Group g : groups) {
        if (groupID == g.getGroupID() && group == null) {
            group = g;
        }

    }

    ArrayList<User> userList = group.getUserList();
    int groupSize = group.getNumberOfMembers();

    GroupGoal groupGoal = group.getGoal();
    ArrayList<GroupGoalProgress> goalProgressLog = new ArrayList();
    User highestContributor = null;

    if (groupGoal != null) {
        goalProgressLog = groupGoal.getProgressList();

        highestContributor = group.getHighestContributor();

        if (groupGoal.getGoalType().equals("Weight")) {
            groupList.updateList();
        }
    }
    String theGroupDescription = group.getGroupDescription();



%>
<!DOCTYPE html>
<html>

    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
    <script src="javascript/SweetAlert/dist/sweetalert2.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="javascript/SweetAlert/dist/sweetalert2.css">
    <script type="text/javascript" src="javascript/canvasjs.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.totemticker.js"></script>
    <title>Health Tracker - <%=fullName%></title>
    <style>.ui-progressbar{text-align:center; line-height: 125%;}
        .ui-progressbar .ui-progressbar-value{height: 15px; position: relative;}
        .ui-progressbar .ui-widget-header{background: #0F0}
    </style>
    <script>
        $(function () {
            $("#accordion:nth-child(1n)").accordion({
                heightStyle: "content",
                active: false,
                collapsible: true
            });
        });</script>
    <script>
        function editDescription(theGroup) {
            swal({
                title: 'Edit Group Description',
                html: '<form action="GroupController?request=Edit" method="post"><p>Current Description: ' + theGroup + '</p></br><p>New Description: <input type="text" maxlength="255" size="40" name="description" required></p></br></br><input type="submit" value="Edit Description" class="button2"></form>',
                showCancelButton: true,
                showConfirmButton: false,
                closeOnConfirm: false
            });
        }
    </script>
    <script>
        $(document).ready(function () {
            $("#content").find("[id^='tab']").hide(); // Hide all content
            $("#tabs li:first").attr("id", "current"); // Activate the first tab
            $("#content #tab1").fadeIn(); // Show first tab's content

            $('#tabs a').click(function (e) {
                e.preventDefault();
                if ($(this).closest("li").attr("id") === "current") { //detection for current tab
                    return;
                }
                else {
                    $("#content").find("[id^='tab']").hide(); // Hide all content
                    $("#tabs li").attr("id", ""); //Reset id's
                    $(this).parent().attr("id", "current"); // Activate this
                    $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
                }
            });
        });</script>
</head>
<body>
    <div id="containnn">
        <div class="header_bg">
            <div class="wrap">
                <div class="header">
                    <a href="profile.jsp">
                        <hgroup>

                        </hgroup>
                    </a>
                    <!--------start-logo------>
                    <div class="logo">
                        <a href="index.jsp"><img src="images/logo.png" alt="" /></a>
                    </div>	
                    <!--------end-logo--------->
                    <!----start-nav-------->	
                    <div class="nav">
                        <ul>
                            <li class="active"><a href="profile.jsp" class="scroll">Home</a></li>
                            <li><a href="account.jsp" class="scroll">Account Settings</a></li>
                                <% if (user.getIsStaff() == true) {
                                        out.println("<li><a href='admin.jsp' class='scroll'>Admin Settings</a></li>");
                                    } else {
                                        out.println("<li><a href='usefulwebsites.jsp' class='scroll'>Useful Websites</a></li>");

                                    }%>                            <li><a href="UserController?request=Logout" class="scroll">Logout</a></li>
                            <div class="clear"> </div>
                        </ul>
                    </div>
                    <!-----end-nav-------->
                    <div class="clear"> </div>
                </div>
            </div>
        </div>
        <div id="body">
            <div class="wrap" id="signup">
                <div class="theContainer">
                    <div id="content-left">
                        <h1><%=group.getGroupName()%></h1>
                        <img src="images/group.png" alt="No profile picture" width="175px" height="175px">
                        <div class="info">
                            <p>No. Members: <%=groupSize%></p><br>
                            <p>Creator: You!</p><br>
                        </div>
                    </div>
                    <div id="content-right">
                        <ul id="tabs">
                            <li><a href="#" name="tab1">Group Details</a></li>
                            <li><a href="#" name="tab2">Group Members</a></li>
                            <li><a href="#" name="tab3">Group Exercise Goal</a></li>
                            <li><a href="#" name="tab4">Group Weight Goal</a></li>
                            <li><a href="#" name="tab5">News Feed</a></li>
                        </ul>

                        <div id="content"> 
                            <div id="tab1">
                                <p>You are the creator!</p><br>
                                <p>Group Description: <%=theGroupDescription%>
                                    <button type="button" class="button2" onClick="editDescription('<%=theGroupDescription%>')">Edit Description</button> </p>

                            </div>
                            <div id="tab2">
                                <h3>Invite member by email</h3>

                                <form action="EmailController?request=InviteUser" method="post">
                                    Email:
                                    <input type="text" name="email" value="">
                                    <input type="hidden" name="groupName" value="<%=group.getGroupName()%>">
                                    <input type="hidden" name="groupID" value="<%=group.getGroupID()%>">
                                    <br/>
                                    <input type="submit" class="button2" value="Submit">
                                </form>
                                <p>List of group members can go here</p>
                                <hr>
                                <%
                                    for (User u : userList) {
                                        out.print(u.getFirstName() + " " + u.getLastName() + " - " + u.getEmail() + "<br/>");
                                    }
                                %><hr>
                                <p>Number of members: <%=groupSize%></p>
                            </div>
                            <div id="tab3">
                                <%
                                    if (groupGoal == null || !groupGoal.getGoalType().equals("Weight")) {
                                        out.print("<form action=\"GoalController?request=CreateGroupGoal&id=" + groupID + "\" method=\"post\"><p>Goal Type:<select name=\"type\" required><option value=\"\" selected disabled><option value=\"Running\">Running</option><option value=\"Swimming\">Swimming</option></select></p><p>Distance <input type=\"number\" step=\"0.01\" min=\"0\" maxlength=\"10\" size=\"10\" name=\"target\" required>m</p></br><input type=\"submit\" value=\"Create Exercise Goal\" class=\"button2\"></form>");
                                    } else {
                                        if (!groupGoal.getGoalType().equals("Weight")) {
                                            out.print("<p>This groups goal is of type " + groupGoal.getGoalType() + " and the target value is " + groupGoal.getTargetValue() + " meters.</p>");
                                            out.print("<h3>Add Progress</h3>");
                                            out.print("<form action=\"GroupController?request=AddGoalProgress\" method=\"post\"><p>Current Progress: " + groupGoal.getGoalProgress() + "m</p><p>Progress Value:<input type=\"text\" maxlength=\"10\" size=\"10\" name=\"value\" required>m</p></br><input type=\"hidden\" name=\"id\" value='" + groupGoal.getGoalID() + "'></br><input type=\"hidden\" name=\"groupID\" value='" + group.getGroupID() + "'></br><input type=\"hidden\" name=\"target\" value='" + groupGoal.getTargetValue() + "'><input type=\"hidden\" name=\"current\" value='" + groupGoal.getGoalProgress() + "'><input type=\"submit\" value=\"Add Progress\" class=\"button2\"></form>'");
                                        } else {
                                            out.print("This groups goal is a weight goal, not an exercise goal.");
                                        }
                                    }
                                %>

                            </div>
                            <div id="tab4">
                                <%
                                    if (groupGoal == null) {
                                        out.print("<form action=\"GoalController?request=CreateGroupGoal&id=" + groupID + "\" method=\"post\"><p>Goal Type:<select name=\"type\" required><option value=\"\" selected disabled><option value=\"Weight\">Weight</option></select></p><p>New Total Group Weight<input type=\"number\" step=\"0.01\" min=\"0\" maxlength=\"10\" size=\"10\" name=\"target\" required>kg</p></br><input type=\"submit\" value=\"Create Weight Goal\" class=\"button2\"></form>");
                                    } else {
                                        out.print("<p>This groups goal is a " + groupGoal.getGoalType() + " goal and the target group weight is " + groupGoal.getTargetValue() + "kg.</p>");
                                        out.print("<p>The total start weight for the group was: " + groupGoal.getStartValue() + "kg</p>");
                                        out.print("<p>The current total weight for the group is: " + groupGoal.getGoalProgress() + "kg</p>");
                                    }
                                %>

                            </div>
                            <div id="tab5">
                                <h3>Post a new message!</h3>
                                    <form action="FeedPostController?request=newMsg">
                                        <input type="text" maxlength="255" width="100" name="msg">
                                        <input type="hidden" name="gName" value="<%=group.getGroupName()%>">
                                        <input type="hidden" name="fName" value="<%=user.getFirstName()%>">
                                        <input type="hidden" name="sName" value="<%=user.getLastName()%>">
                                        <input type="submit" value="Submit">
                                    </form>
                                    <br />
                                    
                                <% 
                                    if (gfeed != null) {
                                        for (FeedMessage fm : gfeed) {
                                            if (fm.getGroup().equals(group.getGroupName())) {
                                                out.print("<p>" + fm.getName() + " wrote: </p>");
                                                out.print("<p>" + fm.getMsg() + "</p><br />");
                                            }
                                        }
                                    } else {
                                        out.print("There are no messages!");
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="copy">
            <p class="copy">Health TrackerSystem(v.2.0.1) - 2015</p>
        </div>
        <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
    </div>   

</body>
</html>