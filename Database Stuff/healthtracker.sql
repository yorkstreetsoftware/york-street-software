CREATE TABLE UserTable
(
id serial NOT NULL PRIMARY KEY,
email varchar(50) NOT NULL,
password varchar(60) NOT NULL,
firstname varchar(35) NOT NULL,
lastname varchar(35) NOT NULL,
weight decimal,
height int,
age int,
gender varchar(10),
BMI decimal,
isBanned boolean NOT NULL,
completedprofile boolean NOT NULL,
isStaff boolean NOT NULL,
lastlogin varchar(50),
notificationdays int DEFAULT 1,
image varchar(200) DEFAULT 'images/empty.png'
);

CREATE TABLE UserGoals
(
GoalID serial NOT NULL PRIMARY KEY,
userID int NOT NULL,
goalType varchar(20),
startValue decimal,
targetValue decimal,
startDate varchar(20),
endDate varchar(20),
goalProgress decimal,
goalStatus varchar(20)
);

CREATE TABLE UserProgress
(
progressID serial NOT NULL PRIMARY KEY,
goalID serial NOT NULL,
userID serial NOT NULL,
progressVAlue decimal,
entryDate varchar(20)
);

CREATE TABLE UserGroup
(
groupID serial NOT NULL PRIMARY KEY,
groupName varchar(255),
userID int NOT NULL,
groupGoalID int,
description varchar(255),
goalProgress decimal,
goalStatus varchar(20)
);

CREATE TABLE GroupGoal
(
groupGoalID serial NOT NULL PRIMARY KEY,
groupID int NOT NULL,
goalType varchar(50),
startValue decimal,
targetValue decimal,
startDate varchar(20),
completionDate varchar(20),
goalStatus varchar(20),
goalProgress decimal
);

CREATE TABLE GroupMembers
(
groupMembersID serial NOT NULL PRIMARY KEY,
groupID int NOT NULL,
userID int NOT NULL
);

CREATE TABLE GroupGoalProgress
(
groupProgressID serial NOT NULL PRIMARY KEY,
groupID int NOT NULL,
groupGoalID int NOT NULL,
userID int NOT NULL,
progressValue decimal,
entryDate varchar(20)
);

CREATE TABLE Food
(
foodID serial NOT NULL PRIMARY KEY,
foodName varchar(30),
mealType varchar(10),
calories int,
protein decimal,
carbohydrates decimal,
sugar decimal,
fat decimal,
satFat decimal,
salt decimal,
userID int NOT NULL
);
CREATE TABLE Drink
(
drinkID serial NOT NULL PRIMARY KEY,
drinkName varchar(30),
calories int,
protein decimal,
carbohydrates decimal,
sugar decimal,
fat decimal,
satFat decimal,
salt decimal,
userID int NOT NULL
);
CREATE TABLE userFood
(
userFood serial NOT NULL PRIMARY KEY,
userID int NOT NULL,
foodID int NOT NULL,
date varchar(20)
);
CREATE TABLE userDrink
(
userDrink serial NOT NULL PRIMARY KEY,
userID int NOT NULL,
drinkID int NOT NULL,
date varchar(20)
);
CREATE TABLE exercise
(
exerciseID serial NOT NULL PRIMARY KEY,
userID int NOT NULL,
type varchar(20),
distance decimal NOT NULL,
time int NOT NULL,
date varchar(20)
);
CREATE TABLE newsfeed
(
msgID serial NOT NULL PRIMARY KEY,
firstname varchar(35) NOT NULL,
lastname varchar(35) NOT NULL,
groupName varchar(255) NOT NULL,
message varchar(255) NOT NULL
);

